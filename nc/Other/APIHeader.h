//
//  APIHeader.h
//  nc
//
//  Created by docy admin on 15/8/25.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#ifndef nc_APIHeader_h
#define nc_APIHeader_h


#define SearchBarHeight 40
#define groupCellHeight 62
#define badNetWorkViewHeight 44
#define ScreenWidth [[UIScreen mainScreen] bounds].size.width//获取屏幕宽度
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height//获取屏幕高度

#define iconPath @"http://im.datacanvas.io" // 域名，用于拼接用户头像
#define LoginPath @"http://im.datacanvas.io/users/login" // 登录API

#define SetPushToken @"http://im.datacanvas.io/users/setpushToken" //设置百度pushToken
#define socketServerPath @"http://im.datacanvas.io" // 服务器连接

#define getidentifyCodePath @"http://im.datacanvas.io/sms/signup" // 获取验证码
//#define identifyCodePath @"http://im.datacanvas.io/sms/signupSms" // 获取验证码
#define registerPath @"http://im.datacanvas.io/users/regist" // 注册请求
#define registCheck @"http://im.datacanvas.io/users/registCheck" // 注册信息检测是否符合要求

// 公司
#define canJoinCompanyList @"http://im.datacanvas.io/companies/canJoin" // 公司列表
#define CurrentCompanyPath @"http://im.datacanvas.io/users/currentCompany" // 获取当前公司
#define setCurrentCompany @"http://im.datacanvas.io/users/currentCompany/%@" // 设置当前公司
#define joinCompany @"http://im.datacanvas.io/users/company/%@" // 添加公司
#define creatCompanyPath @"http://im.datacanvas.io/companies" // 创建公司API
#define leaveCompanyPath @"http://im.datacanvas.io/companies/%@/leave"  //退出公司
#define delectCompanyPath @"http://im.datacanvas.io/companies/del/%@"  //解散公司
#define GetUserList @"http://im.datacanvas.io/companies/%@/users"  //获取公司用户列表
#define GetCompanyInfo  @"http://im.datacanvas.io/companies/%d"     //获取公司info
#define UpdataCompanyInfo  @"http://im.datacanvas.io/companies/%@"     //更新公司info
#define JoinCompanyInv  @"http://im.datacanvas.io/companies/join/%@" //通过邀请码加入公司
#define userCompanyListPath @"http://im.datacanvas.io/users/companies" // 获取用户公司列表
#define companyOwnerTransfer @"http://im.datacanvas.io/companies/%@/transfer" //公司权限转让
#define CompanyInviteCode  @"http://im.datacanvas.io/companies/%d/invite"
#define addCompany         @"http://im.datacanvas.io/users/company/" // 添加公司
#define allCompanyGroups   @"http://im.datacanvas.io/companies/%@/groups" // 获取当前公司所有小组列表
#define creatCompany @"http://im.datacanvas.io/companies" //  创建公司
#define randomLogo @"http://im.datacanvas.io/randomLogo?type=company"   //获得随机图片


// 群组
#define groupDetailInfoPath @"http://im.datacanvas.io/groups/%@" // 获取群组详细信息
#define getSpecifyGroupUsersListPath @"http://im.datacanvas.io/groups/%@/users" // 获取群组用户列表
#define logoutGroupPath @"http://im.datacanvas.io/groups/%@/leave" // 退出群组
#define setLogoPath @"http://im.datacanvas.io/groups/%@/logo" // 设置群组头像
#define uploadFilePath @"http://im.datacanvas.io/files/upload" // 在上传头像之前获取path
#define userGroupsPath @"http://im.datacanvas.io/groups" // 获取群组列表(已加入)
#define creatGroupPath @"http://im.datacanvas.io/groups" // 创建群组
#define canJionGroupPath @"http://im.datacanvas.io/groups/canJoin" // 获取群组列表(未加入)
#define joinGroupPath @"http://im.datacanvas.io/groups/%@/join" // 加入群组
#define UpdateGroupInfo @"http://im.datacanvas.io/groups/%@" // 更改群组信息
#define getMessageListPath @"http://im.datacanvas.io/groups/%@/messages?page=%d" // 获取所有消息列表
#define getSpecifyGroupUsersListPath @"http://im.datacanvas.io/groups/%@/users" // 获取群组用户列表
#define ResetUnread @"http://im.datacanvas.io/groups/%@/resetunread"  //退出详情列表


/**
 * 个人资料
 */
#define personalInfoPath @"http://im.datacanvas.io/users/" // 获取个人信息
#define logoutPath @"http://im.datacanvas.io/users/logout" // 退出登录
#define setAvatarPath @"http://im.datacanvas.io/users/avatar" // 设置个人头像
#define uploadFilePath @"http://im.datacanvas.io/files/upload" // 在上传头像之前获取path
#define UpdateUserInfo @"http://im.datacanvas.io/users/update" // 更改用户信息
#define GetUserInfo @"http://im.datacanvas.io/users/%@"  //获取用户信息 支持用户id 或者是用户名


#define sendMessagePath @"http://im.datacanvas.io/events/message" // 发送消息


#define addCommentToTopicPath @"http://im.datacanvas.io/topics/%@/comment" // 为某一个topic添加评论
#define getTopicCommentsPath @"http://im.datacanvas.io/topics/%@/comment" // 获取topic评论列表
#define GetTopicDetail @"http://im.datacanvas.io/topics/%@" // 获取topic comment

#define getImageBeforeRegisterPath @"http://im.datacanvas.io/directrandomLogo?type=%@" // 在注册前获取图片
#define getRandomLogoPath @"http://im.datacanvas.io/randomLogo?type=%@" // 在注册前获取图片

#define addFavorite @"http://im.datacanvas.io/users/favorite/%@" // 关注
#define getFavorites @"http://im.datacanvas.io/users/favorites/?page=%d" // 获取关注

#define addTopicsVote @"http://im.datacanvas.io/topics/vote"     //创建投票子话题接口
#define sendVoteRes @"http://im.datacanvas.io/topics/vote/%@"     //创建投票子话题接口
#define sendCloseVote @"http://im.datacanvas.io/topics/vote/%@/close"   //关闭子话题接口

#define sendImage @"http://im.datacanvas.io/images"  //上传图片接口 投票图片

#define iPhone6Plus ScreenWidth==414.0?YES:NO
#define iPhone6 ScreenWidth==375.0?YES:NO

// 创建主题
#define creatPostTopic @"http://im.datacanvas.io/topics/post" // 创建主题
#define creatImageTopic @"http://im.datacanvas.io/topics/image" // 创建图片子话题
#define creatMapTopic @"http://im.datacanvas.io/topics/map" // 创建地图子话题
#define deleteTopic @"http://im.datacanvas.io/topics/del/%@" // 删除子话题
#define getSubTopicListPath @"http://im.datacanvas.io/topics/?groupId=%@&page=%d" // 获取子话题列表

/**
 *  私聊
 */

#define directChat @"http://im.datacanvas.io/groups/directChat" 
#define directChatSendImage @"http://im.datacanvas.io/events/image"

#define GroupOwnerTransfer @"http://im.datacanvas.io/groups/%@/transfer/%@"
#define GroupOwnerDisbandGroup @"http://im.datacanvas.io/groups/delete/%@"
#define SetGroupPrivate @"http://im.datacanvas.io/groups/%@/setPrivate"
#define SetGroupBroadcast @"http://im.datacanvas.io/groups/%@/broadcast" // 设置广播群


#define DeleteDirectChat @"http://im.datacanvas.io/groups/directchat/del/%@" // 删除私聊会话

#define ResetPassword @"http://im.datacanvas.io/users/resetPassword" // 忘记密码
#define ResetPasswordSms @"http://im.datacanvas.io/sms/forgotPassword" // 忘记密码验证码


#define InviteJoinGroup @"http://im.datacanvas.io/groups/%@/invite" // 邀请加入群组

// 通知name名称
#define GetGroupNotificationLevel @"http://im.datacanvas.io/groups/%@/notification" // 获取通知级别 
#define conveyGroupOwer @"conveyGroupOwer" // 转换群主
#define disbandGroup @"disbandGroup" // 解散小组
//#define CFBundleShortVersionString @"CFBundleShortVersionString"
#define CFBundleVersion @"CFBundleVersion"

#define MainColor [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1]
#define CustomColor(a,b,c) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:1]

#endif
