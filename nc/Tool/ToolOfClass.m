//
//  ToolOfClass.m
//  nc
//
//  Created by docy admin on 7/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>
#import "AFNetWorkNotReachableView.h"
//#import "AFNetworkReachabilityManager.h"

#define ScreenWidth [[UIScreen mainScreen] bounds].size.width//获取屏幕宽度
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height//获取屏幕宽度

@class UIViewController;
@implementation ToolOfClass{
    UIActivityIndicatorView *activity;
    UIView *activityBgView;
}
// 时间转换器 第一个参数：只获取上午，下午时间，第二个参数：判断是否为群组列表时间
+ (NSString *)toolGetLocalDateFormateUTCDate:(NSString *)utcDateString isGetUpAmCueerntDate:(BOOL)isAmDate isGroupListDate:(BOOL)isGroupListDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    
    if (isAmDate) { // 只显示上下午时间
        [dateFormatter setDateFormat:@"a HH:mm"];
    } else {
        // 获取一天的早晚时间
        NSDate *fromDate = [self toolGetUpCustomDateWithHour:0]; // 零晨00:00:00
        NSDate *toDate = [self toolGetUpCustomDateWithHour:23]; // 23:59:59
        
        if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
            
            // 输出格式
            if (isGroupListDate) { // 是群组列表的时间
                
                [dateFormatter setDateFormat:@"a HH:mm"];
            } else {

                return @"今天";
            }
        } else if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:NO]){
            
            return @"昨天";
        } else {
            
            if (isGroupListDate) { // 是群组列表的时间
                
                [dateFormatter setDateFormat:@"yy/MM/dd"];
            } else { // 主聊天界面的时间
                
                // 输出格式
                [dateFormatter setDateFormat:@"yyyy年MM月dd日 E"];
            }
        }
    }
    NSString *dateString = [dateFormatter stringFromDate:currentDate];

    return dateString;
}

+ (NSString *)toolGetLocalWeekDateFormateWithUTCDate:(NSString *)utcDateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    // 获取一天的早晚时间
    NSDate *fromDate = [self toolGetUpCustomDateWithHour:0]; // 零晨00:00:00
    NSDate *toDate = [self toolGetUpCustomDateWithHour:23]; // 23:59:59
    
    if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
        return @"今天";
    } else {
        [dateFormatter setDateFormat:@"yyyy年MM月dd日 E"];
    }
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)toolGetLocalAllDateFormateWithUTCDate:(NSString *)utcDateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    // 获取一天的早晚时间
    NSDate *fromDate = [self toolGetUpCustomDateWithHour:0]; // 零晨00:00:00
    NSDate *toDate = [self toolGetUpCustomDateWithHour:23]; // 23:59:59
    
    if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
        
//        [dateFormatter setDateFormat:@"a HH:mm"];
        
        [dateFormatter setDateFormat:@"HH:mm"];
        
    } else {
        fromDate = [fromDate dateByAddingTimeInterval:-24*3600];
        toDate = [toDate dateByAddingTimeInterval:-24*3600];
        if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
            return @"昨天";
        } else {
            [dateFormatter setDateFormat:@"yy/MM/dd"];
        }
    }
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)toolGetLocalAllDetailDateFormateWithUTCDate:(NSString *)utcDateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    // 获取一天的早晚时间
    NSDate *fromDate = [self toolGetUpCustomDateWithHour:0]; // 零晨00:00:00
    NSDate *toDate = [self toolGetUpCustomDateWithHour:23]; // 23:59:59
    
    if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
        
        //        [dateFormatter setDateFormat:@"a HH:mm"];
        
        [dateFormatter setDateFormat:@"HH:mm"];
        
    } else {
        fromDate = [fromDate dateByAddingTimeInterval:-24*3600];
        toDate = [toDate dateByAddingTimeInterval:-24*3600];
        if ([self isBetweenFromDate:fromDate toDate:toDate withCurrentDate:currentDate withIsToday:YES]) {
            return @"昨天";
        } else {
//            [dateFormatter setDateFormat:@"yy/MM/dd"];
            
            [dateFormatter setDateFormat:@"MM/dd HH:mm"];
        }
    }
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)toolGetLocalAmDateFormateWithUTCDate:(NSString *)utcDateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    
//    [dateFormatter setDateFormat:@"a HH:mm"];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)toolGetLocalAllAmDateFormateWithUTCDate:(NSString *)utcDateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDateString];
    
    // 将相差8小时的时间转换成当前时间
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600];
    
//    [dateFormatter setDateFormat:@"yy/MM/dd HH:mm"];
    [dateFormatter setDateFormat:@"MM/dd HH:mm"];

    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    return dateString;
}

+ (NSString *)toolSetUpClientIdWithGroupId:(NSNumber *)groupId{
    NSString *clientId = nil;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSDate *data = [NSDate date];
    clientId = [NSString stringWithFormat:@"%@_%@_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],groupId,[formatter stringFromDate:data]];
    return clientId;
}

+ (BOOL)isBetweenFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate withCurrentDate:(NSDate *)currentDate withIsToday:(BOOL)isToday{
    if ([currentDate compare:fromDate] == NSOrderedDescending && [currentDate compare:toDate] == NSOrderedAscending) {
        return YES;
    }
    return NO;
}


+ (NSDate *)toolGetUpCustomDateWithHour:(NSInteger)hour{
    // 获取当前时间
    NSDate *currentDate = [NSDate date];
    NSCalendar *currentCalentar = [NSCalendar currentCalendar];
    NSDateComponents *currentComps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    currentComps = [currentCalentar components:unitFlags fromDate:currentDate];
    
    // 设置当天的某个点
    NSDateComponents *resultComps = [[NSDateComponents alloc] init];
    [resultComps setYear:[currentComps year]];
    [resultComps setMonth:[currentComps month]];
    [resultComps setDay:[currentComps day]];
    
    if (hour==0) { // 所获取的时间与实际时间相差8小时（想要设置为凌晨，需要加上8小时，下面相同）
        [resultComps setHour:0];
        [resultComps setMinute:00];
        [resultComps setSecond:00];
    } else {
        [resultComps setHour:23];
        [resultComps setMinute:59];
        [resultComps setSecond:59];
    }
    NSCalendar *resultCalendar = [NSCalendar currentCalendar];
    NSDate *resultDate = [resultCalendar dateFromComponents:resultComps];
    return resultDate;
}

- (instancetype)init{
    
    self = [super init];
    if (self) {
        activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityBgView = [[UIView alloc] init];
        activityBgView.backgroundColor = [UIColor whiteColor];
        
        CGRect bounds =[UIScreen mainScreen].bounds;
        activityBgView.frame = CGRectMake(0, 0, bounds.size.width, bounds.size.height);
        activity.center = activityBgView.center;
        CGRect avFrame = activity.frame;
        avFrame.origin.y = avFrame.origin.y-64;
        activity.frame = avFrame;
        [activityBgView addSubview:activity];
    }
    return self;
}

- (void)startAnimationInView:(UIView *)view{
    [view addSubview:activityBgView];
    [activity startAnimating];
}

- (void)stopAnimation{
    [activity stopAnimating];
    [activityBgView removeFromSuperview];
}

// 获取上传文件的路径 ,参数一：请求网址 ，参数二：用于存储请求下来的path(更改头像)
+ (void)toolUploadFilePathWithHttpString:(NSString *)httpStr fileName:(NSString *)fileName file:(UIImage *)image setAvatarOrLogoPath:(NSString *)setAvatarOrLogoPath{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    // 将文件转换成二进制
    NSData *data = UIImageJPEGRepresentation(image, 1.0);

    [manager POST:httpStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        // form表单添加字段
        [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue]==0) {
            NSString *path = responseObject[@"data"][@"path"];
            if ([setAvatarOrLogoPath isEqualToString:setAvatarPath]) { // 上传的是个人头像
                parameter[@"avatar"] = path;
                [info setObject:path forKey:@"avatar"];
            } else { // 上传的是群组头像
                parameter[@"logo"] = path;
            }
            [manager POST:setAvatarOrLogoPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if (![responseObject[@"code"] intValue]==0) {
                    [self showMessage:responseObject[@"message"]];
                } else {
//                    [self showMessage:@"上传成功!"];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self showMessage:@"上传失败!"];
            }];
            
        } else {
            [self showMessage:responseObject[@"message"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self showMessage:@"上传失败!"];
    }];
}

+(void)showMessage:(NSString *)message{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [UIColor blackColor];
    showview.frame = CGRectMake(1, 1, 1, 1);
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    
    UILabel *label = [[UILabel alloc]init];
    
//    CGSize LabelSize = [message sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    
    CGSize LabelSize = [message sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(290, 9000)];
    
    label.frame = CGRectMake(10, 5, LabelSize.width+10, LabelSize.height);
    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont boldSystemFontOfSize:13];
    [showview addSubview:label];
    showview.frame = CGRectMake((ScreenWidth - LabelSize.width - 20)/2,  ScreenHeight*0.5, LabelSize.width+20+6, LabelSize.height+10);
    [UIView animateWithDuration:3 animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}
+ (NSString *)authToken{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
}


+ (AFNetWorkNotReachableView *)showAFNetWorkNotReachableView{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    AFNetWorkNotReachableView *view = [[[NSBundle mainBundle] loadNibNamed:@"AFNetWorkNotReachableView" owner:self options:nil] lastObject];
    view.frame = CGRectMake(0, 64, ScreenWidth, ScreenHeight);
    [window addSubview:view];
    return view;
}

+ (void)toolStartTimeSecondsCountDown:(UIButton *)sender{
    __block int timeout = 30; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
                sender.userInteractionEnabled = YES;
            });
        }else{
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                //NSLog(@"____%@",strTime);
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1];
                [sender setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
                [UIView commitAnimations];
                sender.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

+ (void)toolStartTimeSecondsCountDown_uilabel:(UILabel *)sender{
    __block int timeout = 30; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                sender.text=@"发送验证码";
                sender.userInteractionEnabled = YES;
            });
        }else{
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                //NSLog(@"____%@",strTime);
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
                sender.text=[NSString stringWithFormat:@"%@秒",strTime];
//                [UIView commitAnimations];
                sender.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

@end
