//
//  XJTSearchBar.h
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XJTSearchBar : UITextField
+ (instancetype)searchBar;
@end
