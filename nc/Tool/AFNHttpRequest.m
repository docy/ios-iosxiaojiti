//
//  AFNHttpRequest.m
//  nc
//
//  Created by docy admin on 15/10/8.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AFNHttpRequest.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
@implementation AFNHttpRequest


+ (void)httpRequestResetUnreadCountWithGroupId:(NSNumber *)groupId{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager POST:[NSString stringWithFormat:ResetUnread,groupId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@0]) { // 发送失败

        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

+ (void)httpRequestSetDataWithUrl:(NSString *)url parameters:(NSMutableDictionary *)parameter{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue] == 0) {
            [ToolOfClass showMessage:@"设置成功!"];
        } else {
            
            [ToolOfClass showMessage:@"设置成功!"];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [ToolOfClass showMessage:@"设置成功!"];
    }];

}

@end
