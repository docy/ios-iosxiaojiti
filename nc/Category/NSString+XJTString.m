//
//  NSString+XJTString.m
//  nc
//
//  Created by docy admin on 15/8/26.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "NSString+XJTString.h"

@implementation NSString (XJTString)

+ (NSString *)stringThrowOffBlankWithString:(NSString *)string
{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@" "];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

+ (NSString *)stringThrowOffLinesWithString:(NSString *)string
{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@"\n"];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

+ (NSString *)stringThrowOffBlankAndLinesWithString:(NSString *)string
{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@" "];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@"\n"];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

@end
