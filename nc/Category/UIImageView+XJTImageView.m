//
//  UIImageView+XJTImageView.m
//  nc
//
//  Created by docy admin on 15/11/3.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "UIImageView+XJTImageView.h"

@implementation UIImageView (XJTImageView)

+ (UIImageView *)imageViewWithImage:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.frame = [[UIScreen mainScreen] bounds];
    return imageView;
}

+ (void)hiddenImageView{
    
}

@end
