//
//  UIColor+XJTColor.h
//  nc
//
//  Created by docy admin on 6/1/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (XJTColor)

+ (UIColor *) colorWithHexString: (NSString *) stringToConvert;

@end
