//
//  UIColor+Hex.h
//  nc
//
//  Created by guanxf on 15/9/11.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHex:(long)hexColor;
+ (UIColor *)colorWithHex:(long)hexColor alpha:(float)opacity;

@end