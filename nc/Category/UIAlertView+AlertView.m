//
//  UIAlertView+AlertView.m
//  nc
//
//  Created by docy admin on 7/14/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "UIAlertView+AlertView.h"

@implementation UIAlertView (AlertView)

+ (void)alertViewWithTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

+ (void)alertViewWithtitle:(NSString *)title target:(id)target
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:target cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title target:(id)target
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:target cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    return alert;
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title subTitle:(NSString *)subTitle target:(id)target
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:subTitle delegate:target cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    return alert;
}

@end
