//
//  NSString+XJTString.h
//  nc
//
//  Created by docy admin on 15/8/26.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XJTString)

+ (NSString *)stringThrowOffBlankWithString:(NSString *)string;
+ (NSString *)stringThrowOffLinesWithString:(NSString *)string;
+ (NSString *)stringThrowOffBlankAndLinesWithString:(NSString *)string;

@end
