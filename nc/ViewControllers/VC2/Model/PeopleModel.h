//
//  PeopleModel.h
//  nc
//
//  Created by docy admin on 6/25/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"

// 联系人model

@class UIImage;
@interface PeopleModel : CommonModel

@property (nonatomic, copy) NSString *name; // 姓名
@property (nonatomic, copy) NSString *nickName; // 姓名
@property (nonatomic, retain) NSNumber *id;
@property (nonatomic, copy) NSString *email; // 邮箱
@property (nonatomic, copy) NSString *tellPhone; // 电话
@property (nonatomic, strong) UIImage *iconImage; // 联系人头像
@property (nonatomic, copy) NSString *iconImageUrl; // 联系人头像URL
@property (nonatomic, assign) BOOL rowSelected; // cell是否被选
@property (nonatomic, assign) BOOL isShowAccessoryView; // 是否显示cell的accessoryView

@property NSInteger sectionNumber;

@end
