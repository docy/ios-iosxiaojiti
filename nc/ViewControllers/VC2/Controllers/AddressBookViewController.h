//
//  AddressBookViewController.h
//  xjt
//
//  Created by docy admin on 8/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 通讯录视图控制器
@interface AddressBookViewController : UIViewController
- (IBAction)AddressBookViewPhoneAddressBook:(id)sender;

@end
