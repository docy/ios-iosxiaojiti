//
//  AddAddressBookViewController.m
//  nc
//
//  Created by docy admin on 15/9/11.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AddAddressBookViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "SVProgressHUD.h"
#import <AFNetworking/AFNetworking.h>

@interface AddAddressBookViewController ()

@end

@implementation AddAddressBookViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.title = @"添加联系人";
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:@"取消" target:self action:@selector(onAddAddressBookViewLeftItemClick)];
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:@"确定" target:self action:@selector(onAddAddressBookViewRightItemClick)];
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    }
    return self;
}

- (void)onAddAddressBookViewLeftItemClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onAddAddressBookViewRightItemClick
{
    NSLog(@"%s",__func__);
    if (self.nameTextFiled.text.length==0||self.phoneTextFiled.text.length==0||self.companyTextFiled.text.length==0||self.zhiweiTextFiled.text.length==0) {
        [SVProgressHUD showErrorWithStatus:@"信息缺失"];
    } else {
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//        
//        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            
//        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpAddAddressBookViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setUpAddAddressBookViewData
{
    
}

@end
