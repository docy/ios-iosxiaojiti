//
//  AddAddressBookViewController.h
//  nc
//
//  Created by docy admin on 15/9/11.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddressBookViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *companyTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTextFiled;


@end
