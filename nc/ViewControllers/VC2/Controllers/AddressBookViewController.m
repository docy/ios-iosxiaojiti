//
//  AddressBookViewController.m
//  xjt
//
//  Created by docy admin on 8/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "AddressBookViewController.h"
#import "AddressBookTableViewController.h"
@interface AddressBookViewController ()

@end

@implementation AddressBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"通讯录";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 手机通讯录
- (IBAction)AddressBookViewPhoneAddressBook:(id)sender {
    AddressBookTableViewController *phoneAB = [[AddressBookTableViewController alloc] init];
    phoneAB.hidesBottomBarWhenPushed = YES;
    phoneAB.navigationItemTitle = @"手机通讯录";
    [self.navigationController pushViewController:phoneAB animated:YES];
}

@end
