//
//  PersonalProfileViewController.m
//  nc
//
//  Created by docy admin on 6/27/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "PersonalProfileViewController.h"
#import "AppDelegate.h"

#import "SwitchCompanyViewController.h"
#import "UserJionedGroupViewController.h"
#import "LoginViewController.h"
#import "XJTNavigationController.h"
#import "PersonalProfileModifyInfoViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "RegisterCreatCompanyViewController.h"

#import "PersonalMessageModel.h"
#import "CompanyModel.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "FixPasswordViewController.h"

@interface PersonalProfileViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    NSString *_authToken; // 用户的authToken
    NSNumber *_userId; // 用户id
}

@property (nonatomic, copy) NSString *avarPath; // 获取上传头像的路径

@end

@implementation PersonalProfileViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        
        self.avarPath = [NSString string];
        self.navigationItem.title = @"设置";
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        _authToken = [info objectForKey:@"authToken"];
        _userId = [info objectForKey:@"id"];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _userId = [info objectForKey:@"id"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置基本属性
    [self setUpPersonalProfileViewdata];
    
    // 网络获取个人信息
    [self getPersonalProfileViewPersonalInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark setUpData

// 设置基本属性
- (void)setUpPersonalProfileViewdata{
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:nil];
    [self.navigationItem setHidesBackButton:YES];
    self.scrollerView.contentSize = CGSizeMake(0, [[UIScreen mainScreen] bounds].size.height);
//     切换公司通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPersonalProfileViewPersonalInfo) name:@"SwitchCurrentCompany" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PersonalProfileViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    //切换账号登录之后
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newUserLogin) name:@"newUserLogin" object:nil];
}

-(void)StartingFromTheNotification:(NSNotification*)notifi{
    [self.tabBarController setSelectedIndex:0];
}

- (void)PersonalProfileViewInGoodNetWork{
    if (self.personalNameLabel.text.length==0) {
        [self getPersonalProfileViewPersonalInfo];
    }
}

-(void) newUserLogin{
    [self getPersonalProfileViewPersonalInfo];
}

#pragma mark getUpData

// 网络获取个人信息
- (void)getPersonalProfileViewPersonalInfo{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _authToken = [info objectForKey:@"authToken"];
    _userId = [info objectForKey:@"id"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = _authToken;
    
    NSString *str = [personalInfoPath stringByAppendingString:[NSString stringWithFormat:@"%@",_userId]];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
            [model setValuesForKeysWithDictionary:responseObject[@"data"]];
           // 显示数据
            [self showPersonalProfileViewUserDataWithModel:model];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
#pragma mark showData
// 显示个人资料
- (void)showPersonalProfileViewUserDataWithModel:(PersonalMessageModel *)model{
    self.personalNickNameLabel.text = model.nickName;
    self.personalNameLabel.text = model.name;
    [self.personalIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]]];
    if ([model.sex intValue]==0) {
//        [self.sexButton setTitle:@"女" forState:UIControlStateNormal];
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_women_icon"] forState:UIControlStateNormal];
    } else {
//        [self.sexButton setTitle:@"男" forState:UIControlStateNormal];
        
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_men_icon"] forState:UIControlStateNormal];
    }
    self.companyPhoneLabel.text = model.phone;
    self.companyEmailLabel.text = model.email;
    self.companyNameLabel.text = model.companyName;
}

#pragma mark 按钮点击事件
// 更改昵称
- (IBAction)onModifityNickNameBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.personalNickNameLabel.text;
    infoVC.itemTitle = @"昵称";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.personalNickNameLabel.text = modifyText;
    };
    
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
}
// 退出按钮
- (IBAction)onLogoutBtnClick:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"退出当前账号?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"退出", nil];
    [alert show];
    
}

- (IBAction)onModifityCompanyPhoneBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyPhoneLabel.text;
    infoVC.itemTitle = @"电话";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyPhoneLabel.text = modifyText;
    };
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onModifityCompanyEmailBtnClick:(id)sender {
    
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyEmailLabel.text;
    infoVC.itemTitle = @"邮箱";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyEmailLabel.text = modifyText;
    };
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
    
}

- (IBAction)onFixPasswordBtnClick:(id)sender {
    
    FixPasswordViewController *fix = [[FixPasswordViewController alloc] initWithNibName:@"FixPasswordViewController" bundle:[NSBundle mainBundle]];
    fix.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fix animated:YES];
    
}

// 加入的小组
- (IBAction)PersonalProfileViewGetUpUserJionedGroup:(id)sender {
    UserJionedGroupViewController *joinGroupVC = [[UserJionedGroupViewController alloc] init];
    joinGroupVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:joinGroupVC animated:YES];
}

// 选择个人头像
- (IBAction)onSelectePersonalIconBtnClick:(id)sender {
//    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
//    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    pickerVC.allowsEditing = YES;
//    pickerVC.delegate = self;
//    [self presentViewController:pickerVC animated:YES completion:nil];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册中选取",@"拍照", nil];
    [actionSheet showInView:self.view];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        
        [manager POST:logoutPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) { // 退出程序
                
                [info setObject:@"YES" forKey:@"isLogout"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postLoginVC" object:nil];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

#pragma mark UIimagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
//    NSLog(@"info=%@",info);
    if (picker.sourceType==UIImagePickerControllerSourceTypePhotoLibrary) {
        self.personalIconImageView.image = info[UIImagePickerControllerEditedImage];
        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
        NSString *fileName = [url lastPathComponent];
        // 上传图片文件
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
//
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {

        self.personalIconImageView.image = info[UIImagePickerControllerOriginalImage];
        NSString *fileName = @"origin.jpg";
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex == 2) {
        return;
    }
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
    } else if (buttonIndex==1){
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
