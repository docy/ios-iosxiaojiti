//
//  PersonalProfileViewController.m
//  nc
//
//  Created by docy admin on 6/27/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "PersonalProfileViewController.h"

#import "SwitchCompanyViewController.h"
#import "UserJionedGroupViewController.h"
#import "LoginViewController.h"
#import "XJTNavigationController.h"
#import "PersonalProfileModifyInfoViewController.h"

#import "PersonalMessageModel.h"
#import "CompanyModel.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "APIHeader.h"

@interface PersonalProfileViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    NSString *_authToken; // 用户的authToken
    NSNumber *_userId; // 用户id
}

@property (nonatomic, copy) NSString *avarPath; // 获取上传头像的路径

@end

@implementation PersonalProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置基本属性
    [self setUpPersonalProfileViewdata];
    
    // 获取当前用户的公司
//    [self getPersonalProfileViewCurrentCompany];
    // 网络获取个人信息
    [self getPersonalProfileViewPersonalInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark setUpData

// 设置基本属性
- (void)setUpPersonalProfileViewdata
{
    self.avarPath = [NSString string];
    self.navigationItem.title = @"设置";
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _authToken = [info objectForKey:@"authToken"];
    _userId = [info objectForKey:@"id"];
    self.scrollerView.contentSize = CGSizeMake(0, [[UIScreen mainScreen] bounds].size.height);
}

#pragma mark getUpData
// 获取当前用户的公司
//- (void)getPersonalProfileViewCurrentCompany
//{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//    parameter[@"accessToken"] = _authToken;
//    [manager GET:CurrentCompanyPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if ([responseObject[@"code"] isEqualToNumber:@0]) {
////            NSLog(@"%@",responseObject);
//            NSDictionary *data = responseObject[@"data"];
//            CompanyModel *model = [[CompanyModel alloc] init];
//            [model setValuesForKeysWithDictionary:data];
//
//            
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
//}

// 网络获取个人信息
- (void)getPersonalProfileViewPersonalInfo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = _authToken;
    
    NSString *str = [personalInfoPath stringByAppendingString:[NSString stringWithFormat:@"%@",_userId]];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
            [model setValuesForKeysWithDictionary:responseObject[@"data"]];
           // 显示数据
            [self showPersonalProfileViewUserDataWithModel:model];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
#pragma mark showData
// 显示个人资料
- (void)showPersonalProfileViewUserDataWithModel:(PersonalMessageModel *)model
{
    self.personalNickNameLabel.text = model.nickName;
    self.personalNameLabel.text = model.name;
    [self.personalIconImageView setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]]];
    if ([model.sex intValue]==0) {
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_women"] forState:UIControlStateNormal];
    } else {
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_men"] forState:UIControlStateNormal];
    }
    self.companyPhoneLabel.text = model.phone;
    self.companyEmailLabel.text = model.email;
}

#pragma mark 按钮点击事件
// 更改昵称
- (IBAction)onModifityNickNameBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.personalNickNameLabel.text;
    infoVC.itemTitle = @"昵称";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.personalNickNameLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}
// 退出按钮
- (IBAction)onLogoutBtnClick:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"退出当前账号?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"退出", nil];
    [alert show];
}
// 切换公司
- (IBAction)selectedCurrentCompanyBtnClick:(id)sender {
    SwitchCompanyViewController *switchVC = [[SwitchCompanyViewController alloc] init];
    switchVC.companyName = self.personalCompanyNameLabel.text;
    switchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:switchVC animated:YES];
}

- (IBAction)onModifityCompanyPhoneBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyPhoneLabel.text;
    infoVC.itemTitle = @"电话";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyPhoneLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onModifityCompanyEmailBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyEmailLabel.text;
    infoVC.itemTitle = @"邮箱";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyEmailLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onModifityCompanyZhiWeiBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyZhiWeiLabel.text;
    infoVC.itemTitle = @"职位";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyZhiWeiLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

// 加入的小组
- (IBAction)PersonalProfileViewGetUpUserJionedGroup:(id)sender {
    UserJionedGroupViewController *joinGroupVC = [[UserJionedGroupViewController alloc] init];
    joinGroupVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:joinGroupVC animated:YES];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        
        [manager POST:logoutPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) { // 退出程序
                
                [info setObject:@"YES" forKey:@"isLogout"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                XJTNavigationController *nav = [[XJTNavigationController alloc] initWithRootViewController:[LoginViewController new]];
                self.view.window.rootViewController = nav;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

#pragma mark UITextFieldDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.personalIconImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    // 上传图片文件
    [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
// 选择个人头像
- (IBAction)onSelectePersonalIconBtnClick:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

@end
