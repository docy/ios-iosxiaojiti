//
//  PersonalProfileViewController.h
//  nc
//
//  Created by docy admin on 6/27/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalProfileViewController : UIViewController
// 个人
@property (weak, nonatomic) IBOutlet UIImageView *personalIconImageView; // 个人头像
@property (weak, nonatomic) IBOutlet UILabel *personalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalNickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *sexButton;

// 公司
@property (weak, nonatomic) IBOutlet UILabel *personalCompanyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyZhiWeiLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;

// 个人
- (IBAction)onSelectePersonalIconBtnClick:(id)sender;
- (IBAction)onModifityNickNameBtnClick:(id)sender;
- (IBAction)PersonalProfileViewGetUpUserJionedGroup:(id)sender;

- (IBAction)onLogoutBtnClick:(id)sender;
// 公司
- (IBAction)selectedCurrentCompanyBtnClick:(id)sender;
- (IBAction)onModifityCompanyPhoneBtnClick:(id)sender;
- (IBAction)onModifityCompanyEmailBtnClick:(id)sender;
- (IBAction)onModifityCompanyZhiWeiBtnClick:(id)sender;




@end
