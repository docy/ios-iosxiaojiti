//
//  ResetPasswordViewController.m
//  nc
//
//  Created by docy admin on 15/10/21.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"重置密码";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(setUpResetPasswordViewBack)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpResetPasswordViewBack)];
}

- (void)setUpResetPasswordViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)onGetUpidentifyBtnClick:(id)sender {
    if (self.phoneTF.text.length==0) {
        [ToolOfClass showMessage:@"手机号不能为空"];
    } else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneTF.text;
        
        [manager POST:ResetPasswordSms parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

            if ([responseObject[@"code"] intValue] == 0) {
                
                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyBtn];
                
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (IBAction)onResetPassTiJiaoBtnClick:(id)sender {
    
    if (self.identifyTF.text.length==0) {
        [ToolOfClass showMessage:@"请输入验证码"];
    } else if (self.passwordTF.text.length==0) {
        [ToolOfClass showMessage:@"请输入密码"];
    } else {
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        
        parameter[@"phone"] = self.phoneTF.text;
        parameter[@"code"] = self.identifyTF.text;
        parameter[@"password"] = self.passwordTF.text;
        
        [manager POST:ResetPassword parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] intValue] == 0) {
                [ToolOfClass showMessage:@"重置成功!"];
                [info setObject:self.passwordTF.text forKey:@"password"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (IBAction)onSetUpSecureTextEntryBtnClicl:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    self.passwordTF.secureTextEntry = !self.passwordTF.secureTextEntry;
}


//-(void)resetPassStartTimeSecondsCountDown:(UIButton *)sender{
//    __block int timeout = 30; //倒计时时间
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
//    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
//    dispatch_source_set_event_handler(_timer, ^{
//        if(timeout<=0){ //倒计时结束，关闭
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
//                sender.userInteractionEnabled = YES;
//            });
//        }else{
//            int seconds = timeout % 60;
//            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                //NSLog(@"____%@",strTime);
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
//                [sender setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
//                [UIView commitAnimations];
//                sender.userInteractionEnabled = NO;
//            });
//            timeout--;
//        }
//    });
//    dispatch_resume(_timer);
//}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.passwordTF resignFirstResponder];
    [self.identifyTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}

@end
