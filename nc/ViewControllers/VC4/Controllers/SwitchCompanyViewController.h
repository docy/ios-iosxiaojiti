//
//  SwitchCompanyViewController.h
//  nc
//
//  Created by docy admin on 7/3/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
// 切换公司视图控制器
@interface SwitchCompanyViewController : UITableViewController

@property (nonatomic, copy) NSString *companyName; // 公司名称


@end
