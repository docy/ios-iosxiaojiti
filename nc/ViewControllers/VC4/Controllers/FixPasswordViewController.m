//
//  FixPasswordViewController.m
//  nc
//
//  Created by docy admin on 15/10/20.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "FixPasswordViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "UIAlertView+AlertView.h"
#import "ResetPasswordViewController.h"

@interface FixPasswordViewController ()

@end

@implementation FixPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改密码";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(setUpFixPasswordViewBack)];
}

- (void)setUpFixPasswordViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onFixPasswordViewTiJiaoBtnClick:(id)sender {
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSString *password = self.newpass.text;
    
    if (self.oldPasswordTF.text.length==0) {
        [UIAlertView alertViewWithTitle:@"请输入旧密码"];
    } else if (password.length < 6) {
        [UIAlertView alertViewWithTitle:@"新密码至少需要6位"];
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        parameter[@"password"] = password;
        
        parameter[@"oldpassword"] = self.oldPasswordTF.text;
        [manager POST:UpdateUserInfo parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                
                [ToolOfClass showMessage:@"修改成功!"];
                [info setObject:password forKey:@"password"];
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:@"保存失败!"];
        }];
    }
}

- (IBAction)onResetPasswordBtnClick:(id)sender {
    ResetPasswordViewController *resetPass = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:resetPass animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.oldPasswordTF resignFirstResponder];
    [self.newpass resignFirstResponder];
}

@end
