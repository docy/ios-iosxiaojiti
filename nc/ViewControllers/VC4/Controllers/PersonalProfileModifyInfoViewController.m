//
//  PersonalProfileModifyInfoViewController.m
//  nc
//
//  Created by docy admin on 15/8/27.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "PersonalProfileModifyInfoViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "NSString+XJTString.h"
#import "UIAlertView+AlertView.h"

@interface PersonalProfileModifyInfoViewController ()

@end

@implementation PersonalProfileModifyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self SetUpPersonalProfileModifyInfoViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)SetUpPersonalProfileModifyInfoViewData
{
    self.navigationItem.title = self.itemTitle;
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:@"保存" target:self action:@selector(onPersonalProfileModifyInfoViewSaveBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:@"取消" target:self action:@selector(onPersonalProfileModifyInfoViewBackBtnClick)];
    self.modifyTextField.text = self.modifyText;
}
#pragma mark 按钮点击事件
- (void)onPersonalProfileModifyInfoViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onPersonalProfileModifyInfoViewSaveBtnClick
{
     NSString *text = [NSString stringThrowOffBlankWithString:self.modifyTextField.text];
    if (text.length==0) {
        [ToolOfClass showMessage:[NSString stringWithFormat:@"%@不能为空!",self.itemTitle]];
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        if ([self.itemTitle isEqualToString:@"昵称"]) {
            if (text.length>15) {
                [UIAlertView alertViewWithTitle:@"昵称控制在15个字以内"];
                return ;
            }else{
                 parameter[@"nickName"] = text;
            }
        } else if ([self.itemTitle isEqualToString:@"电话"]) {
            parameter[@"phone"] = text;
        } else if ([self.itemTitle isEqualToString:@"邮箱"]) {
            parameter[@"email"] = text;
        } else if ([self.itemTitle isEqualToString:@"小组名称"]) {
            parameter[@"name"] = text;
        } else if ([self.itemTitle isEqualToString:@"小组介绍"]) {
            parameter[@"desc"] = text;
        }
        NSString *strPath = nil;
        if (self.groupId != nil) {
            
            strPath = [NSString stringWithFormat:UpdateGroupInfo,self.groupId];
            
        } else {
            strPath = [UpdateUserInfo copy];
        }
        
        [manager POST:strPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                [ToolOfClass showMessage:@"保存成功!"];
                if ([self.itemTitle isEqualToString:@"昵称"]) {
                    [info setObject:text forKey:@"nickName"];
                }
                if (self.modifyBlock) {
                    self.modifyBlock(text);
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:@"保存失败!"];
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    self.hidesBottomBarWhenPushed = YES;
}

@end
