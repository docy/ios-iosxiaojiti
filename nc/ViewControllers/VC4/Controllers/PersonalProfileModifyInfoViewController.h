//
//  PersonalProfileModifyInfoViewController.h
//  nc
//
//  Created by docy admin on 15/8/27.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^modifyBlock)(NSString *);

@interface PersonalProfileModifyInfoViewController : UIViewController

@property (nonatomic, copy) NSString *modifyText;
@property (nonatomic, copy) NSString *itemTitle;
@property (weak, nonatomic) IBOutlet UITextField *modifyTextField;

@property (nonatomic, strong) modifyBlock modifyBlock;
@property (nonatomic, retain) NSNumber *groupId;

@end
