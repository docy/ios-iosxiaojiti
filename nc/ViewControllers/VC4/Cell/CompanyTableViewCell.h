//
//  CompanyTableViewCell.h
//  nc
//
//  Created by docy admin on 15/9/9.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImView;

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *setCurrentCpyBtn;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UILabel *line;


@end
