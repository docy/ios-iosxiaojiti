//
//  SubTopicViewCell.h
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubTopicViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *creatAtLabel;


@end
