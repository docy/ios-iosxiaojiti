//
//  MessageVoteDetailHeaderView.h
//  nc
//
//  Created by guanxf on 15/9/9.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLEmojiLabel.h"
@interface MessageVoteDetailHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *creatAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet MLEmojiLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *subImageView;


@property (weak, nonatomic) IBOutlet UIView *BtnBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *collectButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UITableView *headVoteTableView;
@property (weak, nonatomic) IBOutlet UIButton *sendVoteButton;
@property (weak, nonatomic) IBOutlet UIButton *allVoteButton;
@property (weak, nonatomic) IBOutlet UIButton *closeVoteButton;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UIView *alertview;

@end
