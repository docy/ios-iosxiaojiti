//
//  SubTopicViewController.m
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SubTopicViewController.h"
#import "MessageDetailViewController.h"

#import "SubTopicViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "SubTopicModel.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
//#import "SVProgressHUD.h"


#define SubTopicViewCellHeight 70
@interface SubTopicViewController ()

@property (nonatomic, assign) int subTopicPage;
@property (nonatomic, retain) UIImageView *bgSubTpView;

@end

@implementation SubTopicViewController

- (UIImageView *)bgSubTpView{
    if (_bgSubTpView == nil) {
        _bgSubTpView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"none_topic"]];
    }
    return _bgSubTpView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpSubTopicViewControllerAttribute];
    [self getUpTopicListWithMJFresh];
    
    // 获取子话题列表
    [self getUpTopicList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSubTopicViewNewMessage:) name:@"newMessage" object:nil];
    // 取消关注
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccess:) name:@"deleteTopicSuccess" object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deleteTopicSuccess:(NSNotification *)not{
    for (NSInteger i = 0; i < self.subTopicListData.count; i++) {
        SubTopicModel *model = self.subTopicListData[i];
        if ([not.object isEqualToNumber:model.id]) {
            [self.subTopicListData removeObject:model];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
    if (self.subTopicListData.count == 0) {
        self.tableView.backgroundView = self.bgSubTpView;
    }
}

- (void)getSubTopicViewNewMessage:(NSNotification *)notification
{
    NSArray *array = notification.object;
    NSDictionary * dict=array[0];
    
    if ([dict[@"type"] intValue] == 8) {
        for (int i = 0;i < self.subTopicListData.count;i++) {
            SubTopicModel *mode=self.subTopicListData[i];
            if (mode.id.intValue ==[(NSNumber*)dict[@"info"][@"topicId"] intValue]) { // 刷新message
                NSMutableDictionary *com = [NSMutableDictionary dictionary];
                com[@"createdAt"] = dict[@"updatedAt"];
                com[@"creatorAvatar"] = dict[@"info"][@"creatorAvatar"];
                com[@"creatorName"] = dict[@"info"][@"creatorName"];
                if (dict[@"info"][@"message"]==nil) {
                    com[@"message"] = @"";
                } else {
                    com[@"type"] = @1;
                    com[@"message"] = dict[@"info"][@"message"];
                }
//                com[@"message"] = dict[@"info"][@"message"];
                com[@"nickName"] = dict[@"nickName"];
                mode.comments = @[com];
                [self.subTopicListData replaceObjectAtIndex:i withObject:mode];
            
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第二行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
                if (i!=0) {
                    [self.tableView moveRowAtIndexPath:te toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [self.subTopicListData insertObject:mode atIndex:0];
                    [self.subTopicListData removeObjectAtIndex:i+1];
                }
                break;
            }
        }
    }
}

// 设置基本属性
- (void)setUpSubTopicViewControllerAttribute
{
    self.navigationItem.title = [self.groupName stringByAppendingString:@"-子话题"];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onSubTopicViewControllerNavBackClick)];
    self.subTopicListData = [NSMutableArray array];
    self.subTopicPage = 1;
}
- (void)onSubTopicViewControllerNavBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpTopicListWithMJFresh
{
    
    [self.subTopicListData removeAllObjects];
    __block SubTopicViewController *weakSelf = self;
    [self.tableView addFooterWithCallback:^{
        weakSelf.subTopicPage++;
        [weakSelf getUpTopicList];
    }];
}

// 获取子话题列表
- (void)getUpTopicList
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString *str = [NSString stringWithFormat:getSubTopicListPath,self.groupId,self.subTopicPage];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            
//            NSLog(@"%@",responseObject);
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.subTopicListData addObject:model];
            }
            
            if (self.subTopicListData.count==0) {
                self.tableView.backgroundView = self.bgSubTpView;
            } else {
                self.tableView.backgroundView = nil;
            }

            
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.subTopicListData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    SubTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SubTopicViewCell" owner:self options:nil] lastObject];
    }
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    
    cell.titleLabel.text = model.title;
    
    if ([model.type intValue] == 2) { // 图片子话题
        [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else if ([model.type intValue] == 3){
        cell.iconImageView.image = [UIImage imageNamed:@"list_map"];
    } else if ([model.type intValue] == 4){
        cell.iconImageView.image = [UIImage imageNamed:@"list_vote"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"list_topic"];
    }
    if (model.comments.count!=0) {
        NSDictionary *comment = model.comments[0];
        
        NSString *text = nil;
        if ([comment[@"type"] isEqualToNumber:@1]) {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        } else {
            text = [NSString stringWithFormat:@"%@：发表图片",comment[@"nickName"]];
        }
//        text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        NSMutableAttributedString *attributsStr = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:[NSString stringWithFormat:@"%@：",comment[@"nickName"]]];
        [attributsStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:range];
        
//        [attributsStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] range:range];
        cell.descLabel.attributedText = attributsStr;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:comment[@"createdAt"]];
    } else {
        
        cell.descLabel.text = nil;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.updatedAt];
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return SubTopicViewCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageDetailViewController *detailVC = [[MessageDetailViewController alloc] init];
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    detailVC.topicId = model.id;
    detailVC.topicType = model.type;
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
