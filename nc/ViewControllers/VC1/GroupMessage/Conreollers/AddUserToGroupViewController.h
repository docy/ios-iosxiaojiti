//
//  AddUserToGroupViewController.h
//  nc
//
//  Created by docy admin on 15/10/22.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^viewBlock)(void);

@interface AddUserToGroupViewController : UITableViewController

@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, strong) viewBlock viewBlock;


@property (nonatomic, retain) NSArray *currentUsers;

@end
