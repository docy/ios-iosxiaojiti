//
//  ShowPictureViewController.h
//  nc
//
//  Created by docy admin on 6/26/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 消息聊天界面中的图片的点击查看视图控制器

@interface ShowPictureViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

//@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSDictionary *imageInfo;

//@property (strong, nonatomic) NSURL *imageURL;

@end
