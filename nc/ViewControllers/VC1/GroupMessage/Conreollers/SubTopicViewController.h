//
//  SubTopicViewController.h
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubTopicViewController : UITableViewController

// 群组的子话题页面
@property (nonatomic,retain) NSMutableArray *subTopicListData;

@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, retain) NSNumber *groupId;

@end
