//
//  MessageViewController.h
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SLKTextViewController.h"
#import "MessageViewController.h"

// 群组消息聊天视图控制器

typedef void(^cellBlock)(void);
//typedef void (^cellBackNav)(void);

@interface MessageViewController : SLKTextViewController <UIActionSheetDelegate>

@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，
@property (nonatomic, copy) NSString *groupName; // 群组名称

@property (nonatomic, retain) NSMutableArray *messagesData; // 消息数组

@property (nonatomic, retain) NSMutableArray *users; // 用户名

//@property (nonatomic, retain) NSMutableArray *usersAvatar; // 用户头像数组
@property (nonatomic, retain) NSArray *channels;
@property (nonatomic, retain) NSArray *emojis;
@property (nonatomic, retain) NSArray *searchResult; // 查询结果数组

@property (nonatomic, copy) NSString *lastTimeString;

@property (nonatomic, retain) NSMutableArray *groupUsers; // 群组成员，存储model

@property (nonatomic, strong) cellBlock cellBlock;

//@property (nonatomic, strong) cellBackNav cellBackNav;


@property (nonatomic, retain) NSNumber *unreadCount;
@property (nonatomic, assign) int broadcast;

//@property (nonatomic, retain) NSNumber *groupCreator; // 群组创建者


@end