//
//  MessageDetailViewController.h
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SLKTextViewController.h"
#import "MessageModel.h"
@interface MessageDetailViewController : SLKTextViewController

// 消息详情视图控制器
//@property (nonatomic, retain) MessageModel *message;
@property (nonatomic, retain) NSNumber *topicId;
@property (nonatomic, retain) NSNumber *topicType;
@property (nonatomic, retain) NSNumber *createTopicUserID;

@end
