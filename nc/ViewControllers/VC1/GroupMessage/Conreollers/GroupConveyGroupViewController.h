//
//  GroupConveyGroupViewController.h
//  nc
//
//  Created by docy admin on 15/10/16.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^cannotDisbandBlock)(NSNumber *);

@interface GroupConveyGroupViewController : UITableViewController

@property (nonatomic ,retain) NSNumber *groupId;

@property (nonatomic, strong) cannotDisbandBlock disbandBlock;

@end
