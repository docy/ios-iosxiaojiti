//
//  DirectChatViewController.h
//  nc
//
//  Created by docy admin on 15/9/19.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "SLKTextViewController.h"

typedef void(^cellBlock)(void);

@interface DirectChatViewController : SLKTextViewController

@property (nonatomic, retain) NSNumber *directChatId;

@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, copy) NSString *groupName;

@property (nonatomic, strong) cellBlock cellBlock;

@end
