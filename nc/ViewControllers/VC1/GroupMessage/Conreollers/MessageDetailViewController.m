//
//  MessageDetailViewController.m
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MessageDetailViewController.h"
#import "XJTMessageTableViewCell.h"
#import "CommentTableViewCell.h"
#import "MessVoteTableViewCell.h"
#import "MessageDetailHeaderView.h"
#import "MessageVoteDetailHeaderView.h"
#import "ShowPictureViewController.h"
#import "UIColor+Hex.h"
#import "UIColor+XJTColor.h"
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "VoteUserListViewController.h"
#import "MapViewController.h"
#import "UMSocial.h"
#import "NSString+XJTString.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "SVProgressHUD.h"
#import "MLEmojiLabel.h"

//#import "SVProgressHUD.h"

#define defaultSpaceWith 16 // cell的子控件距离cell边缘的默认间距


// cell的重用标识符
static NSString *MessengerCellIdentifier = @"MessengerCellId";

@interface MessageDetailViewController () <UIAlertViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate>

@property (nonatomic, retain) NSMutableArray *messageData; // 评论数据
@property (nonatomic, retain) NSMutableArray *VoteDataArray; // 投票选项列表
@property (nonatomic, retain) NSMutableDictionary *VoteDataDic; // 投票选项列表
@property (nonatomic, assign) CGFloat cellHeight; // cell的高度(自动计算)
@property (nonatomic, retain) MessageDetailHeaderView *headerView; //tableView的头视图
@property (nonatomic, retain) MessageVoteDetailHeaderView *voteHeaderView; //tableView的vote头视图
@property (nonatomic, retain) NSMutableArray *headerViewData; //tableView的头视图数据
@property (nonatomic, assign) NSNumber* resouceId;  //子话题接口ID
@property (nonatomic, assign) NSNumber* closed;  //投票是否关闭
@property (nonatomic, assign) NSNumber* voted;  //投票当前用户是否参与过
@property (nonatomic, assign) NSNumber* anonymous;  //投票是否匿名
@property (nonatomic, assign) NSNumber* single;     //投票是否单选
@property (nonatomic, retain) NSArray * ColorStrArray; //投票子话题cell进度条颜色数组



@property (nonatomic, strong) NSMutableArray * cellHeightArray;
@end

@implementation MessageDetailViewController
{
    
    CGFloat _kCellBodyLabelWidth;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _ColorStrArray=[[NSArray alloc] initWithObjects:@"#87D1D0",@"#C1DCB9",@"F5869C",@"FCE0E9",@"#02B9CE",@"#F6B3BC",@"#FEE9BC",@"F2C486",@"B8E6FE",@"8CDBCD", nil];
    
    NSLog(@"topic type is %d",[self.topicType intValue]);
    // 设置基本属性
    [self setUpMessageDetailViewAttribute];
    // 获取评论列表
    [self getTopicComments];
}

// 设置基本属性
- (void)setUpMessageDetailViewAttribute{
    
    self.navigationItem.title = @"主题";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = YES;
    self.typingIndicatorView.canResignByTouch = YES;
    
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    
    self.cellHeightArray=[NSMutableArray array];
    self.messageData = [NSMutableArray array];
    self.VoteDataArray=[NSMutableArray array];
    self.VoteDataDic=[[NSMutableDictionary alloc] init];
    self.headerViewData = [NSMutableArray array];
    self.cellHeight = 0;
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(MessageDetailViewBackButtonClick)];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(MessageDetailViewBackButtonClick)];
    [self.rightButton setTitle:NSLocalizedString(@"发送", nil) forState:UIControlStateNormal];
//    [self setInverted:NO]; // 设置tableView的翻转模式（从上往下显示）
    self.inverted = NO;
    
    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.textInputbar.leftButton setImage:image forState:UIControlStateNormal];
    
    // 设置tableView的headerView
    if ([_topicType isEqualToNumber:@4]) {//设置投票的headview
        MessageVoteDetailHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"MessageVoteDetailHeaderView" owner:self options:nil] lastObject];
        self.voteHeaderView = headerView;
        [self.voteHeaderView.shareButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.voteHeaderView.collectButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.voteHeaderView.deleteButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        MessageDetailHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"MessageDetailHeaderView" owner:self options:nil] lastObject];
        self.headerView = headerView;
    }
    
    [self.headerView.shareButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.collectButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.deleteButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 注册通知观察者(发表的评论)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMessageDetailViewNewMessage:) name:@"newMessage" object:nil];
}

// 返回按钮的点击事件
- (void)MessageDetailViewBackButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

// 刷新评论
- (void)getMessageDetailViewNewMessage:(NSNotification *)notification{
    NSArray *array = notification.object;
    
    for (NSDictionary *dict in array) {
        if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到 url链接 为 预加载内容 时
            NSString *clientId = dict[@"info"][@"clientId"];
            for (long i=0; i<_messageData.count; i++) {
                MessageModel *model=_messageData[i];
                if ([clientId isEqualToString:model.info[@"clientId"]]) {
                    MessageModel *mdol=[[MessageModel alloc] init];
                    [mdol setValuesForKeysWithDictionary:dict];

                    [_messageData replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                    NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第i行
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                    return;
                }
            }
        }
        
        if ([dict[@"type"] isEqualToNumber:@8]&&[dict[@"info"][@"topicId"] isEqualToNumber:self.topicId]) { // 说明是评论message
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            [self.messageData addObject:model];
            
            [self.tableView reloadData];
            
            // tableView滚动到最下面
            [self setUpMessageDetailViewTableViewScroller];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return _VoteDataArray.count;
    }
    
    return self.messageData.count;
}

//投票cell选中 button 事件
-(void)cellSelectButtonClick:(UIButton*)button{
    long tag=button.tag;
    NSLog(@"button.tag=%ld",tag);
    
    if (![_single isEqualToNumber:@0]) {
        for (int i=0; i<_VoteDataArray.count; i++) {
            if (tag==i) {
                [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d_State",i]];
            }else{
                [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
            }
        }
    }else{
        if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",tag]] isEqualToString:@"1"]) {
            [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
        }else{
            [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
        }
    }
    
    [self.voteHeaderView.headVoteTableView reloadData];
//    MessVoteTableViewCell * cell=[button superview];
//    cell.selectImg=[UIImage imageNamed:@"choose_selected"];
//    self.tableView.tableHeaderView.headVoteTableView
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==100) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        if ([_closed isEqualToNumber:@1]||[_voted isEqualToNumber:@1]) {
            return;
        }
        NSInteger tag=indexPath.row;
        if (![_single isEqualToNumber:@0]) {
            for (int i=0; i<_VoteDataArray.count; i++) {
                if (tag==i) {
                    [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d_State",i]];
                }else{
                    [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
                }
            }
//            [self.voteHeaderView.headVoteTableView reloadData];
        }else{
            if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",tag]] isEqualToString:@"1"]) {
                [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
            }else{
                [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
            }
//            [self.voteHeaderView.headVoteTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        [self.voteHeaderView.headVoteTableView reloadData];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     
//    XJTMessageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
//        
//        cell.userInteractionEnabled = NO;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    
//    cell.detailButton.hidden = YES; // 隐藏详情按钮
//    
//    MessageModel *messageModel = self.messageData[indexPath.row];
//    
//    cell.senderLabel.text = messageModel.sender;
//    NSString *str = [iconPath stringByAppendingString:messageModel.avatar];
//    [cell.userAvatarImageView setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
//    // 设置时间显示格式
//    
//    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:messageModel.updatedAt];
//    
//    CGRect senderFrame = cell.senderLabel.frame;
//    CGRect avatarFrame = cell.userAvatarImageView.frame;
//    CGRect creatAtFrame = cell.createdAtLabel.frame;
//    cell.timeView.hidden = YES;
//    cell.responseLabel.hidden = YES;
//    
//    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
//    senderFrame.size.width = ceil(senderSize.width);
//    
//    senderFrame.origin.y = defaultSpaceWith;
//    avatarFrame.origin.y = defaultSpaceWith;
//    creatAtFrame.origin.y = defaultSpaceWith;
//    cell.senderLabel.frame = senderFrame;
//    
//    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
//    cell.userAvatarImageView.frame = avatarFrame;
//    cell.createdAtLabel.frame = creatAtFrame;
//    
//    
//    NSDictionary *info = messageModel.info;
//    // 设置属性来设置行高
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineSpacing = 3;
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
//    // label的宽度
//     CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 16;
//    
//    // 主题存在（消息为topic）
//    if ([messageModel.type isEqualToNumber:@7]) { // 此model为topic
//        
//        // 设置themeLabel
//        cell.themeLabel.text = [NSString stringWithFormat:@"#%@#",info[@"title"]];
//        cell.themeLabel.hidden = NO;
//        CGRect themeFrame = cell.themeLabel.frame;
//        themeFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//        themeFrame.size.width = width;
//        // 设置行高
//        CGRect themeRect = CGRectMake(0, 0, 0, 0);
//        if (cell.themeLabel.text.length==0) {
//            themeRect = [cell.themeLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//        } else {
//            cell.themeLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.themeLabel.text attributes:attributes];
//            themeRect = [cell.themeLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        }
//        themeFrame.size.height = ceil(themeRect.size.height);
//        cell.themeLabel.frame = themeFrame;
//        
//    } else { // 此model为comment
//        cell.themeLabel.hidden = YES;
//        cell.themeLabel.text = nil;
//    }
//    
//    // 定制bodyLabel
//    CGRect frame = cell.bodyLabel.frame;
//    frame.size.width = width;
//    
//    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
//    if ([messageModel.type isEqualToNumber:@7]) {
//        cell.bodyLabel.text = info[@"desc"];
//        frame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
//    } else {
//        cell.bodyLabel.text = info[@"message"];
//        frame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//    }
//    if (cell.bodyLabel.text.length==0) {
//        bodyFrame = [cell.bodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.bodyLabel.text attributes:attributes];
//        bodyFrame = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        
//    }
//
//    frame.size.height = ceil(bodyFrame.size.height);
//    cell.bodyLabel.frame = frame;
//    
//    // 存储高度，用于返回cell的高度
//    CGFloat cellHeight = 0;
//    if (frame.size.height==0) {
//        cellHeight = CGRectGetMaxY(cell.senderLabel.frame)+ 21;
//    } else {
//        cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
//    }
//    self.cellHeight = cellHeight;
//    
//    // 定制subImageView(子话题图片)
//    if (![messageModel.type isEqualToNumber:@7]) {
//        cell.subImageView.hidden = YES;
//        cell.subImageView.image = nil;
//    } else {
//        cell.subImageView.userInteractionEnabled = YES;
//        cell.subImageView.hidden = NO;
//        CGRect subFrame = cell.subImageView.frame;
//        subFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
//        cell.subImageView.frame = subFrame;
//        //        cell.subImageView.image = message.subImage;
//        cell.userInteractionEnabled = YES;
//        
//        // 存储高度，用于返回cell的高度
//        //        float cellHeight = CGRectGetMaxY(cell.subImageView.frame);
//        //        self.cellHeight = cellHeight;
//    }

    */
    
    if (tableView.tag==100) {//投票子话题里tableview
        MessVoteTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MessVoteTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MessVoteTableViewCell" owner:self options:nil] lastObject];
        }
        cell.rowNum.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        cell.DetailLabel.text=_VoteDataArray[indexPath.row];

        [cell.selectButton addTarget:self action:@selector(cellSelectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectButton.tag=indexPath.row;
        UIImage * selectImg=nil;
        if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",(long)indexPath.row]] isEqualToString:@"0"]) {
            selectImg=[UIImage imageNamed:@"choose_normal"];
        }else{
            selectImg=[UIImage imageNamed:@"choose_selected"];   
        }

        if ([_voted isEqualToNumber:@1]&&[_VoteDataDic[@"MyVoteID"] integerValue]==indexPath.row) {  //若已经投票
            cell.DetailLabel.textColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
        }
        
        cell.selectImg.image=selectImg;
        cell.VoteCount.text=[NSString stringWithFormat:@"%@",_VoteDataDic[[NSString stringWithFormat:@"%ld_Choices",(long)indexPath.row]][@"votesCount"]];
//        [NSString stringWithFormat:@"%d",[_VoteDataDic[@"voterNum"] intValue]];
        //计算柱状图宽度
        
        NSInteger allVotesCount=0;
        for (int i=0; i<(_VoteDataDic.count-1)/2; i++) {
            NSInteger count=[_VoteDataDic[[NSString stringWithFormat:@"%d_Choices",i]][@"votesCount"] integerValue];
            allVotesCount+=count;
        }
        
        float danWeiChang=cell.jinduBG.frame.size.width/(allVotesCount==0.0?1.0:allVotesCount);//[cell.VoteCount.text intValue]
        CGRect frame=cell.jinduv.frame;
        float beilu=(ScreenWidth-67)/(320.0-67);
        frame.size.width=danWeiChang*[cell.VoteCount.text intValue]*beilu;
        cell.jinduv.frame=frame;
        /*
//        if ([_voted isEqualToNumber:@1]) {
//            cell.selectImg.hidden=YES;
//            cell.jinduBG.hidden=NO;
//            cell.VoteCount.hidden=NO;
//            UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
//            view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
//            [cell addSubview:view];
//            [UIView animateWithDuration:0.8 animations:^{
//                view.frame=frame;
//            }];
//            
//        }else{
//            if ([_closed isEqualToNumber:@1]) {
//                cell.selectImg.hidden=YES;
//            }else{
//                cell.selectImg.hidden=NO;
//            }
//            cell.jinduBG.hidden=YES;
//            cell.VoteCount.hidden=YES;
//        }
        */
        
        if ([_closed isEqualToNumber:@1]) {
            cell.selectImg.hidden=YES;
            cell.jinduBG.hidden=NO;
            cell.VoteCount.hidden=NO;
            UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
            view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
            [cell addSubview:view];
            [UIView animateWithDuration:0.8 animations:^{
                view.frame=frame;
            }];
        }else{
            if ([_voted isEqualToNumber:@1]) {
                cell.selectImg.hidden=YES;
                cell.jinduBG.hidden=NO;
                cell.VoteCount.hidden=NO;
                UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
                view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
                [cell addSubview:view];
                [UIView animateWithDuration:0.8 animations:^{
                    view.frame=frame;
                }];
                
            }else{
                cell.selectImg.hidden=NO;
                cell.jinduBG.hidden=YES;
                cell.VoteCount.hidden=YES;
            }
        }
        return cell;
    }
    
    //*******************************
    CommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MessageModel *messageModel = self.messageData[indexPath.row];
    
    cell.senderLabel.text = messageModel.sender;
    NSString *str = [iconPath stringByAppendingString:messageModel.avatar];
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
    // 设置时间显示格式
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconTapGestureClick:)];
    cell.avatarImageView.tag = indexPath.row;
    cell.avatarImageView.userInteractionEnabled = YES;
    [cell.avatarImageView addGestureRecognizer:userIconTap];
//    cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllAmDateFormateWithUTCDate:messageModel.updatedAt];
    cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDetailDateFormateWithUTCDate:messageModel.updatedAt];
    
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect creatAtFrame = cell.creatAtLabel.frame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width);
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    
    if (senderFrame.size.width>200) {
        senderFrame.size.width = 200;
    }
    
    cell.senderLabel.frame = senderFrame;
    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    cell.creatAtLabel.frame = creatAtFrame;
    
    NSDictionary *info = messageModel.info;
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 16;
    CGRect frame = cell.descLabel.frame;
    frame.size.width = width;
    
    cell.descLabel.emojiDelegate = self;
    [cell.descLabel sizeToFit];
    
//    cell.descLabel.text = info[@"message"];
    [cell.descLabel setEmojiText:info[@"message"]];
    
    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
//    if (cell.descLabel.text!=NULL) {
        bodyFrame = [cell.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.descLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.descLabel.text attributes:attributes];
//        bodyFrame = [cell.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        
//    }
    frame.size.height = ceil(bodyFrame.size.height);
    cell.descLabel.frame = frame;

    CGFloat cellHeight = 0;
    if (messageModel.subType.intValue==1||messageModel.subType.intValue==4||messageModel.subType.intValue==3) {
        cell.descLabel.hidden = NO;
        cell.comImageView.hidden = YES;
        
        cell.comImageView.image = nil;
        CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
        if (cell.descLabel.text!=NULL) {
            bodyFrame = [cell.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        } else {
            cell.descLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.descLabel.text attributes:attributes];
            bodyFrame = [cell.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            
        }
        frame.size.height = ceil(bodyFrame.size.height)+10;
        cell.descLabel.frame = frame;
        
        
        /**
         *  定制urlBodyLabel链接预加载内容
         */
        if ([messageModel.type isEqualToNumber:@16]){
//            NSLog(@"---------Message:%@",messageModel.info[@"content"]);
            cell.urlBodyLabe.hidden=NO;
            CGRect frame2 = cell.urlBodyLabe.frame;
            frame2.size.width = width;
            cell.urlBodyLabe.text = info[@"content"];
            frame2.origin.y = CGRectGetMaxY(cell.descLabel.frame);
            CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
            ubodyFrame = [cell.urlBodyLabe.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
            frame2.size.height = ceil(ubodyFrame.size.height)+23;
            cell.urlBodyLabe.frame = frame2;
        }else{
            cell.urlBodyLabe.hidden=YES;
        }
        
        // 存储高度，用于返回cell的高度
        if (frame.size.height==0) {
            cellHeight = CGRectGetMaxY(cell.senderLabel.frame);
        } else {
            cellHeight = CGRectGetMaxY(cell.descLabel.frame);
            if ([messageModel.type isEqualToNumber:@16]){
                cellHeight = CGRectGetMaxY(cell.urlBodyLabe.frame)+23;
            }
        }
        self.cellHeight = cellHeight; 
    } else {
        
        cell.descLabel.hidden = YES;
        cell.comImageView.hidden = NO;


        CGRect subIgFrame = cell.comImageView.frame;
        subIgFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        
        CGFloat subW = [messageModel.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [messageModel.info[@"thumbNail_h"] floatValue];
        if (subW>subH) {
            subH = 180*(subH/subW);
            subW = 180;
            
        } else {
            subW = 180*(subW/subH);
            subH = 180;
        }
        subIgFrame.size.width = isnan(subW)?180:subW;
        subIgFrame.size.height = subH;
        
        cell.comImageView.frame = subIgFrame;

        [cell.comImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.info[@"thumbNail"]]] placeholderImage:nil];

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCommentImageViewTapClick:)];
        cell.comImageView.tag = indexPath.row;
        cell.comImageView.userInteractionEnabled = YES;
        [cell.comImageView addGestureRecognizer:tap];
        
        // 存储高度，用于返回cell的高度
        self.cellHeight = CGRectGetMaxY(cell.comImageView.frame)+5;
        
    }
    
    [self.cellHeightArray insertObject:[NSNumber numberWithFloat:self.cellHeight] atIndex:indexPath.row];
    
    //****************************************
    
    cell.transform = self.tableView.transform;
    
    return cell;
}

#pragma mark 发送按钮，左侧按钮点击事件

- (void)didPressRightButton:(id)sender{
    [self.textView refreshFirstResponder];
    // 发表评论
    [self addCommentToTopic];
    /*
    // tableView滚动到最下面
//    [self setUpMessageDetailViewTableViewScroller];
    
//    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageData.count-1 inSection:0];
//    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    
//    [self.tableView beginUpdates];
//    [_messageData insertObject:message atIndex:0];
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
//    [self.tableView endUpdates];
//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];

//    // Fixes the cell from blinking (because of the transform, when using translucent cells)
//    // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView reloadData];
     */
    [super didPressRightButton:sender];
    
}

- (void)didPressLeftButton:(id)sender{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册选取",@"拍照", nil];
    [action showInView:self.view];
}

// tableView滚动到最下面
- (void)setUpMessageDetailViewTableViewScroller{

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageData.count-1 inSection:0];
    
    UITableViewScrollPosition scrollPosition = (self.inverted) ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==100) {
        if ([_voted isEqualToNumber:@1]||[_closed isEqualToNumber:@1]) {
            return 70;
        }
        return 44;
    }
//    if (self.cellHeight==0) {
//        return 44;
//    }
//    return self.cellHeight;
    
    float hight=0;
    if (indexPath.row>=_cellHeightArray.count) {
        hight=_cellHeight;
    }else{
        hight=[_cellHeightArray[indexPath.row] floatValue];
    }
    
    return hight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return 0;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return nil;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    view.backgroundColor = [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, self.view.frame.size.width-16, 30)];
    label.text = @"评论";
    label.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    label.font = [UIFont fontWithName:@"Heiti SC" size:15.0];
    [view addSubview:label];
    return view;
}

//  设置时间显示格式
- (NSString *)setUpFormatterDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}

// 发表评论
- (void)addCommentToTopic{
    [self addOrGetTopicComments:@"POST"];
}

// 获取评论列表
- (void)getTopicComments{
    [self addOrGetTopicComments:@"GET"];
}

#pragma mark // 发表评论或获取评论列表
- (void)addOrGetTopicComments:(NSString *)urlType{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *str;
    
    if ([urlType isEqualToString:@"POST"]) { // 发表评论
        str = [NSString stringWithFormat:addCommentToTopicPath,self.topicId];

        parameter[@"message"] = [NSString stringThrowOffLinesWithString:self.textView.text];
        [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    } else { // 获取评论列表
        str = [NSString stringWithFormat:GetTopicDetail,self.topicId];
        [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {

//                [SVProgressHUD dismissWithDelay:0.5];
//                NSLog(@"responseObject：%@",responseObject);
                // 清空之前的数据
                [self.messageData removeAllObjects];
                [self.headerViewData removeAllObjects];
                [_VoteDataArray removeAllObjects];
                [_VoteDataDic removeAllObjects];
                
                NSDictionary *data = responseObject[@"data"];
                MessageModel *model = [[MessageModel alloc] init];
                [model setValuesForKeysWithDictionary:data];
                [self.headerViewData addObject:model];
                for (NSDictionary *dict in data[@"comments"]) {
                    MessageModel *model = [[MessageModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    [self.messageData addObject:model];
                }
                
                _createTopicUserID=data[@"userId"];
                
                NSDictionary * choicesDic=data[@"info"][@"choices"];
                _resouceId=data[@"info"][@"resouceId"];
                _closed=data[@"info"][@"closed"];
                _voted =data[@"info"][@"voted"];
                _anonymous=data[@"info"][@"anonymous"];
                _single=data[@"info"][@"single"];
                

                self.VoteDataDic=data[@"info"][@"options"];
                for ( int i=0; i<_VoteDataDic.count; i++) {
                    [_VoteDataArray addObject:_VoteDataDic[[NSString stringWithFormat:@"%d",i]]];
                }
                long count=_VoteDataDic.count;
                NSMutableDictionary * dicc=[[NSMutableDictionary alloc] initWithDictionary:_VoteDataDic];
                for ( int i=0; i<count; i++) {
                    [dicc setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
                    [dicc setObject:choicesDic[[NSString stringWithFormat:@"%d",i]] forKey:[NSString stringWithFormat:@"%d_Choices",i]];
                }
                if (count!=0) {
                    [dicc setObject:data[@"info"][@"voterNum"] forKey:@"voterNum"];
                }
                
                for (int i=0; i<count; i++) {
                    NSArray * array=choicesDic[[NSString stringWithFormat:@"%d",i]][@"votes"];
                    for (int n=0; n<array.count; n++) {
                        NSDictionary * dic=[array objectAtIndex:n];
                        NSUserDefaults * usd=[NSUserDefaults standardUserDefaults];
                        if ([dic[@"id"] isEqualToNumber:[usd objectForKey:@"id"] ]) {
                            [dicc setObject:[NSNumber numberWithInt:i] forKey:@"MyVoteID"];  //设置我投票的选项id
                        }
                    }
                    
                }
                
                _VoteDataDic=dicc;
                // 显示 vote headerView数据
                
                if ([_topicType isEqualToNumber:@4]) {
                    [self setUpVoteMessageDetailViewHeaderViewData];
                }else{
                    [self setUpMessageDetailViewHeaderViewData];
                }
                
                [self.tableView reloadData];
            } else {
                [UIAlertView alertViewWithTitle:responseObject[@"message"]];
//                [SVProgressHUD showErrorWithStatus:@"加载失败!"];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [ToolOfClass showMessage:@"加载失败，请检查网络"];
        }];
    }
}

//发送投票信息
-(void)sendVoteButClick:(UIButton*)button{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSMutableArray* array=[NSMutableArray array];
    for (int i=0; i<_VoteDataArray.count; i++) {
        if ([_VoteDataDic[[NSString stringWithFormat:@"%d_State",i]] isEqualToString:@"1"]) {
            [array addObject:[NSNumber numberWithInt:i]];
        }
    }
    parameter[@"choice"] =array;

    [manager POST:[NSString stringWithFormat:sendVoteRes,self.resouceId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            [ToolOfClass showMessage:@"投票成功"];
            
            self.textView.text=@"参与了投票";
            [self addCommentToTopic];
            //重新刷新界面数据
            [self getTopicComments];
//            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"投票成功");
            self.textView.text=@"";
        }else if ([responseObject[@"code"] intValue] == 1) {
            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"投票失败"] message:[NSString stringWithFormat:@"%@",responseObject[@"message"]] delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alv show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

//发送关闭投票
-(void)closeVoteButClick:(UIButton*)button{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSMutableArray* array=[NSMutableArray array];
    for (int i=0; i<_VoteDataArray.count; i++) {
        if ([_VoteDataDic[[NSString stringWithFormat:@"%d_State",i]] isEqualToString:@"1"]) {
            [array addObject:[NSNumber numberWithInt:i]];
        }
    }
    parameter[@"choice"] =array;
    
    [manager POST:[NSString stringWithFormat:sendCloseVote,self.resouceId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            [ToolOfClass showMessage:@"关闭成功"];
            //重新刷新界面数据
            [self getTopicComments];
            //            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"关闭投票成功");
        }else if ([responseObject[@"code"] intValue] == 1) {
            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"关闭投票失败"] message:[NSString stringWithFormat:@"%@",responseObject[@"message"]] delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alv show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark// 显示投票 Vote topic的数据
- (void)setUpVoteMessageDetailViewHeaderViewData{
    MessageModel *model = self.headerViewData[0];
    
    [self.voteHeaderView.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]] placeholderImage:nil];
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconClick)];
    self.voteHeaderView.avatarImageView.userInteractionEnabled = YES;
    [self.voteHeaderView.avatarImageView addGestureRecognizer:userIconTap];
    
    self.voteHeaderView.senderLabel.text = model.sender;
    self.voteHeaderView.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.createdAt];
    
    // senderLabel,时间label
    CGRect senderFrame = self.voteHeaderView.senderLabel.frame;
    CGSize senderSize = [model.sender sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = senderSize.width;
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    self.voteHeaderView.senderLabel.frame = senderFrame;
    CGRect creatAtFrame = self.voteHeaderView.creatAtLabel.frame;
    creatAtFrame.origin.x = CGRectGetMaxX(self.voteHeaderView.senderLabel.frame)+10;
    self.voteHeaderView.creatAtLabel.frame = creatAtFrame;
    
    /**
     title定制
     */
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    // title加粗
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17.0],NSParagraphStyleAttributeName:paragraphStyle};
    [self.voteHeaderView.titleLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    self.voteHeaderView.titleLabel.emojiDelegate=self;
    
    CGRect titleFrame = self.voteHeaderView.titleLabel.frame;
    CGFloat width = CGRectGetWidth(self.view.frame)-16;//-CGRectGetMinX(self.voteHeaderView.senderLabel.frame)
    titleFrame.size.width = width;
    
    
    NSMutableString * titleStr=[NSMutableString stringWithFormat:@"%@",[model.info[@"title"] copy]];
    NSRange range=[titleStr rangeOfString:@"发起投票:"];
    if (range.length!=0) {
        [titleStr deleteCharactersInRange:range];
    }
    CGRect titleRect = CGRectMake(0, 0, 0, 0);
//    if (titleStr.length==0) { // 其实title不会为空（判断多余）
//        self.voteHeaderView.titleLabel.text = nil;
    [self.voteHeaderView.titleLabel setEmojiText:titleStr];
        titleRect = [self.voteHeaderView.titleLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        self.voteHeaderView.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleStr attributes:attributes];
//        titleRect = [self.voteHeaderView.titleLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    }
    
    titleFrame.size.height = ceil(titleRect.size.height);
    self.voteHeaderView.titleLabel.frame = titleFrame;
    
    /**
     *  包含图片的投票子话题
     */
    CGRect subImageFrame = self.voteHeaderView.subImageView.frame;
    BOOL isAddimg=[[model.info allKeys] containsObject:@"imageId"];
    if (isAddimg){ // 是包含图片的投票子话题
        // 添加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageDetailViewTapGestureClick:)];
        self.voteHeaderView.subImageView.userInteractionEnabled = YES;
        [self.voteHeaderView.subImageView addGestureRecognizer:tap];
        [self.voteHeaderView.subImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
        subImageFrame.origin.y = CGRectGetMaxY(self.voteHeaderView.titleLabel.frame)+5;
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        CGFloat w = 0;
        CGFloat h = 0;
        if (subW>subH) {
            w = 180;
            h = 180*(subH/subW);
        } else {
            h = 180;
            w = 180*(subW/subH);
        }
        
        subImageFrame.size.width = w;
        subImageFrame.size.height = h;
        [self.voteHeaderView.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else {
        self.voteHeaderView.subImageView.hidden = YES;
        subImageFrame.origin.y=CGRectGetMaxY(titleFrame);
        subImageFrame.size.height = 0;
    }
    self.voteHeaderView.subImageView.frame = subImageFrame;
    
    /**
     *  alertview.frame定制
     */
    [self.voteHeaderView.alertview setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect framm=CGRectMake(0, CGRectGetMaxY(self.voteHeaderView.subImageView.frame)+5, CGRectGetWidth(self.view.frame), 33);
    self.voteHeaderView.alertview.frame =framm;

    
    NSString * string1=@"记名投票";
    NSString * string2=@"(单选)";
    if ([_anonymous isEqualToNumber:@1]) {
        string1=@"匿名投票";
    }
    if ([_single isEqualToNumber:@0]) {
        string2=@"(多选)";
    }
    [self.voteHeaderView.allVoteButton setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect allVoteButtonff=self.voteHeaderView.allVoteButton.frame;
    allVoteButtonff.origin.x=CGRectGetMaxX(self.view.frame)-112;
    self.voteHeaderView.allVoteButton.frame=allVoteButtonff;
    self.voteHeaderView.alertLabel.text=[NSString stringWithFormat:@"%@ %@",string1,string2];
    
    //设置发送投票信息Button
    [self.voteHeaderView.sendVoteButton addTarget:self action:@selector(sendVoteButClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //如果投票关闭了 隐藏投票button
    if (![_closed isEqualToNumber:@0]||[_voted isEqualToNumber:@1]) {
        self.voteHeaderView.sendVoteButton.hidden=YES;
    }
    
    NSUserDefaults * info=[NSUserDefaults standardUserDefaults];
    if (![_createTopicUserID isEqualToNumber:[info objectForKey:@"id"]]) {
        self.voteHeaderView.closeVoteButton.hidden=YES;
    }
    if (![_closed isEqualToNumber:@0]) {
        self.voteHeaderView.closeVoteButton.hidden=YES;
    }
    //设置创建者的关闭投票的button
    [self.voteHeaderView.closeVoteButton addTarget:self action:@selector(closeVoteButClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.voteHeaderView.headVoteTableView setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect  framee=self.voteHeaderView.headVoteTableView.frame;
    framee.origin.y=CGRectGetMaxY(self.voteHeaderView.alertview.frame);
    framee.size.width=ScreenWidth;
    framee.size.height=_VoteDataArray.count*(([_voted isEqualToNumber:@1]||[_closed isEqualToNumber:@1])?70:44);
    self.voteHeaderView.headVoteTableView.frame=framee;
    
    
    //控制是否显示投票及关闭but
    int X=0;
    if (self.voteHeaderView.closeVoteButton.hidden==YES&&self.voteHeaderView.sendVoteButton.hidden==YES) {
        X=60;
    }
    
    // 按钮的背景view
    CGRect btnBackGroundViewFrame = self.voteHeaderView.BtnBackgroundView.frame;
    
    btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.voteHeaderView.headVoteTableView.frame)+70-X;
    
    self.voteHeaderView.BtnBackgroundView.frame = btnBackGroundViewFrame;
    
    // 整个headerView
    CGRect headerViewFrame = self.headerView.frame;
    headerViewFrame.size.height = CGRectGetMaxY(self.voteHeaderView.BtnBackgroundView.frame);
    self.voteHeaderView.frame = headerViewFrame;
    
    
    [self.voteHeaderView.allVoteButton setTitle:[NSString stringWithFormat:@"%d人参与投票",[_VoteDataDic[@"voterNum"] intValue]] forState:UIControlStateNormal];
    [self.voteHeaderView.allVoteButton addTarget:self action:@selector(showAllVoterVC:) forControlEvents:UIControlEventTouchUpInside];
    self.voteHeaderView.headVoteTableView.delegate=self;
    self.voteHeaderView.headVoteTableView.dataSource=self;
    [self.voteHeaderView.headVoteTableView reloadData];
    self.tableView.tableHeaderView = self.voteHeaderView;
    
    if ([model.favorited intValue] == 1) {
        self.voteHeaderView.collectButton.selected = YES;
    }
    if (![model.userId isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.voteHeaderView.deleteButton.enabled = NO;
    }
}

//显示投票名单详情
-(void)showAllVoterVC:(UIButton*)button{
    if ([_anonymous isEqualToNumber:@1]) {
        UIAlertView * alv=[[UIAlertView alloc] initWithTitle:@"温馨提示：" message:@"当前为匿名投票不能查看参与人列表" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alv show];
        return;
    }else{
        if ([_voted isEqualToNumber:@0]&&[_closed isEqualToNumber:@0]) {
            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:@"温馨提示：" message:@"您尚未参与投票不能查看结果" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alv show];
            return;
        }
    }
    VoteUserListViewController* voteAllUserVC=[[VoteUserListViewController alloc] initWithNibName:@"VoteUserListViewController" bundle:nil];
    voteAllUserVC.choicesDic=_VoteDataDic;
    [self.navigationController pushViewController:voteAllUserVC animated:YES];
    //showAllVoterVC
}

// 显示topic的数据
- (void)setUpMessageDetailViewHeaderViewData{
    MessageModel *model = self.headerViewData[0];
    
    [self.headerView.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]] placeholderImage:nil];
    
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconClick)];
    self.headerView.avatarImageView.userInteractionEnabled = YES;
    [self.headerView.avatarImageView addGestureRecognizer:userIconTap];
    
    self.headerView.senderLabel.text = model.sender;
    self.headerView.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.createdAt];
    
    // senderLabel,时间label
    CGRect senderFrame = self.headerView.senderLabel.frame;
    CGSize senderSize = [model.sender sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = senderSize.width;
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    self.headerView.senderLabel.frame = senderFrame;
    CGRect creatAtFrame = self.headerView.creatAtLabel.frame;
    creatAtFrame.origin.x = CGRectGetMaxX(self.headerView.senderLabel.frame)+10;
    self.headerView.creatAtLabel.frame = creatAtFrame;
    
    /**
        title,desc定制
     */
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    // title加粗
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    // desc不加粗
    NSDictionary *attributes1 = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    
    CGRect titleFrame = self.headerView.titleLabel.frame;
    CGRect descFrame = self.headerView.descLabel.frame;
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(self.headerView.senderLabel.frame)-16;
    titleFrame.size.width = width;
    descFrame.size.width = width;
    
    self.headerView.titleLabel.emojiDelegate=self;
    
    NSString *titleStr = [model.info[@"title"] copy];
    CGRect titleRect = CGRectMake(0, 0, 0, 0);
//    if (titleStr.length==0) { // 其实title不会为空（判断多余）
//        self.headerView.titleLabel.text = nil;
        [self.headerView.titleLabel setEmojiText:titleStr];
        titleRect = [self.headerView.titleLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        self.headerView.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleStr attributes:attributes];
//        titleRect = [self.headerView.titleLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    }
    [self.headerView.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
    titleFrame.size.height = ceil(titleRect.size.height);
    self.headerView.titleLabel.frame = titleFrame;
    /**
        desc定制
     */
    NSString *descStr = [model.info[@"desc"] copy];
    CGRect descRect = CGRectMake(0, 0, 0, 0);
    if (descStr.length==0) {
        self.headerView.descLabel.text = nil;
//        descRect = [self.headerView.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes1 context:nil];
        descRect.size.height = 0;
    } else {
        self.headerView.descLabel.attributedText = [[NSAttributedString alloc] initWithString:descStr attributes:attributes1];
       descRect = [self.headerView.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    }
    self.headerView.descLabel.emojiDelegate=self;
    [self.headerView.descLabel setEmojiText:descStr];
    descFrame.size.height = ceil(descRect.size.height)+10;
    descFrame.origin.y = CGRectGetMaxY(self.headerView.titleLabel.frame);
    self.headerView.descLabel.frame = descFrame;
    
    /**
     *  判断是否为图片子话题
     */
    CGRect subImageFrame = self.headerView.subImageView.frame;
    if (model.subType.intValue==2 || model.subType.intValue==3) { // 是图片子话题
        // 添加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageDetailViewTapGestureClick:)];
        self.headerView.subImageView.userInteractionEnabled = YES;
        [self.headerView.subImageView addGestureRecognizer:tap];

        subImageFrame.origin.y = CGRectGetMaxY(self.headerView.descLabel.frame);
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        CGFloat w = 0;
        CGFloat h = 0;
        if (subW>subH) {
            w = 180;
            h = 180*(subH/subW);
        } else {
            h = 180;
            w = 180*(subW/subH);
        }
        
        subImageFrame.size.width = w;
        subImageFrame.size.height = h;
        [self.headerView.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else {
        self.headerView.subImageView.hidden = YES;
        subImageFrame.size.height = 0;
    }
    self.headerView.subImageView.frame = subImageFrame;
    
    // 按钮的背景view
    CGRect btnBackGroundViewFrame = self.headerView.BtnBackgroundView.frame;
    if (model.subType.intValue==2 || model.subType.intValue==3) {
        btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.headerView.subImageView.frame);
    } else {
        btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.headerView.descLabel.frame);
    }
    
    self.headerView.BtnBackgroundView.frame = btnBackGroundViewFrame;
    
    // 整个headerView
    CGRect headerViewFrame = self.headerView.frame;
    headerViewFrame.size.height = CGRectGetMaxY(self.headerView.BtnBackgroundView.frame);
    self.headerView.frame = headerViewFrame;
    
    self.tableView.tableHeaderView = self.headerView;

    if ([model.favorited intValue] == 1) {
        self.headerView.collectButton.selected = YES;
    }
    if (![model.userId isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.headerView.deleteButton.enabled = NO;
    }
    
}

#pragma mark 分享，收藏，删除操作

- (void)onShareOrCollectOrDelecteBtnClick:(UIButton *)sender
{
    if (sender.tag==100) {
        if (self.textView.becomeFirstResponder) {
            [self.textView resignFirstResponder];
        }
        [UMSocialSnsService presentSnsIconSheetView:self
                                             appKey:nil
                                          shareText:@"图片"
                                         shareImage:self.headerView.subImageView.image
                                    shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToRenren,nil]
                                           delegate:nil];
        
        
    } else if (sender.tag==101) {
        if (sender.selected==YES) {
            [self addMessageDetailViewFavorite:NO];
        } else {
            [self addMessageDetailViewFavorite:YES];
        }
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确定删除子话题?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"删除", nil];
        [alert show];
    }
}
- (void)deleteMessageDetailViewTopic{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    [manager POST:[NSString stringWithFormat:deleteTopic,self.topicId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            
            [ToolOfClass showMessage:@"成功删除!"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteTopicSuccess" object:self.topicId userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:@"操作失败，请检查网络"];
    }];
    
}

- (void)addMessageDetailViewFavorite:(BOOL)isFavorite{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = nil;
    if (isFavorite) {
        parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    } else {
        parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],@"true"] forKeys:@[@"accessToken",@"unfavorite"]];
    }
    [manager POST:[NSString stringWithFormat:addFavorite,self.topicId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            if (isFavorite) {
                self.headerView.collectButton.selected = YES;
                self.voteHeaderView.collectButton.selected = YES;
            } else {
               self.headerView.collectButton.selected = NO;
                self.voteHeaderView.collectButton.selected = NO;
            }
            if (isFavorite) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newFavorite" object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newUnFavorite" object:self.topicId];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
#pragma mark 图片子话题图片的点击手势

- (void)onMessageDetailViewTapGestureClick:(UITapGestureRecognizer *)tapGesture{
    
    MessageModel *model = self.headerViewData[0];
    if ([model.subType isEqualToNumber:@3]) {
        MapViewController *mapVC = [[MapViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        ShowPictureViewController *showPictureVC = [[ShowPictureViewController alloc] initWithNibName:@"ShowPictureViewController" bundle:[NSBundle mainBundle]];
        showPictureVC.imageInfo = model.info;
        [self presentViewController:showPictureVC animated:YES completion:nil];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you ovveride this method, to call super.
    [super scrollViewDidScroll:scrollView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (self.messageData.count>0) {
        [self setUpMessageDetailViewTableViewScroller];
    }
}

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

- (void)textViewDidChangeSelection:(SLKTextView *)textView{
    [super textViewDidChangeSelection:textView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [super alertView:alertView clickedButtonAtIndex:buttonIndex];
    if (buttonIndex==1) {
        [self deleteMessageDetailViewTopic];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.textView resignFirstResponder];
    
    if (buttonIndex==2){
//        [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
    } else  if (buttonIndex==1) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)onMesDetailViewUserIconTapGestureClick:(UITapGestureRecognizer *)tap{
    UIImageView *userIconImageView = (UIImageView *)tap.view;

    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.messageData[userIconImageView.tag];

    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)onMesDetailViewUserIconClick{
    MessageModel *model = self.headerViewData[0];
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:@"正在发送..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];

    NSData *imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1);
    [manager POST:[NSString stringWithFormat:addCommentToTopicPath,self.topicId] parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

- (void)onCommentImageViewTapClick:(UITapGestureRecognizer *)tap{
    MessageModel *model = self.messageData[tap.view.tag];
    ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
    picVC.imageInfo = model.info;
    [self presentViewController:picVC animated:NO completion:nil];
}

- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
    
}

@end
