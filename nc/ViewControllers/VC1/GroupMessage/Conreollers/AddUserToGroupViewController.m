//
//  AddUserToGroupViewController.m
//  nc
//
//  Created by docy admin on 15/10/22.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AddUserToGroupViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "UserModel.h"
#import "CompanyTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIButton+XJTButton.h"

@interface AddUserToGroupViewController ()

@property (nonatomic, retain) NSMutableArray *userData;
@property (nonatomic, retain) UIButton *countBtn;

@property (nonatomic, assign) int counts;

@end

@implementation AddUserToGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpAddUserToGroupViewData];
    
    [self getUpCompanyAllUser];
    
}

- (void)setUpAddUserToGroupViewData{
    
    self.navigationItem.title = @"通讯录";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpAddUserToGroupViewback)];
    
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:@"发送" target:self action:@selector(onRightItemSendClick)];
    self.counts = 0;
    UIButton *button = [[UIButton alloc] init];
    button.frame = CGRectMake(0, 0, 50, 30);
    [button setTitle:@"邀请" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onRightItemSendClick) forControlEvents:UIControlEventTouchUpInside];
    UIButton *countBtn = [UIButton buttonWithFrame:CGRectMake(50-14, 0, 14, 14) backgroundColor:[UIColor whiteColor] cornerRadius:7];
    [countBtn setTitleColor:[UIColor colorWithHue:72/255.0 saturation:193/255.0 brightness:168/255.0 alpha:1.0] forState:UIControlStateNormal];
    countBtn.hidden = YES;
    countBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    [button addSubview:countBtn];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.countBtn = countBtn;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.userData = [NSMutableArray array];
}

- (void)setUpAddUserToGroupViewback{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onRightItemSendClick{

    NSMutableArray *toAdd = [NSMutableArray array];
    for (NSArray *arr in self.userData) {
        for (UserModel *model in arr) {
            if (model.cellIsSelected) {
                [toAdd addObject:model.id];
            }
        }
    }
    if (toAdd.count==0) {
        [ToolOfClass showMessage:@"至少选择一个成员"];
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        parameter[@"toAdd"] = toAdd;

        [manager POST:[NSString stringWithFormat:InviteJoinGroup, self.groupId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"re==%@",responseObject[@"message"]);
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                
                self.navigationItem.rightBarButtonItem.enabled = YES;
                [ToolOfClass showMessage:@"邀请成功"];
                if (self.viewBlock) {
                    self.viewBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
                
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            self.navigationItem.rightBarButtonItem.enabled = YES;
            [ToolOfClass showMessage:@"加载失败，请检查网络"];
        }];
    }
}

- (void)getUpCompanyAllUser{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * cid=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCompany"];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager GET:[NSString stringWithFormat:GetUserList, cid] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            // 清空数据
            [self.userData removeAllObjects];
            NSMutableArray *peopleArray = [NSMutableArray array]; // 用来存储联系人
            NSArray * array=responseObject[@"data"];
            
            for (int i=0; i < array.count; i++) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:array[i]];
                model.cellIsSelected = NO;
                [peopleArray addObject:model];
            }
            
//            if (self.userData.count == 0) {
//                [ToolOfClass showMessage:@"没有成员可以邀请噢!"];
//                return ;
//            }
            
            for (UserModel *user in self.currentUsers) {
                for (int i=0; i < peopleArray.count; i++) {
                    UserModel *model = peopleArray[i];
                    if ([user.id isEqualToNumber:model.id]) {
                        [peopleArray removeObject:model];
                        break;
                    }
                }
            }
            
            
            // Sort data
            UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
            for (UserModel *model in peopleArray) {
                NSInteger sect = [theCollation sectionForObject:model
                                        collationStringSelector:@selector(nickName)];
                model.sectionNumber = sect;
            }
            
            NSInteger highSection = [[theCollation sectionTitles] count];
            NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
            
            for (int i=0; i<=highSection; i++) {
                NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
                [sectionArrays addObject:sectionArray];
            }
            
            for (UserModel *model in peopleArray) {
                [(NSMutableArray *)[sectionArrays objectAtIndex:model.sectionNumber] addObject:model];
            }
            
            // 排好序的联系人加入数组中
            for (NSMutableArray *sectionArray in sectionArrays) {
                int count = 0; // 统计name为nil的个数
                for (UserModel *model in sectionArray) {
                    if (model.name.length==0) {
                        count++;
                    }
                }
                NSArray *sortedSection = [NSArray array];
                //        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
                if (count >= 2) { // 有2个以上name为nil,
                    sortedSection = [NSArray arrayWithArray:sectionArray];
                } else {
                    sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(nickName)];
                }
                [self.userData addObject:sortedSection];
            }
            

            [self.tableView reloadData];
            
        }
        else if([responseObject[@"code"] isEqualToNumber:@3]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
        }else {
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [ToolOfClass showMessage:@"加载失败，请检查网络"];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.userData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.userData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    CompanyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserModel *model = self.userData[indexPath.section][indexPath.row];
    
    [cell.iconImView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]] placeholderImage:nil];
    cell.companyNameLabel.text = model.nickName;
    cell.phoneLabel.text = [model.phone isEqual:[NSNull null]]?@"":model.phone;
    if (indexPath.row==[self.userData[indexPath.section] count]-1) {
        cell.line.hidden = YES;
    } else {
        cell.line.hidden = NO;
    }
    CGRect rect = cell.line.frame;
    rect.size.height = 0.5;
    cell.line.frame = rect;
    
    if (model.cellIsSelected) {
        cell.setCurrentCpyBtn.selected = YES;
    } else {
        cell.setCurrentCpyBtn.selected = NO;
    }
    
    cell.setCurrentCpyBtn.tag = 100*indexPath.section+indexPath.row;
    
    [cell.setCurrentCpyBtn addTarget:self action:@selector(onSelectedUserBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *title = [self.userData[section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section]:nil;
    return title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)  indexPath{
    CompanyTableViewCell *cell = (CompanyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.setCurrentCpyBtn.selected = !cell.setCurrentCpyBtn.selected;
    UserModel *model = self.userData[indexPath.section][indexPath.row];
    if (cell.setCurrentCpyBtn.selected) {
        model.cellIsSelected = YES;
        self.counts++;
        
    } else {
        model.cellIsSelected = NO;
        self.counts--;
    }
    [self.countBtn setTitle:[NSString stringWithFormat:@"%d",self.counts] forState:UIControlStateNormal];
    if (self.counts==0) {
        self.countBtn.hidden = YES;
    } else {
        self.countBtn.hidden = NO;
    }
    
}

- (void)onSelectedUserBtnClick:(UIButton *)sender{
    
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag%100 inSection:sender.tag/100]];
}

@end
