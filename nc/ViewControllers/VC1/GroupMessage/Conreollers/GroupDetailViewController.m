//
//  GroupDetailViewController.m
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "GroupDetailViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "GroupModel.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "GroupDetailUserView.h"
#import "GroupMessageManagerViewController.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "PersonalProfileModifyInfoViewController.h"
#import "GroupDisbandGroupViewController.h"
#import "AFNHttpRequest.h"
#import "UserModel.h"
#import "AddUserToGroupViewController.h"
#import "GroupConveyGroupViewController.h"

@interface GroupDetailViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, retain) NSMutableArray *groupInfoData;

@end

@implementation GroupDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hiddenView.hidden=NO;
    
    // 设置基本信息
    [self setUpGroupDetailViewData];
    // 获取群组详细信息
    [self getGroupDetailViewData];
    // 获取群组成员数据
    [self getGroupDetailViewUserData];
    
    if (![_SuperViewStr isEqualToString:@"chakan"]) {
        _hiddenView.hidden=YES;
        self.scrollerView.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, self.scrollerView.bounds.size.height);
    }else{
        self.scrollerView.scrollEnabled = NO;
    }
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpGroupDetailViewUserListAgain) name:conveyGroupOwer object:nil];

}


// 设置基本信息
- (void)setUpGroupDetailViewData
{
    self.groupInfoData = [NSMutableArray array];
    self.navigationItem.title = @"小组资料";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onGroupDetailViewBackButtonClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onGroupDetailViewBackButtonClick)];
}

- (void)onGroupDetailViewBackButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 获取群组详细信息
- (void)getGroupDetailViewData
{
    [self getUpGroupInfoOrLogoutGroup:@"GET"];
}
// 获取群组成员数据
- (void)getGroupDetailViewUserData
{
    if (!self.groupUsers) {
        self.groupUsers = [NSMutableArray array];
        [self getUpGroupDetailViewGroupUserListData]; // 请求成员数据
    }
}

#pragma mark 转让群主后的通知
- (void)getUpGroupDetailViewUserListAgain{
    
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:getSpecifyGroupUsersListPath,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupUsers addObject:model];
            }
            
            int i = 0;
            for (GroupDetailUserView *view in self.userBgView.subviews) {
                if (i==0) {
                    
                } else {
                    UserModel *usermodel = self.groupUsers[i-1];
                    [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
                    [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
                }
                i++;
            }
            GroupModel *gmodel = [self.groupInfoData lastObject];
            UserModel *usermodel = self.groupUsers[0];
            gmodel.creator = usermodel.id;
            
            self.selectedLogoBtn.enabled = NO;
            self.modifyGroupDescBtn.enabled = NO;
            self.modifyGroupNameBtn.enabled = NO;
            self.privateGroup.enabled = NO;
//            [self.logOutGroupBtn setTitle:@"退出" forState:UIControlStateNormal];
//            self.logOutGroupBtn.enabled = YES;
//            [self.logOutGroupBtn setBackgroundColor:[UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1.0]];
            self.conveyGroupBtn.hidden = YES;
            self.disbandGroupBtn.hidden = YES;
            self.logOutGroupBtn.hidden = NO;
            self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.logOutGroupBtn.frame)+50);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 请求成员数据
- (void)getUpGroupDetailViewGroupUserListData
{
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:getSpecifyGroupUsersListPath,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupUsers addObject:model];
            }
            if (self.groupInfoData.count!=0) {
                [self showGroupDetailViewGroupInfo];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 群组信息展示
- (void)showGroupDetailViewGroupInfo
{
    CGRect hiddenViewFrame = self.hiddenView.frame;
    hiddenViewFrame=CGRectMake(hiddenViewFrame.origin.x, hiddenViewFrame.origin.y*[UIScreen mainScreen].bounds.size.height/568-5, ScreenWidth, ScreenHeight);
    self.hiddenView.frame = hiddenViewFrame;
    
    GroupModel *model = [self.groupInfoData lastObject];
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    
   
    
    if (model.creator==nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"该小组暂时没有组长，暂时不能查看小组信息" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        alert.tag = 300;
        return;
    }
    
    
    if ([num isEqualToNumber:model.creator]) {
        self.selectedLogoBtn.enabled = YES;
        self.modifyGroupDescBtn.enabled = YES;
        self.modifyGroupNameBtn.enabled = YES;
//        self.canJoin.enabled = YES;
//        self.notifiMode.enabled = YES;
        self.privateGroup.enabled = YES;
        self.logOutGroupBtn.hidden = YES;
//        [self.logOutGroupBtn setTitle:@"转让或解散小组" forState:UIControlStateNormal];
//        self.logOutGroupBtn.enabled = NO;
//        [self.logOutGroupBtn setBackgroundColor:[UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1.0]];
        
//        self.disbandGroupBtn.layer.borderWidth = 1;
//        self.disbandGroupBtn.layer.borderColor = [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1].CGColor;
    } else {
        self.selectedLogoBtn.enabled = NO;
        self.modifyGroupDescBtn.enabled = NO;
        self.modifyGroupNameBtn.enabled = NO;
//        self.canJoin.enabled = NO;
//        self.notifiMode.enabled = NO;
        self.privateGroup.enabled = NO;
        
        self.conveyGroupBtn.hidden = YES;
        self.disbandGroupBtn.hidden = YES;
    }
    
    self.nameLabel.text = model.name;
    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    self.descLabel.text = model.desc;
//    self.notifiMode.on = model.broadcast.boolValue;
//    self.canJoin.on = model.joinType.boolValue;
    if (model.category.intValue==4) {
        self.privateGroup.on = 1;
    } else {
        self.privateGroup.on = 0;
    }
    self.userCountLabel.text = [NSString stringWithFormat:@"成员%lu",self.groupUsers.count];
    
    CGFloat width = CGRectGetWidth(self.view.frame)/5.0;
    NSInteger y = 0;

    for (NSInteger i = 0; i < self.groupUsers.count+1; i++) {
        GroupDetailUserView *view = [[[NSBundle mainBundle] loadNibNamed:@"GroupDetailUserView" owner:nil options:nil] lastObject];
        
        if (i==0) {
            view.avatarImageView.image = [UIImage imageNamed:@"invitation"];
            [view.nameButton setTitle:@"" forState:UIControlStateNormal];
            
            view.crownIg.hidden = YES;
            if (model.category.intValue==4) {
                
                if ([num isEqualToNumber:model.creator]) {
                    view.userInteractionEnabled = YES;
                } else {
                    view.userInteractionEnabled = NO;
                }
            }
            
        } else {
            
            UserModel *usermodel = self.groupUsers[i-1];
            [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
            [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
//            if (i==1) {
//                UIImage *image = [UIImage imageNamed:@"crown"];
//                image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//                [view.nameButton setImage:image forState:UIControlStateNormal];
//            }
            
            if ([model.creator isEqualToNumber:usermodel.id]) {
//                [view.nameButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                view.crownIg.hidden = NO;
            } else {
                view.crownIg.hidden = YES;
            }
        }
        
        y = i/5;
        
        CGRect viewFrame = view.frame;
        viewFrame.size.width = width;
        viewFrame.size.height = width+5;
        viewFrame.origin.x = width*(i%5);
        viewFrame.origin.y = viewFrame.size.height*y;
        
        view.frame = viewFrame;
        [self.userBgView addSubview:view];
        // 给头像添加点击手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupUserPerfileLookUpTapGesture:)];
        [view.avatarImageView addGestureRecognizer:tap];
        view.avatarImageView.tag = i;
        view.avatarImageView.userInteractionEnabled = YES;
    }
    
    CGRect userBgViewFrame = self.userBgView.frame;
    userBgViewFrame.origin.y = CGRectGetMaxY(self.userCountLabel.frame);
    userBgViewFrame.size.height = (y+1)*(width+5);
    self.userBgView.frame = userBgViewFrame;

    CGRect logOutBtnFrame = self.logOutGroupBtn.frame;
    logOutBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
    self.logOutGroupBtn.frame = logOutBtnFrame;
    
    CGRect conveyBtnFrame = self.conveyGroupBtn.frame;
    conveyBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
    self.conveyGroupBtn.frame = conveyBtnFrame;
    
    CGRect disbandBtnFrame = self.disbandGroupBtn.frame;
    disbandBtnFrame.origin.y = CGRectGetMaxY(self.conveyGroupBtn.frame)+30;
    self.disbandGroupBtn.frame = disbandBtnFrame;
    
    if (self.conveyGroupBtn.hidden) {
        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.logOutGroupBtn.frame)+50);
    } else {
        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.disbandGroupBtn.frame)+50);
    }
    

//    hiddenViewFrame.size.width=[UIScreen mainScreen].bounds.size.width;
//    NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    if ([UIScreen mainScreen].bounds.size.height==568) {
        return;
    }
//    CGRect hiddenViewFrame = self.hiddenView.frame;
//    hiddenViewFrame=CGRectMake(hiddenViewFrame.origin.x, hiddenViewFrame.origin.y*[UIScreen mainScreen].bounds.size.height/568, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
//    self.hiddenView.frame = hiddenViewFrame;

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    // 上传图片文件
    [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.logoImageView.image setAvatarOrLogoPath:[NSString stringWithFormat:setLogoPath,self.groupId]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 更改群组信息

- (IBAction)onGroupDetailViewModifyGroupNameBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.nameLabel.text;
    infoVC.itemTitle = @"小组名称";
    infoVC.groupId = self.groupId;
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.nameLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onGroupDetailViewModifyGroupDescBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.descLabel.text;
    infoVC.itemTitle = @"小组介绍";
    infoVC.groupId = self.groupId;
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.descLabel.text = modifyText;
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

#pragma mark 更换群组头像,退出，转让，解散

- (IBAction)selectedLogoBtnClick:(id)sender {
    
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

- (IBAction)onLogoutGroupBtnClick:(id)sender {
    
//    GroupModel *model = [self.groupInfoData lastObject];
//    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
//    if ([num isEqualToNumber:model.creator]) {
//        
//        GroupDisbandGroupViewController *disband = [[GroupDisbandGroupViewController alloc] initWithNibName:@"GroupDisbandGroupViewController" bundle:[NSBundle mainBundle]];
//        disband.groupId = self.groupId;
//        disband.userCount = self.groupUsers.count;
//        disband.groupName = self.nameLabel.text;
//        [self.navigationController pushViewController:disband animated:YES];
//    } else {
    
        [UIAlertView alertViewWithTitle:@"是否确定退出该小组" target:self];
//    }
    
}

- (IBAction)onConveyGroupBtnClick:(id)sender {
    
    GroupConveyGroupViewController *convey = [[GroupConveyGroupViewController alloc] init];
    convey.groupId = self.groupId;
    convey.disbandBlock = ^(NSNumber *userId){
//        self.cannotDisband = YES;
//        if (self.conveyBlock) {
//            self.conveyBlock(userId);
//        }
        [self getUpGroupDetailViewUserListAgain];
        
    };
    [self.navigationController pushViewController:convey animated:YES];
}

- (IBAction)onDisbandGroupBtnClick:(id)sender {
    UIAlertView *alert =  [UIAlertView alertViewWithTitle:@"解散小组" subTitle:@"解散小组之后你将和好友失去联系，确定解散小组?" target:self];
    alert.tag = 100;
}
// 跳转群组消息管理界面
- (IBAction)GroupDetaiViewGroupMessageManager:(id)sender {
    GroupMessageManagerViewController *managerView = [[GroupMessageManagerViewController alloc] init];
    managerView.groupId = self.groupId;
    [self.navigationController pushViewController:managerView animated:YES];
}

- (IBAction)onCanJoinSwitchClick:(UISwitch *)sender {
    NSLog(@"122v==%d",sender.on);
}

- (IBAction)onprivateGroupSwitchClick:(UISwitch *)sender {
    
    NSString *path = [NSString stringWithFormat:SetGroupPrivate,self.groupId];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"setPrivate"] = @(sender.on);
    [AFNHttpRequest httpRequestSetDataWithUrl:path parameters:parameter];
}

// 获取群组详细信息或退出群组
- (void)getUpGroupInfoOrLogoutGroup:(NSString *)str
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path;
    if ([str isEqualToString:@"GET"]) {// 请求群组信息
        [self.groupInfoData removeAllObjects];
        path = [NSString stringWithFormat:groupDetailInfoPath,self.groupId];
        [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:responseObject[@"data"]];
                [self.groupInfoData addObject:model];
                
                if (self.groupUsers.count!=0) {
                    // 显示信息
                    [self showGroupDetailViewGroupInfo];
                }
                
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    } else { // 退出群组
        path = [NSString stringWithFormat:logoutGroupPath,self.groupId];
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                // 发通知，刷新群组列表
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutGroup" object:self.groupId];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                [UIAlertView alertViewWithTitle:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

// 群组资料中的查看成员点击手势
- (void)groupUserPerfileLookUpTapGesture:(UITapGestureRecognizer *)gesture
{
    UIImageView *avatarImageView = (UIImageView *)gesture.view;
    if (avatarImageView.tag==0) {
        AddUserToGroupViewController *addUser = [[AddUserToGroupViewController alloc] init];
        addUser.groupId = self.groupId;
        addUser.currentUsers = self.groupUsers;
        
        addUser.viewBlock = ^(void){
            for (GroupDetailUserView *view in self.userBgView.subviews) {
                [view removeFromSuperview];
            }
            [self getUpGroupDetailViewGroupUserListData];
        };
        [self.navigationController pushViewController:addUser animated:YES];
    } else {
        LookUpGroupUserPerfileViewController *perfileVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
        perfileVC.userPerfileModel = self.groupUsers[avatarImageView.tag-1];
        [self.navigationController pushViewController:perfileVC animated:YES];
    }
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 300) { // 没有群主
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if (buttonIndex==1) {
        
        if (alertView.tag==100) {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
            NSString *path = [NSString stringWithFormat:GroupOwnerDisbandGroup,self.groupId];
            [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] intValue] == 0) {
                    [ToolOfClass showMessage:@"解散成功"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:disbandGroup object:self.groupId];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                } else {
                    [ToolOfClass showMessage:responseObject[@"message"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [ToolOfClass showMessage:@"解散失败"];
            }];

        } else {
            
            [self getUpGroupInfoOrLogoutGroup:@"POST"];
        }
        
        
//        GroupModel *model = [self.groupInfoData lastObject];
//        NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
//        if ([num isEqualToNumber:model.creator]) {
//            GroupDisbandGroupViewController *disband = [[GroupDisbandGroupViewController alloc] initWithNibName:@"GroupDisbandGroupViewController" bundle:[NSBundle mainBundle]];
//            disband.groupId = self.groupId;
//            disband.userCount = self.groupUsers.count;
//            [self.navigationController pushViewController:disband animated:YES];
//        } else {
//            
//            [self getUpGroupInfoOrLogoutGroup:@"POST"];
//        }
    }
}


@end
