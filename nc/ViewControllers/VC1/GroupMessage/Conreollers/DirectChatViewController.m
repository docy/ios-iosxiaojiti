//
//  DirectChatViewController.m
//  nc
//
//  Created by docy admin on 15/9/19.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "DirectChatViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "MessageModel.h"
#import "XJTMessageTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "NSString+XJTString.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "MapViewController.h"
#import "SVProgressHUD.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "ShowPictureViewController.h"
#import "AFNHttpRequest.h"
#import "MLEmojiLabel.h"

#define defaultY 16

@interface DirectChatViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate>

@property (nonatomic, retain) NSMutableArray *chatMessage;
@property (nonatomic, assign) CGFloat cellHeight; // cell的高度(自动计算)
@property (nonatomic, assign) int chatPage;
@property (nonatomic, copy) NSString *lastTimeString;

@end

@implementation DirectChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [self setUpDirectChatViewData];
    [self setUpDirectChatViewMJRefresh];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    if (self.groupId!=nil) {
        self.chatPage = 1;
        [self getUpDirectChatViewChatListWithGroupId:self.groupId];
    } else {
        [self getUpDirectChatViewChatMessage];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpDirectChatViewNewMessage:) name:@"newMessage" object:nil];
}
- (void)setUpDirectChatViewData{
    
    self.chatMessage = [NSMutableArray array];
    self.cellHeight = 0;
    self.chatPage = 1;

    self.navigationItem.title = self.groupName;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(DirectChatViewBack)];

    [self.rightButton setTitle:NSLocalizedString(@"发送", nil) forState:UIControlStateNormal];
    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.textInputbar.leftButton setImage:image forState:UIControlStateNormal];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.automaticallyAdjustsScrollViewInsets = YES;
 
}

- (void)setUpDirectChatViewMJRefresh{
    
    [self.tableView addFooterWithCallback:^{
        [self getUpDirectChatViewChatListWithGroupId:self.groupId];
    }];
    
}
- (void)DirectChatViewBack{
    if (self.cellBlock) {
        self.cellBlock();
    }
    
    if (self.navigationController.viewControllers.count>2) {
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeUnReadConutPriGroup" object:self.groupId];
    }
    
    [AFNHttpRequest httpRequestResetUnreadCountWithGroupId:self.groupId];
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.groupId = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getUpDirectChatViewChatMessage{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],self.directChatId] forKeys:@[@"accessToken",@"targetId"]];
    [manager POST:directChat parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            self.groupId = responseObject[@"data"][@"id"];
            [self getUpDirectChatViewChatListWithGroupId:self.groupId];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.chatMessage.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"MessengerCellId";
    
    XJTMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.responseLabel.hidden = YES;
    cell.themeLabel.hidden = YES;
    cell.detailButton.hidden = YES;
    cell.voteView.hidden = YES;
    MessageModel *model = self.chatMessage[indexPath.row];
    
    cell.senderLabel.text = model.nickName;
    [cell.userAvatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]] placeholderImage:nil];
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconTapGestureClick:)];
    cell.userAvatarImageView.tag = indexPath.row;
    cell.userAvatarImageView.userInteractionEnabled = YES;
    [cell.userAvatarImageView addGestureRecognizer:userIconTap];
    
    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:model.updatedAt];
    
    CGRect avatarFrame = cell.userAvatarImageView.frame;
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect creatAtFrame = cell.createdAtLabel.frame;
    
    CGRect leftViewFrame = cell.leftImageView.frame;
    if (model.flag==0) { // 显示时间框
        cell.timeView.hidden = NO;
        avatarFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        senderFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        creatAtFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        
        cell.timeLabel.text = model.timeViewStr;
        if ([cell.timeLabel.text isEqualToString:@"今天"]) {
            leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-30-32-20)/2.0;
        } else {
            leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-150-32-20)/2.0;
        }
        CGPoint timeCenter = cell.timeLabel.center;
        CGRect rightViewFrame = cell.rightImageView.frame;
        
        timeCenter.x = cell.timeView.center.x;
        cell.timeLabel.center = timeCenter;
        
        cell.leftImageView.frame = leftViewFrame;
        rightViewFrame.size.width = cell.leftImageView.frame.size.width;
        rightViewFrame.origin.x = self.view.frame.size.width - cell.leftImageView.frame.size.width-16;
        cell.rightImageView.frame = rightViewFrame;
        
        if ([model.type isEqualToNumber:@16]) {
            cell.timeView.hidden = YES;
        }
    } else {
        cell.timeView.hidden = YES;
        avatarFrame.origin.y = defaultY;
        senderFrame.origin.y = defaultY;
        creatAtFrame.origin.y = defaultY;
    }
    
    cell.userAvatarImageView.frame = avatarFrame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width)+10;
    
    if (senderFrame.size.width>200) {
        senderFrame.size.width = 200;
    }
    
    cell.senderLabel.frame = senderFrame;
    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    cell.createdAtLabel.frame = creatAtFrame;
    
    
    
    NSDictionary *info = model.info;
    
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - defaultY;
    
    CGRect bodyFrame = cell.bodyLabel.frame;
    bodyFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
    
//    if ([model.type isEqualToNumber:@2] ||[model.type isEqualToNumber:@3]) {
//        cell.bodyLabel.hidden = NO;
//        cell.subImageView.hidden = YES;
//        cell.bodyLabel.text = [NSString stringWithFormat:@"欢迎%@加入小组",model.nickName];
//        bodyFrame.size.height = 21;
//        
//        cell.bodyLabel.frame = bodyFrame;
//        self.cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
//    } else
    cell.bodyLabel.emojiDelegate=self;
    if ([model.type isEqualToNumber:@1]||[model.type isEqualToNumber:@15]||[model.type isEqualToNumber:@16]) {
        cell.bodyLabel.hidden = NO;
        cell.subImageView.hidden = YES;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
//        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:info[@"message"] attributes:attributes];
        [cell.bodyLabel setEmojiText:info[@"message"]];
        CGRect bodyRect = CGRectMake(0, 0, 0, 0);
        bodyRect = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        bodyFrame.size.height = ceil(bodyRect.size.height);
        bodyFrame.size.width = width;
        cell.bodyLabel.frame = bodyFrame;
        self.cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
        /**
         *  定制urlBodyLabel链接预加载内容
         */
        if ([model.type isEqualToNumber:@16]){
//            NSLog(@"---------Message:%@",model.info[@"content"]);
            cell.urlBodyLabel.hidden=NO;
            CGRect frame2 = cell.urlBodyLabel.frame;
            frame2.size.width = width;
            cell.urlBodyLabel.text = info[@"content"];
            frame2.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
            CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
            ubodyFrame = [cell.urlBodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
            frame2.size.height = ceil(ubodyFrame.size.height)+23;
            cell.urlBodyLabel.frame = frame2;
            self.cellHeight = CGRectGetMaxY(cell.urlBodyLabel.frame);
        }else{
            cell.urlBodyLabel.hidden=YES;
        }
        
        
    } else { // type=12,13
        cell.bodyLabel.hidden = YES;
        cell.bodyLabel.attributedText = nil;
        cell.subImageView.hidden = NO;
        cell.subImageView.userInteractionEnabled = YES;
        CGRect subIgFrame = cell.subImageView.frame;
        subIgFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        if (subW>subH) {
            subH = 180*(subH/subW);
            subW = 180;
            
        } else {
            subW = 180*(subW/subH);
            subH = 180;
        }
        subIgFrame.size.width = subW;
        subIgFrame.size.height = subH;
        
        cell.subImageView.frame = subIgFrame;
        [cell.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSubImageViewTapClick:)];
        cell.subImageView.tag = indexPath.row;
        [cell.subImageView addGestureRecognizer:tap];
        
        // 存储高度，用于返回cell的高度
        self.cellHeight = CGRectGetMaxY(cell.subImageView.frame)+5;
        
    }
    
    cell.transform = tableView.transform;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.cellHeight==0) {
        return 44;
    }
    return self.cellHeight;
}

- (void)didPressRightButton:(id)sender{
    [self.textView refreshFirstResponder];
    [self setUpSendChatMessage];
    [super didPressRightButton:sender];
    
}

- (void)didPressLeftButton:(id)sender{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"选择照片",@"地图", nil];
    action.tag = 100;
    [action showInView:self.view];
}

- (void)getUpDirectChatViewChatListWithGroupId:(NSNumber *)groupId{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    [manager GET:[NSString stringWithFormat:getMessageListPath,groupId,self.chatPage] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@0]) {

            if (self.chatPage==1) {
                [self.chatMessage removeAllObjects];
            }
            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if (![dict[@"type"] isEqualToNumber:@2] && ![dict[@"type"] isEqualToNumber:@3]) {
                    MessageModel *model = [[MessageModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    model.sendSuccessflag=1;
                    [self.chatMessage addObject:model];
                }
            }
            
            self.lastTimeString = @"";
            for (NSInteger i = self.chatMessage.count-1; i>=0; i--) {
                MessageModel *mes = self.chatMessage[i];
                NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:mes.updatedAt];
                if (![timeStr isEqualToString:self.lastTimeString]) {
                    mes.flag = 0; // 显示标志
                    self.lastTimeString = [timeStr copy];
                    mes.timeViewStr = [timeStr copy]; // 记录下来
                } else {
                    mes.flag = 1;
                    mes.timeViewStr = nil; // 记录下来
                }
            }
            
            double delayInSeconds = 0.6;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.tableView footerEndRefreshing];
                [self.tableView reloadData];
            });
            self.chatPage++;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 发送消息

- (void)setUpSendChatMessage{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    parameter[@"message"] = [NSString stringThrowOffLinesWithString:self.textView.text];
    
    [manager POST:sendMessagePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            
        } else if ([responseObject[@"code"] isEqualToNumber:@3]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 通知消息

- (void)getUpDirectChatViewNewMessage:(NSNotification *)not{
    NSArray *message = not.object;
    NSDictionary *dict = message[0];
    
    if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到url链接预加载内容时
        NSString *clientId = dict[@"info"][@"clientId"];
        for (long i=0; i<_chatMessage.count; i++) {
            MessageModel *model=_chatMessage[i];
            if ([clientId isEqualToString:model.info[@"clientId"]]) {
                MessageModel *mdol=[[MessageModel alloc] init];
                [mdol setValuesForKeysWithDictionary:dict];
                mdol.sendSuccessflag=1;   //隐藏发送失败的感叹号
                [_chatMessage replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第i行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
        }
    }
    
    if (self.groupId!=nil) {
        
        if ([dict[@"groupId"] isEqualToNumber:self.groupId]) {
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];

            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            model.sendSuccessflag=1;
            [self.chatMessage insertObject:model atIndex:0];
            [self.tableView reloadData];
        }
    }
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag==100) {
        if (buttonIndex==0) {
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册中选取",@"拍照", nil];
            action.tag = 101;
            [action showInView:self.view];
        } else if (buttonIndex==1) {
            MapViewController *mapVC = [[MapViewController alloc] init];
            mapVC.isFromPrivateVC = YES;
            mapVC.groupId = self.groupId;
            [self.navigationController pushViewController:mapVC animated:YES];
        }
    } else {
        
        if (buttonIndex==2){
            return;
        }
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
        if (buttonIndex==0) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.allowsEditing = YES;
        } else  if (buttonIndex==1) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [SVProgressHUD showWithStatus:@"正在发送..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    NSData *imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1);
    [manager POST:directChatSendImage parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showWithStatus:@"发送失败"];
            [SVProgressHUD dismissWithDelay:1.0];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark 图片的点击事件

- (void)onSubImageViewTapClick:(UITapGestureRecognizer *)tap{
    MessageModel *model = self.chatMessage[tap.view.tag];
    
    if ([model.type  isEqualToNumber: @13]) { // 地图
        MapViewController *mapVC = [[MapViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
        picVC.imageInfo = model.info;
        [self presentViewController:picVC animated:NO completion:nil];
    }
}

- (void)onMesDetailViewUserIconTapGestureClick:(UITapGestureRecognizer *)tap{
    UIImageView *userIconImageView = (UIImageView *)tap.view;
    
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.chatMessage[userIconImageView.tag];
    
    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
    
}
@end
