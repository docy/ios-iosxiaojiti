//
//  LookUpGroupUserPerfileViewController.m
//  xjt
//
//  Created by docy admin on 7/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LookUpGroupUserPerfileViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIImageView+WebCache.h"
#import "DirectChatViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"

@interface LookUpGroupUserPerfileViewController ()

@end

@implementation LookUpGroupUserPerfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置基本属性
    [self setUpLookUpGroupUserPerfileViewData];
    if (self.userPerfileModel!=nil) {
        // 显示数据
        [self setUpLookUpGroupUserPerfileViewUserPerfile];
    } else {
        // 获取用户信息
        [self getUpUserData];
    }
    
}
// 设置基本属性
- (void)setUpLookUpGroupUserPerfileViewData{
    self.navigationItem.title = @"个人资料";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(LookUpGroupUserPerfileViewNavcBackBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(LookUpGroupUserPerfileViewNavcBackBtnClick)];
}

- (void)getUpUserData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    NSString *str = [personalInfoPath stringByAppendingString:[NSString stringWithFormat:@"%@",self.userId]];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            self.userPerfileModel = [[UserModel alloc] init];
            [self.userPerfileModel setValuesForKeysWithDictionary:responseObject[@"data"]];
            // 显示数据
            [self setUpLookUpGroupUserPerfileViewUserPerfile];
            
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:@"加载失败，请检查网络"];
    }];
}

// 显示数据
- (void)setUpLookUpGroupUserPerfileViewUserPerfile{
//    NSLog(@"%@",self.userPerfileModel.id);
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:self.userPerfileModel.avatar]] placeholderImage:nil];
    // 给头像添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onLookUpGroupUserPerfileViewTapGesture:)];
    [self.avatarImageView addGestureRecognizer:tap];
    self.nameLabel.text = self.userPerfileModel.name;
    self.nickLabel.text = self.userPerfileModel.nickName;
    if ([self.userPerfileModel.sex intValue] ==0) {
//        [self.sexBtn setTitle:@"女" forState:UIControlStateNormal];
        
        [self.sexBtn setBackgroundImage:[UIImage imageNamed:@"selected_women_icon"] forState:UIControlStateNormal];
        
        
    } else {
       // [self.sexBtn setTitle:@"男" forState:UIControlStateNormal];
        [self.sexBtn setBackgroundImage:[UIImage imageNamed:@"selected_men_icon"] forState:UIControlStateNormal];
    }
    self.contactMode.text = self.userPerfileModel.phone;
    if ([self.userPerfileModel.id isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.sendMessageButton.hidden = YES;
    } else {
        self.sendMessageButton.hidden = NO;
    }
}

- (void)LookUpGroupUserPerfileViewNavcBackBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 头像的点击事件
- (void)onLookUpGroupUserPerfileViewTapGesture:(UITapGestureRecognizer *)tapGesture
{
    NSLog(@"%s",object_getClassName(self));
}
- (IBAction)onSendPrivateMessageBtnClick:(id)sender {
    
    DirectChatViewController *directVC = [[DirectChatViewController alloc] init];
    directVC.directChatId = self.userPerfileModel.id;
    directVC.groupName = self.userPerfileModel.nickName;
    [self.navigationController pushViewController:directVC animated:YES];
    
}
@end
