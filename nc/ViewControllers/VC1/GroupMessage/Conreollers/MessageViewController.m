//
//  MessageViewController.m
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//


#import "MessageViewController.h" 

#import "MessageTableViewCell.h"
#import "XJTMessageTableViewCell.h"
#import "MessageTextView.h"

#import <LoremIpsum/LoremIpsum.h>

#import "UIButton+XJTButton.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "XJTTitleButton.h"
#import "FouncationView.h"

#import "PictureViewController.h"
#import "MainThemeViewController.h"
#import "MapViewController.h"
#import "VoteViewController.h"

#import "MessageDetailViewController.h"
#import "ShowPictureViewController.h"
#import "GroupDetailViewController.h"
#import "SubTopicViewController.h"
#import "LookUpGroupUserPerfileViewController.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "MessageModel.h"
#include "UserModel.h"
#import "FooterView.h"
#import "NSString+XJTString.h"
#import "MJRefresh.h" // 刷新数据
//#import "SRRefreshView.h"
//#import "SVProgressHUD.h"
#import "UIButton+XJTButton.h"
#import "MLEmojiLabel.h"
#import "UIColor+Hex.h"
#define kFooterViewHeight self.view.bounds.size.height*0.05
#define defaultSpaceWith 16 // cell的子控件距离cell边缘的默认间距
#define subImageViewWidth 180 // 图片子话题图片的固定高度
#define userIconImageViewTag 1000 // 成员头像的tag值

static NSString *AutoCompletionCellIdentifier = @"AutoCompletionCell";

@interface MessageViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate>

@property (nonatomic, assign) CGFloat cellHeight; // cell的高度
@property (nonatomic, strong) NSMutableArray * cellHeightArray; //存储cell高度，0925_gxf优化显示。
@property (nonatomic, retain) NSString * lastClientId;  //存储最后一条message的cid 用于区分本地生成的message 1015_gxf_Add

@property (nonatomic,retain) UIImageView *bgImageView;;
@property (nonatomic, retain) FouncationView *founcationView; // topicView
@property (nonatomic, retain) UIButton *fcButton; // topic功能键
@property (nonatomic, assign) int messagePage; // 消息页数
@property (nonatomic, assign) BOOL isActivityFirstRefresh; // 消息页数
//@property (nonatomic, retain) UIRefreshControl *refreshControl;

@property (nonatomic, retain) NSNumber * voteSingle; //投票是否关闭
@property (nonatomic, retain) NSNumber * voteAnonymous; //投票是否多选
@property (nonatomic, retain) NSNumber * voteClose; //投票是否多选

@property (nonatomic, retain) NSNumber * On_an_ZHTid;   //上一个子话题id
@property (nonatomic, retain) UIButton *unreadButton;

@property (nonatomic, assign) BOOL isScroToUnreadMess;

@end

@implementation MessageViewController

- (id)init
{
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
//        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
    }
    return self;
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccessful:) name:@"deleteTopicSuccess" object:nil];
    // 设置基本属性
    [self setUpAttribute];
    
    // 设置加好功能键
    [self setUpFouncationButton];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    // 注册通知观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MessageViewRefishNewMessage:) name:@"newMessage" object:nil];
    
    // 获取群组用户列表
    [self getSpecifyGroupUsersListData];
    [self setUpMessageViewMJRefresh];
    
}

-(void)viewWillDisappear:(BOOL)animated{
//    [self onMessageViewNavBackClick];
}

- (void)setUpMessageViewMJRefresh{
    [self.messagesData removeAllObjects];
    

    [self.tableView addFooterWithCallback:^{
        [self getUpMessageListData];
    }];
    [self.tableView footerBeginRefreshing];
}
- (void)deleteTopicSuccessful:(NSNotification *)not{
    self.messagePage = 1;
    [self.messagesData removeAllObjects];
    [self getUpMessageListData];
}

#pragma mark 新消息刷新（通知观察者方法调用）
- (void)MessageViewRefishNewMessage:(NSNotification *)notification{
    NSArray *arr = notification.object;
    
    for (NSDictionary *dict in arr) {
//        NSLog(@"MessVC_message is :%@",dict);
        if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到 url链接 预加载内容 时
            NSString *clientId = dict[@"info"][@"clientId"];
            for (long i=0; i<_messagesData.count; i++) {
                MessageModel *model=_messagesData[i];
                if ([clientId isEqualToString:model.info[@"clientId"]]) {
                    MessageModel *mdol=[[MessageModel alloc] init];
                    [mdol setValuesForKeysWithDictionary:dict];
                    NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
                    if ([timeStr isEqualToString:self.lastTimeString]) {
                        model.flag = 1; // 隐藏标志
                        model.timeViewStr = nil; // 记录下来
                    } else {
                        model.flag = 0; // 显示标志
                        model.timeViewStr = [timeStr copy]; // 记录下来
                        self.lastTimeString = [timeStr copy];
                    }
                    model.sendSuccessflag=1;
                    [self.messagesData replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                    NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第二行
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                    return;
                }
            }
        }
        //本地即时生成消息 替换处理
        NSString *clientId = dict[@"info"][@"clientId"];
        if ([_lastClientId isEqualToString:clientId]) { //若clientId 相同  1019_gxf_add 替换消息防止修改名字头像不更新
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            
            if (![model.info[@"link"] isEqual:[NSNull null]]&&![model.info[@"link"] isEqualToString:model.info[@"title"]]) {
                NSMutableDictionary* newInfo=[[NSMutableDictionary alloc] initWithDictionary:model.info];
                NSString * strrr=[NSString stringWithFormat:@"%@\n%@",newInfo[@"link"],newInfo[@"desc"]];
                [newInfo setValue:strrr forKey:@"desc"];
                model.info=newInfo;
            }
            model.sendSuccessflag=1;
            [self.messagesData replaceObjectAtIndex:0 withObject:model];
            [self.tableView reloadData];
            return;
        }
        NSNumber *groupId = dict[@"groupId"];
        if (groupId.intValue ==self.groupId.intValue) {
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            model.sendSuccessflag=1;
            [self.messagesData insertObject:model atIndex:0];
            [self.tableView reloadData];
        }
    }
}
- (void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"newMessage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 设置加号功能键
- (void)setUpFouncationButton{
    // 添加灰色背景
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    bgImageView.backgroundColor = [UIColor blackColor];
    bgImageView.alpha = 0.5;
    bgImageView.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewTapGestureClick:)];
    bgImageView.userInteractionEnabled = YES;
    [bgImageView addGestureRecognizer:tap];
    self.bgImageView = bgImageView;
    [self.view addSubview:bgImageView];
    
    // 添加topic按钮
    UIButton *fcButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rect = CGRectMake(self.view.frame.size.width-16-60, self.view.frame.size.height-50-60-64, 60, 60);
    [fcButton setNormalImage:@"add.png" andSelectedImage:@"back.png" andFrame:rect addTarget:self action:@selector(onFouncationBtnClick:)];
    [self.view addSubview:fcButton];
    self.fcButton = fcButton;
    
    // 添加topic功能图
    FouncationView *founcationView = [[[NSBundle mainBundle] loadNibNamed:@"FouncationView" owner:self options:nil] lastObject];
    
    CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - founcationView.bounds.size.width;
    CGFloat founcationY = CGRectGetMinY(_fcButton.frame) - founcationView.bounds.size.height - 10;
    CGRect founcationRect = founcationView.frame;
    founcationRect.origin = CGPointMake(founcationX, founcationY);
    founcationView.frame = CGRectMake(founcationRect.origin.x, founcationRect.origin.y, founcationRect.size.width, founcationRect.size.height+70);
    founcationView.hidden = YES;
    
    for (UIView *view in founcationView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn addTarget:self action:@selector(onAddFouncationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    self.founcationView = founcationView;
    [self.view addSubview:founcationView];
    
    CGPoint point1=self.founcationView.center;
    point1.x+=280;
    self.founcationView.center=point1;
    
    self.view.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, ([UIScreen mainScreen].bounds.size.height/2)+64);

}
- (void)onMessageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    self.founcationView.hidden = YES;
    self.bgImageView.hidden = YES;
    self.fcButton.selected = NO;
}

#pragma mark 地图，图片，主题等功能键的点击事件

- (void)onAddFouncationButtonClick:(UIButton *)sender{
    [self onFouncationBtnClick:self.fcButton];
    if (sender.tag == 0) {
        VoteViewController *voteVC = [[VoteViewController alloc] init];
        voteVC.groupId=self.groupId;
        [self.navigationController pushViewController:voteVC animated:YES];
    } else if (sender.tag == 1) {
        
        // block 反向传值
        MapViewController *mapVC = [[MapViewController alloc] init];
        mapVC.groupId=self.groupId;
//        mapVC.mapBlock = ^(Message *message){
//            [self.messages insertObject:message atIndex:0];
//            [self.tableView reloadData];
//            
//        };
        [self.navigationController pushViewController:mapVC animated:YES];
        
    } else if (sender.tag == 2) {
        
    }else if (sender.tag == 3) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册中选取",@"拍照", nil];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];//self.view];
        
    } else if (sender.tag == 4) {
        MainThemeViewController *mainVC = [[MainThemeViewController alloc] init];
        mainVC.groupId = self.groupId;
        [self.navigationController pushViewController:mainVC animated:YES];
    }else{
    
    }
}

// 功能键的点击触发方法
- (void)onFouncationBtnClick:(UIButton *)sender{
    sender.selected = !sender.selected;

    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (sender.selected) {
            self.founcationView.hidden = NO;
            self.bgImageView.hidden = NO;
            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGRect point1=self.founcationView.frame;
            point1.origin.x=founcationX;//-=self.founcationView.center.x<160?0:280;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.5;
        } else {
            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGRect point1=self.founcationView.frame;
            point1.origin.x=founcationX+280;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        if (sender.selected) {
            
        } else {
            self.founcationView.hidden = YES;
            self.bgImageView.hidden = YES;
            
        }
    }];
    
}

- (void)onMessageViewNavBackClick{

    if (self.cellBlock) {
        self.cellBlock();
    }
    [self sendResetUnread];
}
//通知服务器退出聊天详情页面
-(void)sendResetUnread{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager POST:[NSString stringWithFormat:ResetUnread,_groupId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) { // 发送失败
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];

}

#pragma mark 设置基本属性
- (void)setUpAttribute{
    
    // 导航栏属性
    CGSize titleSize = [self.groupName sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:17.0]}];
    XJTTitleButton *titleButton = [[XJTTitleButton alloc] initWithFrame:CGRectMake(0, 0, ceil(titleSize.width)+20, 30)];
    [titleButton setTitle:self.groupName forState:UIControlStateNormal];
    self.navigationItem.titleView = titleButton;
    [titleButton addTarget:self action:@selector(onGetGroupAttributeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"topic list" target:self action:@selector(onGetUpGroupSubTopicBtnClick)];
    
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onMessageViewNavBackClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMessageViewNavBackClick)];

    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    // 数据初始化
    self.cellHeight = 0;
    self.messagesData = [NSMutableArray array];
    self.users = [NSMutableArray array];
    self.cellHeightArray=[NSMutableArray array];
//    self.usersAvatar = [NSMutableArray array];
    self.groupUsers = [NSMutableArray array];
    self.messagePage = 1;
    self.lastTimeString = [NSString string];

    self.isActivityFirstRefresh = YES;
    //    self.users = @[@"Allen", @"Anna", @"Alicia", @"Arnold", @"Armando", @"Antonio", @"Brad", @"Catalaya", @"Christoph", @"Emerson", @"Eric", @"Everyone", @"Steve"];
    
    self.channels = @[@"General", @"Random", @"iOS", @"Bugs", @"Sports", @"Android", @"UI", @"SSB"];
    self.emojis = @[@"m", @"man", @"machine", @"block-a", @"block-b", @"bowtie", @"boar", @"boat", @"book", @"bookmark", @"neckbeard", @"metal", @"fu", @"feelsgood"];
    
    //  设置tableView属性
    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    self.inverted = YES;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // send 按钮
    [self.rightButton setTitle:NSLocalizedString(@"发送", nil) forState:UIControlStateNormal];
    
    // 设置textInputbar属性
    [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
    [self.textInputbar.editorLeftButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [self.textInputbar.editorRightButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];

    self.textInputbar.autoHideRightButton = YES;
    self.textInputbar.maxCharCount = 256;
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
//        self.textInputbar.counterPosition = SLKCounterPositionBottom;
    
    self.typingIndicatorView.canResignByTouch = YES;
    
    [self.autoCompletionView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:AutoCompletionCellIdentifier];
    [self registerPrefixesForAutoCompletion:@[@"@", @"#", @":"]];
    
    /**
     *  未读条数button
     */
    
    
    if (self.unreadCount.intValue!=0) {
        
        UIImage *image = [UIImage imageNamed:@"news tips"];
        
        UIButton *unreadButton = [UIButton buttonWithType:UIButtonTypeCustom];
        unreadButton.frame = CGRectMake(0, 0, ScreenWidth, image.size.height);
        unreadButton.backgroundColor = [UIColor colorWithRed:254/255.0 green:236/255.0 blue:196/255.0 alpha:1.0];
        self.unreadButton = unreadButton;
        [unreadButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        unreadButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [unreadButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if (self.unreadCount.intValue>99) {
          [unreadButton setTitle:@"你有99+条未读消息" forState:UIControlStateNormal];
        } else {
//            [unreadButton setTitle:self.unreadCount.stringValue forState:UIControlStateNormal];
            [unreadButton setTitle:[NSString stringWithFormat:@"你有%@条未读消息",self.unreadCount] forState:UIControlStateNormal];
        }
        
        [self.view addSubview:unreadButton];
        
        [UIView animateWithDuration:3 animations:^{
            unreadButton.alpha = 0;
        } completion:^(BOOL finished) {
            [unreadButton removeFromSuperview];
        }];
        
    }
}

#pragma mark 请求数据
// 请求数据,获取消息列表
- (void)getUpMessageListData{
    [self getUpMessageListOrSpecifyGroupUsersList:getMessageListPath];
}
// 获取群组用户列表
- (void)getSpecifyGroupUsersListData{
    [self.users removeAllObjects];
//    [self.usersAvatar removeAllObjects];
    [self.groupUsers removeAllObjects];
    [self getUpMessageListOrSpecifyGroupUsersList:getSpecifyGroupUsersListPath];
}
// 获取消息列表或者用户列表
- (void)getUpMessageListOrSpecifyGroupUsersList:(NSString *)urlPath{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *str = nil;
    if ([urlPath isEqualToString:getMessageListPath]) {
        str = [NSString stringWithFormat:urlPath,self.groupId,self.messagePage];
    } else {
        str = [NSString stringWithFormat:urlPath,self.groupId];
    }
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            
            if ([urlPath isEqualToString:getMessageListPath]) { // 获取群组消息列表
                
                for (NSDictionary *dict in responseObject[@"data"]) {
                        MessageModel *model = [[MessageModel alloc] init];
                        [model setValuesForKeysWithDictionary:dict];
                        model.sendSuccessflag=1;
                        [self.messagesData addObject:model];
                }
                self.lastTimeString = @"";
                for (NSInteger i = self.messagesData.count-1; i>=0; i--) {
                    MessageModel *mes = self.messagesData[i];
                    NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:mes.updatedAt];
                    if (![timeStr isEqualToString:self.lastTimeString]) {
                        mes.flag = 0; // 显示标志
                        self.lastTimeString = [timeStr copy];
                        mes.timeViewStr = [timeStr copy]; // 记录下来
                    } else {
                        mes.flag = 1;
                        mes.timeViewStr = nil; // 记录下来
                    }
                }
                if (self.isActivityFirstRefresh) {
                    self.isActivityFirstRefresh = NO;
                }
                double delayInSeconds = 0.6;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.tableView footerEndRefreshing];
                    
                    [self.tableView reloadData];
                    
//                    if (self.isScroToUnreadMess==YES) {
//                        
//                        self.isScroToUnreadMess=NO;
//                        [self performSelectorOnMainThread:@selector(checkInUnreadMessage) withObject:nil waitUntilDone:YES];
//                    } else {
//                        
//                        [self.tableView reloadData];
//                    }
                    
                });
                
                self.messagePage++;
                
            } else { // 获取群组用户列表

                for (NSDictionary *dict in responseObject[@"data"]) {
                    [self.users addObject:dict[@"nickName"]];
//                    [self.usersAvatar addObject:dict[@"avatar"]];
                    UserModel *model = [[UserModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    [self.groupUsers addObject:model];
                }
            }
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.tableView footerEndRefreshing];
        [ToolOfClass showMessage:@"加载失败，请检查网络"];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_editing"] style:UIBarButtonItemStylePlain target:self action:@selector(editRandomMessage:)];
    editItem.title = @"edit";
    
    UIBarButtonItem *appendItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_append"] style:UIBarButtonItemStylePlain target:self action:@selector(fillWithText:)];
    appendItem.title = @"append";
    
    UIBarButtonItem *typeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_typing"] style:UIBarButtonItemStylePlain target:self action:@selector(simulateUserTyping:)];
    typeItem.title = @"typing";
    
//    self.navigationItem.rightBarButtonItems = @[editItem, appendItem, typeItem];
    
    self.founcationView.hidden = YES;
    self.bgImageView.hidden = YES;
    self.fcButton.selected = NO;
}
#pragma mark - Action Methods

// 获取群组详情
- (void)onGetGroupAttributeBtnClick{
    GroupDetailViewController *groupDetailVC = [[GroupDetailViewController alloc] init];
    groupDetailVC.groupId = self.groupId;
    groupDetailVC.groupUsers = self.groupUsers;
    [self.navigationController pushViewController:groupDetailVC animated:YES];
}
// 获取群组子话题
- (void)onGetUpGroupSubTopicBtnClick{
    SubTopicViewController *subVC = [[SubTopicViewController alloc] init];
    subVC.groupName = self.groupName;
    subVC.groupId = self.groupId;
    [self.navigationController pushViewController:subVC animated:YES];
}
#pragma mark 导航栏按钮点击事件
//
- (void)fillWithText:(id)sender{
    if (self.textView.text.length == 0)
    {
        int sentences = (arc4random() % 4);
        if (sentences <= 1) sentences = 1;
        self.textView.text = [LoremIpsum sentencesWithNumber:sentences];
    }
    else {
        [self.textView slk_insertTextAtCaretRange:[NSString stringWithFormat:@" %@", [LoremIpsum word]]];
    }
}
- (void)simulateUserTyping:(id)sender{
    if (!self.isEditing && !self.isAutoCompleting) {
        [self.typingIndicatorView insertUsername:[LoremIpsum name]];
    }
}
- (void)editRandomMessage:(id)sender{
    int sentences = (arc4random() % 10);
    if (sentences <= 1) sentences = 1;
    
    [self editText:[LoremIpsum sentencesWithNumber:sentences]];
}

// 长按cell的触发事件
- (void)editCellMessage:(UIGestureRecognizer *)gesture
{
    MessageTableViewCell *cell = (MessageTableViewCell *)gesture.view;
    MessageModel *message = self.messagesData[cell.indexPath.row];
    
//    [self editText:message.body];
    
    [self.tableView scrollToRowAtIndexPath:cell.indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)editLastMessage:(id)sender
{
    if (self.textView.text.length > 0) {
        return;
    }
    
    NSInteger lastSectionIndex = [self.tableView numberOfSections]-1;
    NSInteger lastRowIndex = [self.tableView numberOfRowsInSection:lastSectionIndex]-1;
    
    MessageModel *lastMessage = [self.messagesData objectAtIndex:lastRowIndex];
    
//    [self editText:lastMessage.body];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Overriden Methods

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    // Notifies the view controller that the keyboard changed status.
}

- (void)textWillUpdate
{
    // Notifies the view controller that the text will update.
    
    [super textWillUpdate];
}

- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    
    [super textDidUpdate:animated];
}

// 输入框左侧按钮的事件处理
- (void)didPressLeftButton:(id)sender
{
    // Notifies the view controller when the left button's action has been triggered, manually.
    
    [super didPressLeftButton:sender];
}

// send 发送消息按钮的事件处理
- (void)didPressRightButton:(id)sender
{
    [self.textView refreshFirstResponder];

    // 发送消息至服务器
    [self sendMessageViewMessage];
    
    [super didPressRightButton:sender];
}
// 发送消息至服务器
- (void)sendMessageViewMessage{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    NSString *text = self.textView.text;
    
//    for (NSInteger i = 0; ; i++) {
//        NSRange range = [text rangeOfString:@"\n"];
//        if (range.location!=NSNotFound) {
//            text = [text stringByReplacingCharactersInRange:range withString:@""];
//        } else {
//            break;
//        }
//    }
    
    for (NSInteger i = 0; ; i++) {
        if ([text hasSuffix:@"\n"]) {
            text = [text stringByReplacingCharactersInRange:NSMakeRange(text.length-1, 1) withString:@""];
        } else {
            break;
        }
    }
    
    parameter[@"message"] = text;
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    _lastClientId=parameter[@"clientId"];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    //本地即时显示消息
    MessageModel *model = [[MessageModel alloc] init];
    model.avatar = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"];
    model.sender=[[NSUserDefaults standardUserDefaults] objectForKey:@"nickName"];
    model.sendSuccessflag=1;
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *dateFormatted = [NSDate date];
    NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600*2];
    NSString *currentTime = [formatter stringFromDate:currentDate];
    model.updatedAt=currentTime;
    NSDictionary * dic=[[NSDictionary alloc] initWithObjectsAndKeys:parameter[@"clientId"],@"clientId",parameter[@"message"],@"message", nil];
    model.info =dic;
        model.flag = 1; // 隐藏标志
        model.timeViewStr = nil; // 记录下来
    [self.messagesData insertObject:model atIndex:0];
    [self.tableView reloadData];
    
    [manager POST:sendMessagePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (![responseObject[@"code"] isEqualToNumber:@0]) { // 发送失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:responseObject[@"message"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        MessageModel * modell=_messagesData[0];
        modell.sendSuccessflag=0;
        [self.messagesData replaceObjectAtIndex:0 withObject:modell];//insertObject:model atIndex:0];
        NSIndexPath *te=[NSIndexPath indexPathForRow:0 inSection:0];//刷新第一个section的第二行
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te, nil] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

//  设置时间显示格式
- (NSString *)setUpFormatterDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}

- (void)didPressArrowKey:(id)sender{
    [super didPressArrowKey:sender];

    UIKeyCommand *keyCommand = (UIKeyCommand *)sender;
    
    if ([keyCommand.input isEqualToString:UIKeyInputUpArrow]) {
        [self editLastMessage:nil];
    }
}

- (NSString *)keyForTextCaching{
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (void)didPasteMediaContent:(NSDictionary *)userInfo{
    // Notifies the view controller when the user has pasted a media (image, video, etc) inside of the text view.
    
    SLKPastableMediaType mediaType = [userInfo[SLKTextViewPastedItemMediaType] integerValue];

    NSData *contentData = userInfo[SLKTextViewPastedItemData];
    
    if ((mediaType & SLKPastableMediaTypePNG) || (mediaType & SLKPastableMediaTypeJPEG)) {
        
        MessageModel *message = [MessageModel new];
        message.sender = [LoremIpsum name];
//        message.body = @"Attachment";
        message.createdAt = [self setUpFormatterDate];
//        message.userAvatar = [UIImage imageWithData:contentData scale:[UIScreen mainScreen].scale];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
        UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
        
        [self.tableView beginUpdates];
        [self.messagesData insertObject:message atIndex:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    }
}

- (void)willRequestUndo{
    // Notifies the view controller when a user did shake the device to undo the typed text
    
    [super willRequestUndo];
}

- (void)didCommitTextEditing:(id)sender{
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
    
    MessageModel *message = [MessageModel new];
    message.sender = [LoremIpsum name];
//    message.body = [self.textView.text copy];
    
    [self.messagesData removeObjectAtIndex:0];
    [self.messagesData insertObject:message atIndex:0];
    [self.tableView reloadData];
    
    [super didCommitTextEditing:sender];
}

// 取消编辑
- (void)didCancelTextEditing:(id)sender{
    // Notifies the view controller when tapped on the left "Cancel" button
    
    [super didCancelTextEditing:sender];
}


- (BOOL)canPressRightButton{
    return [super canPressRightButton];
}

// @,#,:条件筛选
- (BOOL)canShowAutoCompletion{
    NSArray *array = nil;
    NSString *prefix = self.foundPrefix;
    NSString *word = self.foundWord;
    
    self.searchResult = nil;
    
    if ([prefix isEqualToString:@"@"]) {
        if (word.length > 0) {
            array = [self.users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
        }
        else {
            array = self.users;
        }
    }
    else if ([prefix isEqualToString:@"#"] && word.length > 0) {
        array = [self.channels filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    else if ([prefix isEqualToString:@":"] && word.length > 1) {
        array = [self.emojis filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    
    if (array.count > 0) {
//        array = [array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
    self.searchResult = [[NSMutableArray alloc] initWithArray:array];
    
    return self.searchResult.count > 0;
}

- (CGFloat)heightForAutoCompletionView{
    CGFloat cellHeight = [self.autoCompletionView.delegate tableView:self.autoCompletionView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    return cellHeight*self.searchResult.count;
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        return self.messagesData.count;
    }
    else {
        return self.searchResult.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {
        return [self messageCellForRowAtIndexPath:indexPath];
    }
    else {
        return [self autoCompletionCellForRowAtIndexPath:indexPath];
    }
}

- (XJTMessageTableViewCell *)messageCellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *MessengerCellIdentifier = @"MessengerCellId";
    XJTMessageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (!cell.textLabel.text) {
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editCellMessage:)];
        [cell addGestureRecognizer:longPress];
    }
    // 固定内容
    MessageModel *next_messageModel = nil;
    MessageModel *messageModel = self.messagesData[indexPath.row];
    if (self.messagesData.count-1>indexPath.row) {
        next_messageModel = self.messagesData[indexPath.row+1];
    }

    NSString *str = [iconPath stringByAppendingString:messageModel.avatar];
    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:messageModel.updatedAt];
    
    if ([messageModel.type isEqualToNumber:@3] || [messageModel.type isEqualToNumber:@2]||[messageModel.type isEqualToNumber:@4]) {
        cell.senderLabel.text = nil;
        cell.userAvatarImageView.image = [UIImage imageNamed:@"horn"];
        cell.userAvatarImageView.userInteractionEnabled = NO;
    } else {
        cell.senderLabel.text = messageModel.sender;
        
        [cell.userAvatarImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
        // 为用户头像添加手势，点击时进入用户个人详情
        UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewUserIconTapGestureClick:)];
        cell.userAvatarImageView.tag = userIconImageViewTag+indexPath.row;
        cell.userAvatarImageView.userInteractionEnabled = YES;
        [cell.userAvatarImageView addGestureRecognizer:userIconTap];
    }
    
    cell.indexPath = indexPath;
    cell.usedForMessage = YES;
    
    CGFloat timeMaxY = CGRectGetMaxY(cell.timeView.frame);
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect avatarFrame = cell.userAvatarImageView.frame;
    CGRect creatAtFrame = cell.createdAtLabel.frame;
    CGRect GanTHFrame=cell.ganTanHao.frame;
    
    CGPoint timeCenter = cell.timeLabel.center;
    CGRect leftViewFrame = cell.leftImageView.frame;
    CGRect rightViewFrame = cell.rightImageView.frame;
    
    /*
        显示时间框
     */
    if (messageModel.flag==0) { // 显示时间框
//        if (indexPath.row!=_messagesData.count-1) {//如果是数组最后一条 不显示时间戳 0925修改_gxf
            cell.timeView.hidden = NO;
            cell.timeLabel.text = messageModel.timeViewStr;
            if ([cell.timeLabel.text isEqualToString:@"今天"]) {
                leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-30-32-20)/2.0;
                
            } else {
                leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-150-32-20)/2.0;
            }
//        }
        
        if ([messageModel.type isEqualToNumber:@16]){
            cell.timeView.hidden = YES;
            timeMaxY = defaultSpaceWith;
        }
    } else { // 隐藏时间框
        cell.timeView.hidden = YES;
        timeMaxY = defaultSpaceWith;
    }
    timeCenter.x = cell.timeView.center.x;
    cell.timeLabel.center = timeCenter;
    cell.leftImageView.frame = leftViewFrame;
    rightViewFrame.size.width = cell.leftImageView.frame.size.width;
    rightViewFrame.origin.x = self.view.frame.size.width - cell.leftImageView.frame.size.width-16;
   
    cell.rightImageView.frame = rightViewFrame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width);
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    
    if (senderFrame.size.width>200) {
         senderFrame.size.width = 200;
    }
    
    senderFrame.origin.y = timeMaxY;
    avatarFrame.origin.y = timeMaxY;
    creatAtFrame.origin.y = timeMaxY;
    cell.senderLabel.frame = senderFrame;
    cell.userAvatarImageView.frame = avatarFrame;
    if (cell.senderLabel.text==nil) {
        creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame);
    } else {
        creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    }
    
    cell.createdAtLabel.frame = creatAtFrame;
    
    GanTHFrame.origin.x = CGRectGetMaxX(cell.createdAtLabel.frame);
    cell.ganTanHao.frame=GanTHFrame;
    if (messageModel.sendSuccessflag==1) {
        cell.ganTanHao.hidden=YES;
    }else{
        cell.ganTanHao.hidden=NO;
    }
    
    // Cells must inherit the table view's transform
    // This is very important, since the main table view may be inverted
    cell.transform = self.tableView.transform;
    
    // 定制bodyLabel,themeLabel高度
    // 设置文本行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    NSDictionary *attributeTheme = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    CGFloat width;
    
    NSDictionary *info = messageModel.info;
    
    /*
     定制回复label（对主题的回复）
     */
    CGRect responseFrame = cell.responseLabel.frame;
    if ([messageModel.type isEqualToNumber:@8]) { // 消息为评论message
        
        NSString * string;
        if ([messageModel.info[@"topicType"] isEqualToNumber:@4]) {
            string = [NSString stringWithFormat:@"回复@%@发起的投票",info[@"creatorName"]];
        }else{
            string = [NSString stringWithFormat:@"回复@%@发表的主题",info[@"creatorName"]];
        }
        cell.responseLabel.text = string;
        cell.responseLabel.hidden = NO;
        
//        NSLog(@"[messageModel.info[topicid]:%@",messageModel.info[@"topicId"]);
        
        /**
         *  对联系子话题的回复合并显示，不重复输出主题
         */
        if ([next_messageModel.info[@"topicId"] isEqual:messageModel.info[@"topicId"]]) {//&&!firstLoad
            cell.responseLabel.hidden = YES;
            cell.userAvatarImageView.hidden=YES;
        }else{
            cell.userAvatarImageView.hidden=NO;
        }
        responseFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
    } else {
        cell.responseLabel.hidden = YES;
        cell.userAvatarImageView.hidden = NO;
    }
    cell.responseLabel.frame = responseFrame;
    /* 
        定制主题存在（消息为topic,或者评论）
     */
    
    if ([messageModel.type isEqualToNumber:@7]||[messageModel.type isEqualToNumber:@8]) {
        NSMutableString * themstr=[NSMutableString stringWithFormat:@"%@",info[@"title"]];
        NSRange range=[themstr rangeOfString:@"发起投票:"];
        if (range.length!=0) {
            [themstr deleteCharactersInRange:range];
        }
        
        NSString * closedVote=@"";
        if ([messageModel.type isEqualToNumber:@7]){
            if ([messageModel.subType isEqualToNumber:@6]) {
                //关闭投票
                closedVote=@"关闭投票: ";
            }
        }
        cell.themeLabel.text = [NSString stringWithFormat:@"#%@%@#",closedVote,themstr];
        
        width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 2*defaultSpaceWith;
        
        CGRect themeFrame = cell.themeLabel.frame;
        if (cell.responseLabel.hidden) {
            themeFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        } else {
            
            themeFrame.origin.y = CGRectGetMaxY(cell.responseLabel.frame);
        }
        themeFrame.size.width = width;
        
//        CGRect themeRect = [info[@"title"] boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        CGRect themeRect = CGRectMake(0, 0, 0, 0);
        if (cell.themeLabel.text.length==0) {
            themeRect = [cell.themeLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeTheme context:nil];
        } else {
            cell.themeLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.themeLabel.text attributes:attributeTheme];
            themeRect = [cell.themeLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        }
        
        themeFrame.size.height = ceil(themeRect.size.height);
        
        /**
         *  对联系子话题的回复合并显示，不重复输出主题 so隐藏控件
         */
        if (cell.userAvatarImageView.hidden==YES) {
            cell.themeLabel.hidden = YES;
            themeFrame.size.height = 0;
        }else{
            cell.themeLabel.hidden = NO;
        }
        
        cell.themeLabel.frame = themeFrame;
        
        cell.detailButton.hidden = NO;
        cell.detailButton.tag = indexPath.row;
        [cell.detailButton addTarget:self action:@selector(onMessageDetailVCButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        cell.themeLabel.hidden = YES;
        cell.themeLabel.text = nil;
        cell.detailButton.hidden = YES;
        
        width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - defaultSpaceWith;
    }
    /*
        定制bodyLabel
     */
    CGRect frame = cell.bodyLabel.frame;
    frame.size.width = width;
    cell.bodyLabel.emojiDelegate = self;
    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
    if ([messageModel.type isEqualToNumber:@7]||[messageModel.type isEqualToNumber:@8]) {
        if ([messageModel.type isEqualToNumber:@7]) {
//            cell.bodyLabel.text = info[@"desc"];
            [cell.bodyLabel setEmojiText:info[@"desc"]];
        } else {
//            cell.bodyLabel.text = info[@"message"];
            [cell.bodyLabel setEmojiText:info[@"message"]];
        }
        
        frame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
//        cell.bodyLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
    } else {
//        cell.bodyLabel.text = info[@"message"];
        [cell.bodyLabel setEmojiText:info[@"message"]];
        frame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//        cell.bodyLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
        // 判断加入，退出群组
        if ([messageModel.type isEqualToNumber:@4]) {
            [cell.bodyLabel setEmojiText:[NSString stringWithFormat:@"%@退出小组",messageModel.sender]];
//            cell.bodyLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
        }  else if ([messageModel.type isEqualToNumber:@3] || [messageModel.type isEqualToNumber:@2]) {
            [cell.bodyLabel setEmojiText:[NSString stringWithFormat:@"欢迎%@加入小组",messageModel.sender]];
//            cell.bodyLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
        }
    }
//    if (cell.bodyLabel.text.length==0) {
        bodyFrame = [cell.bodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.bodyLabel.text attributes:attributes];
//        bodyFrame = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//
//    }
    frame.size.height = ceil(bodyFrame.size.height);
    cell.bodyLabel.frame = frame;
    [cell.bodyLabel sizeToFit];
    
    /**
     *  定制urlBodyLabel链接预加载内容
     */
    if ([messageModel.type isEqualToNumber:@16]){
//        NSLog(@"---------Message:%@",messageModel.info[@"content"]);
        cell.urlBodyLabel.hidden=NO;
        CGRect frame2 = cell.urlBodyLabel.frame;
        frame2.size.width = width;
        cell.urlBodyLabel.text = info[@"content"];
        frame2.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
        CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
        ubodyFrame = [cell.urlBodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        frame2.size.height = ceil(ubodyFrame.size.height)+23;
        cell.urlBodyLabel.frame = frame2;
    }else{
        cell.urlBodyLabel.hidden=YES;
    }
    
    
    // 存储高度，用于返回cell的高度
    CGFloat cellHeight = 0;
    if (frame.size.height==0) {
//        cellHeight = CGRectGetMaxY(cell.senderLabel.frame)+ 21;
        cellHeight = CGRectGetMaxY(cell.themeLabel.frame);
    } else {
        cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
        if ([messageModel.type isEqualToNumber:@16]){
            cellHeight = CGRectGetMaxY(cell.urlBodyLabel.frame);
        }
    }
    self.cellHeight = cellHeight;
    /*
        定制subImageView(子话题图片)
     */
    cell.voteView.hidden=YES;
    if ([messageModel.type isEqualToNumber:@7] || [messageModel.type isEqualToNumber:@8]) { // topic
        if ([messageModel.subType isEqualToNumber:@2] || ([messageModel.subType isEqualToNumber:@3]&&![messageModel.info[@"topicType"] isEqualToNumber:@4])) { // 图片子话题
            
            cell.subImageView.userInteractionEnabled = YES;
            cell.subImageView.hidden = NO;
            CGRect subImageFrame = cell.subImageView.frame;
            subImageFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
            CGFloat subW = [messageModel.info[@"thumbNail_w"] floatValue];
            CGFloat subH = [messageModel.info[@"thumbNail_h"] floatValue];
            CGFloat w = 0;
            CGFloat h = 0;
            if (subW>subH) {
                w = 180;
                h = 180*(subH/subW);
            } else {
                h = 180;
                w = 180*(subW/subH);
            }
            
            subImageFrame.size.width = w;
            subImageFrame.size.height = h;
            
            cell.subImageView.frame = subImageFrame;
            [cell.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.info[@"thumbNail"]]] placeholderImage:nil];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSubImageViewTapGestureClick:)];
            cell.subImageView.tag = indexPath.row;
            [cell.subImageView addGestureRecognizer:tap];
            
            // 存储高度，用于返回cell的高度
            cellHeight = CGRectGetMaxY(cell.subImageView.frame)+5;
            self.cellHeight = cellHeight;
        }else if ([messageModel.subType isEqualToNumber:@4]&&[messageModel.info[@"topicType"] isEqualToNumber:@4]) {
            cell.voteView.hidden=NO;
            cell.subImageView.hidden = YES;
            cell.subImageView.image = nil;
            
            width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 2*defaultSpaceWith;
            CGRect voteVFrame = cell.voteView.frame;
            
            if (info[@"message"]!=nil){
                voteVFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
            }else{
                voteVFrame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
            }
            voteVFrame.size.width = width;
            // 存储高度，用于返回cell的高度
            cell.voteView.frame=voteVFrame;
            cellHeight = CGRectGetMaxY(cell.voteView.frame)+5;
            
            NSString * str1;
            NSString * str2;
            NSString * str3;
            
            if ([messageModel.info[@"closed"] isEqualToNumber:@1]) {    str1=@"投票已结束";  }else{   str1=@"投票进行中";   }
            if ([messageModel.info[@"single"] isEqualToNumber:@1]) {    str2=@"可单选";  }else{   str2=@"可多选";   }
            if ([messageModel.info[@"anonymous"] isEqualToNumber:@1]) {    str3=@"匿名投票";  }else{   str3=@"记名投票";   }
            
            cell.voteLabel.text=[NSString stringWithFormat:@"%@(%@/%@)",str1,str2,str3];
            
            self.cellHeight = cellHeight;
        } else {
            cell.subImageView.hidden = YES;
            cell.subImageView.image = nil;
            cell.voteView.hidden=YES;
        }
        
    } else { // 非topic
        cell.subImageView.hidden = YES;
        cell.subImageView.image = nil;
        cell.voteView.hidden=YES;
        CGRect subImageFrame = cell.subImageView.frame;
        subImageFrame.size.height = 0;
        cell.subImageView.frame = subImageFrame;
        self.cellHeight = cellHeight;
    }
    [self.cellHeightArray insertObject:[NSNumber numberWithFloat:self.cellHeight] atIndex:indexPath.row];
    // 详情按钮的位置设置
    CGPoint detailBtnCenter = cell.detailButton.center;
    if (messageModel.flag==0) { // 显示时间头
        detailBtnCenter.y = (self.cellHeight-45)/2.0 +45+8;
        if ([messageModel.type isEqualToNumber:@16]){
            detailBtnCenter.y = self.cellHeight/2.0 +16;
        }
    } else {
        detailBtnCenter.y = self.cellHeight/2.0 +16;
    }
//    cell.voteIconImg.center=CGPointMake(cell.voteIconImg.center.x, 0);
    cell.detailButton.center = detailBtnCenter;
    
    return cell;
}

- (MessageTableViewCell *)autoCompletionCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.autoCompletionView dequeueReusableCellWithIdentifier:AutoCompletionCellIdentifier];
    cell.indexPath = indexPath;
    
    NSString *item = self.searchResult[indexPath.row];
    
    if ([self.foundPrefix isEqualToString:@"#"]) {
        item = [NSString stringWithFormat:@"# %@", item];
    }
    else if ([self.foundPrefix isEqualToString:@":"]) {
        item = [NSString stringWithFormat:@":%@:", item];
    }
    
    cell.titleLabel.text = item;
    cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    UserModel *userModel = self.groupUsers[indexPath.row];
//    NSString *str = [iconPath stringByAppendingString:self.usersAvatar[indexPath.row]];
     NSString *str = [iconPath stringByAppendingString:userModel.avatar];
    [cell.thumbnailView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {

        if (self.cellHeight==0) {
            return 44;
        }
        
        float hight=0;
        if (indexPath.row>=_cellHeightArray.count) {
            hight=_cellHeight;
        }else{
            hight=[_cellHeightArray[indexPath.row] floatValue];
        }
        
        return hight; //indexPath.row>=_cellHeightArray.count? _cellHeight: [_cellHeightArray[indexPath.row] floatValue];//self.cellHeight+5;
        
//        return self.cellHeight;
    }
    else {
        return kMinimumHeight;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.autoCompletionView]) {
        UIView *topView = [UIView new];
        topView.backgroundColor = self.autoCompletionView.separatorColor;
        return topView;
    }
    
    return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.autoCompletionView]) {
        return 0.5;
    }
    return 0.0;
}


#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.autoCompletionView]) {
        
        NSMutableString *item = [self.searchResult[indexPath.row] mutableCopy];
        
        if ([self.foundPrefix isEqualToString:@"@"] && self.foundPrefixRange.location == 0) {
            [item appendString:@":"];
        }
        else if ([self.foundPrefix isEqualToString:@":"]) {
            [item appendString:@":"];
        }
        
        [item appendString:@" "];
        
        [self acceptAutoCompletionWithString:item keepPrefix:YES];
    } else {
        
        XJTMessageTableViewCell *cell = (XJTMessageTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.detailButton.hidden==NO) {
            MessageDetailViewController *messageDetail = [[MessageDetailViewController alloc] init];
            MessageModel *model = self.messagesData[indexPath.row];
            messageDetail.topicId = model.info[@"topicId"];
            if ([model.type isEqualToNumber:@8]) {
                messageDetail.topicType = model.info[@"topicType"];
            }else if ([model.type isEqualToNumber:@7]){
                if ([model.subType isEqualToNumber:@6]) {
                    //关闭投票
                    messageDetail.topicType = [NSNumber numberWithInt:4];
                }else{
                    messageDetail.topicType = model.subType;
                }
            }
            //    messageDetail.topicId = self.messagesData[sender.tag];
            [self.navigationController pushViewController:messageDetail animated:YES];
        }
    }
}

// 点击cell上的按钮进入message详情页
- (void)onMessageDetailVCButtonClick:(UIButton *)sender{
    [self.textInputbar.textView resignFirstResponder];
    MessageDetailViewController *messageDetail = [[MessageDetailViewController alloc] init];
    MessageModel *model = self.messagesData[sender.tag];
    messageDetail.topicId = model.info[@"topicId"];
    if ([model.type isEqualToNumber:@8]) {
        messageDetail.topicType = model.info[@"topicType"];
    }else if ([model.type isEqualToNumber:@7]){
        messageDetail.topicType = model.subType;
    }
//    messageDetail.topicId = self.messagesData[sender.tag];
    [self.navigationController pushViewController:messageDetail animated:YES];
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you ovveride this method, to call super.
    [super scrollViewDidScroll:scrollView];
    
    if (self.unreadButton!=nil) {
        self.unreadButton.hidden = YES;
        [self.unreadButton removeFromSuperview];
        self.unreadButton = nil;
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
}


#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

- (void)textViewDidChangeSelection:(SLKTextView *)textView{
    [super textViewDidChangeSelection:textView];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    //    [self.tableView beginUpdates];
    //    [self.tableView endUpdates];
    if (self.tableView.contentSize.height>0) { //解决异常闪退  1014_gxf添加
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    }
}
#pragma mark 图片子话题的图片的手势点击事件
- (void)onSubImageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    UIImageView *imageView = (UIImageView *)gesture.view;
    
    MessageModel *model = self.messagesData[imageView.tag];
    
    if ([model.subType  isEqual: @3]) { // 地图
        MapViewController *mapVC = [[MapViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        mapVC.groupId  =self.groupId;
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
        picVC.imageInfo = model.info;
        [self presentViewController:picVC animated:NO completion:nil];
    }
    
}
// 成员头像的点击事件
- (void)onMessageViewUserIconTapGestureClick:(UITapGestureRecognizer *)gesture{
    
    UIImageView *userIconImageView = (UIImageView *)gesture.view;
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.messagesData[userIconImageView.tag-userIconImageViewTag];
    for (UserModel *userModel in self.groupUsers) {
        if ([model.userId isEqualToNumber:userModel.id]) {
            userPerVC.userPerfileModel = userModel;
            [self.navigationController pushViewController:userPerVC animated:YES];
        }
    }
}

#pragma mark delegate for imagePickerConreoller
// 协议中的方法 ,当用户选中图片时被调用
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    MainThemeViewController *themeVC = [[MainThemeViewController alloc] init];
    
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
        NSString *imageName = [url lastPathComponent];
        themeVC.imageName = imageName;
    } else {
        themeVC.imageName = @"origin.jpg";
    }
    
    themeVC.image = info[UIImagePickerControllerOriginalImage];
    themeVC.groupId = self.groupId;
    [self.navigationController pushViewController:themeVC animated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==2){
        [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        picker.allowsEditing = YES;
    } else if (buttonIndex==1) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark 未读消息按钮点击事件
- (void)checkInUnreadMessage{
    
    int count = self.unreadCount.intValue;
    if (count != 0) {
        
        if (count <= self.messagesData.count) {
            
            [self.tableView reloadData];
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            
            [self performSelector:@selector(scrollToUnreadMessage) withObject:nil afterDelay:1.0];
            
        } else {
            
            self.isScroToUnreadMess = YES;
            [self getUpMessageListData];
        }
        
        if (self.unreadButton!=nil) {
            self.unreadButton.hidden = YES;
            [self.unreadButton removeFromSuperview];
            self.unreadButton = nil;
        }
    }
}


- (void)scrollToUnreadMessage{

    int count = self.unreadCount.intValue;
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0]];
    
    [UIView animateWithDuration:2.0 animations:^{
        cell.alpha = 0;
    }];
    [UIView animateWithDuration:2.0 animations:^{
        cell.alpha = 1;
    }];
}

- (void)scrollerToMoreUnreadMess{
    NSLog(@"333");
    int count = self.unreadCount.intValue;
    
    if (count <= self.messagesData.count) {
        [self.tableView reloadData];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
        [self performSelector:@selector(scrollToUnreadMessage) withObject:nil afterDelay:1.0];
    } else {
        
        self.isScroToUnreadMess = YES;
        [self getUpMessageListData];
    }
}


- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
    
}



@end