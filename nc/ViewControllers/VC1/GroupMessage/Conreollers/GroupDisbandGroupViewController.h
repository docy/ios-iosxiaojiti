//
//  GroupDisbandGroupViewController.h
//  nc
//
//  Created by docy admin on 15/10/16.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^conveyBlock)(NSNumber *);

@interface GroupDisbandGroupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *groupUserLabel;

@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;

- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender;

- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender;

// 传值
@property (nonatomic, assign) NSInteger userCount;
@property (nonatomic ,retain) NSNumber *groupId;

@property (nonatomic ,copy) NSString *groupName;

@property (nonatomic, strong) conveyBlock conveyBlock;

@end
