//
//  InviteModeTableViewController.m
//  nc
//
//  Created by docy admin on 6/23/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "InviteModeTableViewController.h"
#import "InviteModeTableViewCell.h"
#import "AddressBookTableViewController.h"
#import "XJTSearchBar.h"

#define SearchBarHeight 40
#define headerHeight 20
@interface InviteModeTableViewController () <UITextFieldDelegate>

@end

@implementation InviteModeTableViewController
{
    XJTSearchBar *_searchBar;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"选择邀请方式";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:nil];
    _searchBar = [XJTSearchBar searchBar];
    _searchBar.frame = CGRectMake(0, 0, self.view.frame.size.width, SearchBarHeight);
    _searchBar.placeholder = @"找人/输入手机号/姓名";
    _searchBar.delegate = self;
    self.tableView.tableHeaderView = _searchBar;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    InviteModeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"InviteModeTableViewCell" owner:self options:nil] lastObject];
    }
    
    if (indexPath.section == 0) {
        cell.titleLabel.text = @"通讯录";
        cell.IconImageView.image = [UIImage imageNamed:@"icon"];
    } else {
        cell.titleLabel.text = @"邮箱";
        cell.IconImageView.image = [UIImage imageNamed:@"icon"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return headerHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddressBookTableViewController *abVC = [[AddressBookTableViewController alloc] init];
    if (indexPath.section == 0) {
        abVC.isTellPhone = YES;
    } else {
        abVC.isTellPhone = NO;
    }
    
    [self.navigationController pushViewController:abVC animated:YES
     ];
}


@end
