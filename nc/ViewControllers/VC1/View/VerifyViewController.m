//
//  VerifyViewController.m
//  nc
//
//  Created by docy admin on 6/13/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "VerifyViewController.h"

@interface VerifyViewController ()

@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    // 设置导航栏属性
    [self setUpNavAttribute];
}

- (void)setUpNavAttribute
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleDone target:self action:@selector(onSendButtonClick)];
    self.navigationItem.title = @"验证信息";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 发送按钮的点击事件
- (void)onSendButtonClick
{
    
}

@end
