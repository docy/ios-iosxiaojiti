//
//  MainThemeViewController.m
//  nc
//
//  Created by docy admin on 6/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MainThemeViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "MessageViewController.h"
#import "MessageModel.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "NSString+XJTString.h"



@interface MainThemeViewController () <UITextViewDelegate>
@property (nonatomic, retain) UIBarButtonItem *sendBarButton;
@end

@implementation MainThemeViewController
@synthesize titleString,detailString;

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置导航栏属性
    [self setUpMainThemeViewData];
    
    self.themeTextView.text=self.titleString;
    self.descTextView.text=self.detailString;
    
    if (self.themeTextView.text.length!=0) {
        self.themeLabel.hidden = YES;
    }
    if (self.descTextView.text.length!=0) {
        self.descLabel.hidden = YES;
    }
}

// 设置导航栏属性
- (void)setUpMainThemeViewData
{
    self.navigationItem.title = @"主题";
    UIBarButtonItem *item = [UIBarButtonItem itemWithTitle:@"发送" target:self action:@selector(onSendThemeButtonClick:)];
    self.navigationItem.rightBarButtonItem = item;
    self.sendBarButton = item;
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMainThemeViewBackBtnClick)];
    
    self.themeTextView.delegate = self;
    self.descTextView.delegate = self;
    
    if (self.image) {
        
        CGRect rect = self.imageView.frame;
        CGFloat width = self.image.size.width;
        CGFloat height = self.image.size.height;
        
        if (width>height) {
            rect.size.height = 180*(height/width);
        } else {
            rect.size.width = 180*(width/height);
        }
        
        self.imageView.frame = rect;
        self.imageView.image = self.image;
        self.imageView.hidden = NO;
    } else {
        self.imageView.hidden = YES;
        self.imageView.image = nil;
    }
    //设置等待view位置大小
    _whiteView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _ActivitView.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2-40);
    _whiteLable.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    _whiteView.hidden=YES;
}

- (void)onMainThemeViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 发送主题按钮点击事件
- (void)onSendThemeButtonClick:(UIBarButtonItem *)sender
{
    NSString *title = [NSString stringThrowOffLinesWithString:self.themeTextView.text];
    if (title.length!=0) { // 主题必须填写
        // 创建topic
        _whiteView.hidden=NO;
        [self sendTopicMessageWithTitle:title];
        sender.enabled = NO;
    } else {
        [ToolOfClass showMessage:@"请填写标题"];
    }
}

// 创建topic
- (void)sendTopicMessageWithTitle:(NSString *)title
{
    self.sendBarButton.enabled = NO;
    [self.themeTextView resignFirstResponder];
    [self.descTextView resignFirstResponder];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"groupId"] = self.groupId;
    parameter[@"title"] = title;
//    parameter[@"clientId"] = [self setUpClientId];
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    if (self.descTextView.text.length!=0) {
        parameter[@"desc"] = self.descTextView.text;
    }
    
    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    if (self.imageView.hidden==NO) { // 发表图片子话题
        NSString *postStr = nil;
        NSData *imageData = UIImageJPEGRepresentation(self.image, 1);
        if (self.imageName==nil) {
            self.imageName=@"image";
        }
        
        if (self.latitude!=0 && self.longitude!=0) {
            parameter[@"longitude"] = @(self.longitude);
            parameter[@"latitude"] = @(self.latitude);
            postStr = [creatMapTopic copy];
        
        } else {
            postStr = [creatImageTopic copy];
        }
        
        [manager POST:postStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"image" fileName:self.imageName mimeType:@"image/jpg"];
            
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {// 发送成功
                [ToolOfClass showMessage:@"发送成功!"];
                
                self.sendBarButton.enabled = YES;
                
                if ([postStr isEqualToString:creatMapTopic]) {
                    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
                
//                if (self.backblock) {
//                    self.backblock();
//                }
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
            self.navigationItem.rightBarButtonItem.enabled=YES;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:@"发送失败!"];
            self.navigationItem.rightBarButtonItem.enabled=YES;
        }];
        
    } else {

        [manager POST:creatPostTopic parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) {// 发送成功
                [ToolOfClass showMessage:@"发送成功!"];
                
                self.sendBarButton.enabled = YES;
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
            self.navigationItem.rightBarButtonItem.enabled=YES;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.navigationItem.rightBarButtonItem.enabled=YES;
            [ToolOfClass showMessage:@"发送失败!"];
        }];
    }
}
// 获取clientID（自定义）
//- (NSString *)setUpClientId
//{
//    NSString *clientId = nil;
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];
//    NSDate *data = [NSDate date];
//    clientId = [NSString stringWithFormat:@"%@_%@_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],self.groupId,[formatter stringFromDate:data]];
//    return clientId;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.tag==9) {
        if (textView.text.length!=0) {
            self.themeLabel.hidden = YES;
        } else {
            self.themeLabel.hidden = NO;
        }
    } else {
        if (textView.text.length!=0) {
            self.descLabel.hidden = YES;
        } else {
            self.descLabel.hidden = NO;
        }
    }
}



@end
