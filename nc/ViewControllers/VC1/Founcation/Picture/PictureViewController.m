//
//  PictureViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "PictureViewController.h"
#import "PictureViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#define kCellHeight 100

@interface PictureViewController ()

@end

@implementation PictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpAttribute];
    self.pictureData = [NSMutableArray arrayWithObjects:@"时尚",@"人像",@"游记", nil];
    
}

// 设置基本属性
- (void)setUpAttribute
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.title = @"选择图片";
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"composedingwei" target:self action:@selector(onRightBarItemButtonClick)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(onBackButtonClick)];
}

// 导航栏左侧按钮的点击事件
- (void)onBackButtonClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 导航栏右侧按钮的点击事件
- (void)onRightBarItemButtonClick
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pictureData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"PictureViewCell";
    PictureViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"PictureViewCell" owner:self options:nil] lastObject];
    }
    
    cell.titleLabel.text = self.pictureData[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellHeight;
}

@end
