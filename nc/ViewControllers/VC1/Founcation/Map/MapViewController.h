//
//  MapViewController.h
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h> // 高德地图
#import <CoreLocation/CoreLocation.h>
#import <AMapSearchKit/AMapSearchObj.h>
#import <AMapSearchKit/AMapSearchAPI.h>
#import <MAMapKit/MAMapURLSearchConfig.h>

@class Message;

typedef void(^mapBlock)(Message *);


@interface MapViewController : UIViewController

@property (nonatomic, strong) mapBlock mapBlock;

@property (nonatomic, strong) CLLocationManager *locationManager; // GPS
@property (nonatomic, strong) MAMapView *mapView; // 高德地图

@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，


@property (nonatomic, assign) CGFloat latitude; // 纬度
@property (nonatomic, assign) CGFloat longitude; // 经度

@property (nonatomic, assign) BOOL isFromPrivateVC; // 判断是否是从私聊界面进入

@end
