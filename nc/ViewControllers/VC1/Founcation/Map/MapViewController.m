//
//  MapViewController.m
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MapViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "MainThemeViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"

@interface MapViewController () <MAMapViewDelegate,AMapSearchDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, strong) AMapReGeocode *reGeocode;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *detailStr;

@property (nonatomic, assign) BOOL isSearchFromDragging;

@property (nonatomic, retain) UIButton *currentAddressBtn;


@end

@implementation MapViewController
@synthesize search=_search;

- (void)initSearch
{
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:[MAMapServices sharedServices].apiKey Delegate:nil];
    self.search.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 添加高德地图
    [self setUpMapView];
    
    [self initSearch];
    [self setUpMapViewAttribute];
    // GPS定位
//    [self setUpGPS];
    
    
    
}
// 设置导航栏属性
- (void)setUpMapViewAttribute{
    self.view.backgroundColor = [UIColor grayColor];
    self.titleStr=[NSString string];
    self.detailStr=[NSString string];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:@"完成" target:self action:@selector(onRightButtonClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMapViewBackBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onMapViewBackBtnClick)];
    
    self.currentAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.currentAddressBtn setBackgroundImage:[UIImage imageNamed:@"map"] forState:UIControlStateNormal];
    CGSize size = self.currentAddressBtn.currentBackgroundImage.size;
    self.currentAddressBtn.frame = CGRectMake(self.view.bounds.size.width-size.width, self.view.bounds.size.height*0.8, size.width, size.height);
    [self.currentAddressBtn addTarget:self action:@selector(onMapViewCurrentAddressBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)onMapViewBackBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 逆地理编码查询

- (void)searchReGeocodeWithCoordinate:(CLLocationCoordinate2D)coordinate{
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    
    regeo.location                    = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    regeo.requireExtension            = YES;
    
    [self.search AMapReGoecodeSearch:regeo];
    
}
- (void)search:(id)searchRequest error:(NSString*)errInfo{
     NSLog(@"%s:, errInfo= %@", __func__, errInfo);
}
- (void)searchRequest:(id)request didFailWithError:(NSError *)error{
    NSLog(@"%s: searchRequest = %@, errInfo= %@", __func__, [request class], error);
}

#pragma mark - AMapSearchDelegate

/* 逆地理编码回调. */
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response{
    if (response.regeocode != nil && _isSearchFromDragging == NO)
    {
        
        self.reGeocode=response.regeocode;
        self.titleStr=self.reGeocode.formattedAddress;
        self.detailStr=@"";
//        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(request.location.latitude, request.location.longitude);
//        ReGeocodeAnnotation *reGeocodeAnnotation = [[ReGeocodeAnnotation alloc] initWithCoordinate:coordinate
//                                                                                         reGeocode:response.regeocode];
//        [self.mapView addAnnotation:reGeocodeAnnotation];
//        [self.mapView selectAnnotation:reGeocodeAnnotation animated:YES];
    }
    else /* from drag search, update address */
    {
//        [self.annotation setAMapReGeocode:response.regeocode];
//        [self.mapView selectAnnotation:self.annotation animated:YES];
    }
}

// 添加高德地图
- (void)setUpMapView{
    
    [[MAMapServices sharedServices] setApiKey:@"92b4b0f34495d7492e008e671a731df1"];
    [MAMapServices sharedServices].apiKey = @"92b4b0f34495d7492e008e671a731df1";
    
    self.mapView = [[MAMapView alloc] initWithFrame:self.view.bounds];
    
    self.mapView.delegate = self;
    if (self.latitude!=0&& self.longitude!=0) {
        self.navigationItem.rightBarButtonItem = nil;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.latitude,self.longitude);
        MACoordinateSpan span = MACoordinateSpanMake(0.01, 0.01);
        MACoordinateRegion region = MACoordinateRegionMake(coordinate, span);
        [self.mapView setRegion:region];
        
        MAPointAnnotation *pin = [[MAPointAnnotation alloc] init];
        pin.coordinate = coordinate;
        [self.mapView addAnnotation:pin];

        self.mapView.showsUserLocation = NO;
        
    } else {
        
        self.mapView.zoomLevel=16.1;
        //    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
        [self.mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];
        self.mapView.showsUserLocation = YES;
    }
    
    [self.view addSubview:self.mapView];
    
    
    // 设置指南针
    self.mapView.showsCompass = YES;
    self.mapView.compassOrigin = CGPointMake(self.view.bounds.size.width*0.9, self.view.bounds.size.height*0.01); // 指南针显示位置
    
    // 设置比例尺
    self.mapView.showsScale = YES;
    self.mapView.scaleOrigin = CGPointMake(self.view.bounds.size.width*0.05, self.view.bounds.size.height*0.01); // 指南针显示位置
    
    
    
    [self.view addSubview:self.currentAddressBtn];
}

#pragma mark 协议中的方法，用于显示大头针的提示框

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation{
    static NSString *identifier = @"MypinId";
    
    MAPinAnnotationView *pinView = (MAPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!pinView) {
        pinView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
//        pinView.canShowCallout = YES; // 弹出提示框
//        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//        leftBtn.frame = CGRectMake(0, 0, 120, 30);
//        [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        [leftBtn setTitle:@"haha" forState:UIControlStateNormal];
//        pinView.leftCalloutAccessoryView = leftBtn;
        pinView.animatesDrop = YES;
    
    } else {
        pinView.annotation = annotation;
    }
    
    return pinView;
}

- (void)mapView:(MAMapView *)mapView didLongPressedAtCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"%s",__func__);
    _isSearchFromDragging = NO;
    [self searchReGeocodeWithCoordinate:coordinate];
}

// 大头针视图的点击事件
- (void)mapView:(MAMapView *)mapView annotationView:(MAAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
}

- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation{
//    NSLog(@"%s",__func__);
    self.latitude = userLocation.coordinate.latitude;
    self.longitude = userLocation.coordinate.longitude;
//   NSLog(@"latitude：%f,longitude:%f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    [self searchReGeocodeWithCoordinate:userLocation.coordinate];
}

// 截屏按钮点击事件
- (void)onRightButtonClick{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    if (self.isFromPrivateVC==NO) { // 发表主题
        MainThemeViewController *themeVC = [[MainThemeViewController alloc] init];
        themeVC.image = [self.mapView takeSnapshotInRect:self.view.frame];
        themeVC.groupId=_groupId;
        themeVC.titleString=_titleStr;
        themeVC.detailString=_detailStr;
        themeVC.latitude = _latitude;
        themeVC.longitude = _longitude;
        //    themeVC.block = ^(Message *message) {
        //        if (self.mapBlock) {
        //            self.mapBlock(message);
        //        }
        //    };
        // 代理传值
        themeVC.block = ^() {
            [self.navigationController popViewControllerAnimated:NO];
        };
        
        themeVC.backblock = ^(){
            [self.navigationController popViewControllerAnimated:NO];
        };
        
        [self.navigationController pushViewController:themeVC animated:YES];
        
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else { // 私聊发表地图

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [ToolOfClass authToken];
        parameter[@"groupId"] = self.groupId;
        parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
        parameter[@"latitude"] = @(self.latitude);
        parameter[@"longitude"] = @(self.longitude);
        
        NSData *imageData = UIImageJPEGRepresentation([self.mapView takeSnapshotInRect:self.view.frame], 1);
        [manager POST:directChatSendImage parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    NSLog(@"%s",__func__);
}

- (void)onMapViewCurrentAddressBtnClick{
    self.mapView.zoomLevel=16.1;
    //    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];
    self.mapView.showsUserLocation = YES;
}

@end

