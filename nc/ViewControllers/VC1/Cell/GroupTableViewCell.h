//
//  GroupTableViewCell.h
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 群组展示cell

@interface GroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupDescribeLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *groupIconImageView;

@property (weak, nonatomic) IBOutlet UILabel *updatedAtLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageRemindView; // 消息提醒
@property (weak, nonatomic) IBOutlet UIButton *joinGroupBtn;

@property (weak, nonatomic) IBOutlet UIImageView *isPrivateGroup;

@end
