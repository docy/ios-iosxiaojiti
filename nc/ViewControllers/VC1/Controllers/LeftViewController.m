//
//  LeftViewController.m
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LeftViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

#import "AFNetWorkNotReachableView.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "MessageViewController.h"
#import "CreatGroupViewController.h"
#import "addGroupViewController.h"
#import "DirectChatViewController.h"
#import "GroupDetailViewController.h"

#import "OpenNetWorkViewController.h"
#import "GroupModel.h"
#import "GroupTableViewCell.h"
#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>
#import "MJRefresh.h"
#import "UIImageView+WebCache.h"
#import "UISegmentedControl+XJTSegmentedControl.h"
#import "GroupDisbandGroupViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "UIAlertView+AlertView.h"


#define SearchBarHeight 40

@interface LeftViewController ()<UINavigationBarDelegate,UIAlertViewDelegate>{
    UILabel * weiduNum;         //segm 群聊未读数提示
    UILabel * weiduNum2;        //segm 私聊未读数提示
    int PrvGrpUnreadCount;      //私聊未读数记录
    int GrpUnreadCount;         //群聊未读数记录
    
    UISegmentedControl *segContro;
}

@property(nonatomic, retain) UIImageView * bgImage;
@property (nonatomic, retain) NSTimer *timer;

//@property (nonatomic, retain) AFNetWorkNotReachableView *netWorkView;

@property (nonatomic, retain) NSNumber * willInGroupId;
@property (nonatomic, assign) BOOL  isPrivateGroup;
@property (nonatomic, retain) UIBarButtonItem *leftBarItem;
@property (nonatomic, retain) UIBarButtonItem *rightBarItem;
@property (nonatomic, retain) NSNumber *deleGroupId;
//@property (nonatomic, retain) NSNumber *PrivId; // 11.11

@end

@implementation LeftViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.view.backgroundColor = [UIColor whiteColor];
//        self.navigationItem.title = @"小组";
        self.groupData = [NSMutableArray array];
        self.PrivateGroupData = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLeftViewControllerData];
    [self getUpLeftViewUserGroupList];
    [self setUpLeftViewMJRefresh];
    
}

// 返回按钮的点击事件
- (void)ViewBackButtonClick{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
}

#pragma mark 设置属性
- (void)setUpLeftViewControllerData{

    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.deleGroupId = @(-1);
//    self.PrivId = @(-1);
//    self.leftBarItem = [UIBarButtonItem itemWithIcon:@"navigation_add" target:self action:@selector(onLeftViewAddGroupButtonClick)];
//    self.navigationItem.leftBarButtonItem = self.leftBarItem;
    
    self.leftBarItem = [UIBarButtonItem itemWithIcon:@"home" target:self action:@selector(ViewBackButtonClick)];
    self.navigationItem.leftBarButtonItem = self.leftBarItem;

    self.rightBarItem = [UIBarButtonItem itemWithIcon:@"navigation_search" target:self action:@selector(onLeftViewSearchButtonClick)];
    self.navigationItem.rightBarButtonItem = self.rightBarItem;
    
//    self.navigationItem.leftBarButtonItem =[UIBarButtonItem itemWithTarget:self action:@selector(ViewBackButtonClick)];
//    [UIBarButtonItem itemWithTarget:self action:@selector(onLeftViewAddGroupButtonClick)];
    [self.navigationItem setHidesBackButton:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    // 为添加群组注册观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewCreatGroup) name:@"creatGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewLogoutGroup:) name:@"logoutGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLeftViewNewMessage:) name:@"newMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(huidaoQianTai) name:@"huidaoQianTai" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewLogoutGroup:) name:disbandGroup object:nil]; // 解散群组
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewConveyGroupSuccess:) name:conveyGroupOwer object:nil]; // 转让群主成功
    
    //从通知进入时调用
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    // 切换公司通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpLeftViewUserGroupList) name:@"SwitchCurrentCompany" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUnReadConutPriGroup:) name:@"removeUnReadConutPriGroup" object:nil];
    self.isPrivateGroup = NO;
    segContro = [UISegmentedControl segmentedControlWithItems:@[@"小组",@"私信"] target:self action:@selector(onSegClick:)];
    
    UIView * viewSeg=[[UIView alloc] initWithFrame:segContro.frame];
    [viewSeg addSubview:segContro];
    CGRect frame=segContro.frame;
    
    weiduNum=[[UILabel alloc] initWithFrame:CGRectMake(frame.size.width/3, 4, 6, 6)];
    weiduNum.backgroundColor=[UIColor redColor];
    weiduNum.layer.cornerRadius=3;
    weiduNum.clipsToBounds=YES;
    [viewSeg addSubview:weiduNum];
    
    weiduNum2=[[UILabel alloc] initWithFrame:CGRectMake(frame.size.width/2+frame.size.width/3, 4, 6, 6)];
    weiduNum2.backgroundColor=[UIColor redColor];
    weiduNum2.layer.cornerRadius=3;
    weiduNum2.clipsToBounds=YES;
    [viewSeg addSubview:weiduNum2];
    
    weiduNum.hidden=YES;
    weiduNum2.hidden=YES;
    
    PrvGrpUnreadCount=0;
    GrpUnreadCount=0;
    
    self.navigationItem.titleView = viewSeg;
    
}

- (id)bgImage{
    if (_bgImage == nil) {
        _bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Direct messages"]];
    }
    
    return _bgImage;
}

- (void)onSegClick:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex==0) {
        self.isPrivateGroup = NO;
        self.navigationItem.rightBarButtonItem = self.rightBarItem;
        
//        if (self.groupData.count == 0) {
//            self.tableView.backgroundView = self.bgImage;
//        } else {
//            self.tableView.backgroundView = nil;
//            self.bgImage = nil;
//        }
        self.tableView.backgroundView = nil;
        
        
    } else {
        self.navigationItem.rightBarButtonItem = nil;
        self.isPrivateGroup = YES;
        
        if (self.PrivateGroupData.count == 0) {
            self.tableView.backgroundView = self.bgImage;
        } else {
            self.tableView.backgroundView = nil;
            self.bgImage = nil;
        }
        
    }
    [self.tableView reloadData];
    
}

//回到前台调用
-(void)huidaoQianTai{
    
    [self getUpLeftViewUserGroupList];
}

/**
 *  通过通知进入app 加载指定群组详情
 *
 *  @param notification 通知详情
 */

-(void)StartingFromTheNotification:(NSNotification*)notification{
    
    NSDictionary * dic= [notification userInfo];
//    NSLog(@"%s---%@",__func__,dic);
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    double delayInSecondsd = 0.3;
    dispatch_time_t popTimee = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecondsd * NSEC_PER_SEC));
    dispatch_after(popTimee, dispatch_get_main_queue(), ^(void){
    
        int indexrow=-1;
        for (int i=0; i<_groupData.count+_PrivateGroupData.count; i++) {
            GroupModel *mode=nil;
            int k=i;
            if (i<_groupData.count) {
                mode=[self.groupData objectAtIndex:k];
            }else{
                k=i-(int)_groupData.count;
                mode=[self.PrivateGroupData objectAtIndex:k];
            }
            
            if ([mode.id isEqualToNumber:dic[@"groupId"]]) {
                indexrow=0;
                if ([mode.category isEqualToNumber:@3]) {
                    segContro.selectedSegmentIndex=1;
                }else{
                    segContro.selectedSegmentIndex=0;
                }
                [self onSegClick:segContro];
                break;
            }
        }
        if (indexrow!=-1) {
            
        }
    });
    
    double delayInSeconds = 0.6;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        int indexrow=-1;
//        for (int i=0; i<_groupData.count; i++) {
//            GroupModel *model = self.groupData[i];
//            if ([model.id isEqualToNumber:dic[@"groupId"]]) {
//                indexrow=0;
//            }
//        }
//        if (indexrow!=-1) {
            NSIndexPath * indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
//        }
    });
}

- (void)setUpLeftViewMJRefresh{
    __block LeftViewController *wearSelf = self;
    [self.tableView addHeaderWithCallback:^{
        [wearSelf getUpLeftViewUserGroupList];
    }];
}

#pragma mark 通知

- (void)LeftViewConveyGroupSuccess:(NSNotification *)not{
    for (GroupModel *model in self.groupData) {
        
        if ([self.willInGroupId isEqualToNumber:model.id]) {
            
            model.creator = (NSNumber *)not.object;
            break;
        }
        [UIView animateWithDuration:1.0 animations:^{
            
        }];
    }
}

- (void)LeftViewRefreshAgainInBadNetWork{
//    NSLog(@"%s",object_getClassName(self));
    NetWorkBadView *badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
//    badView.frame = CGRectMake(0, 0, ScreenWidth, badNetWorkViewHeight);
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = badView;
}

- (void)LeftViewInGoodNetWork{
    if (self.tableView.tableHeaderView!=nil) {
        self.tableView.tableHeaderView = nil;
    }
    if (self.groupData.count==0) {
        [self getUpLeftViewUserGroupList];
    }
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewCreatGroup{
    [self getUpLeftViewUserGroupList];
}

- (void)LeftViewLogoutGroup:(NSNotification *)notification{
//    [self getUpLeftViewUserGroupList];
    for (NSInteger i=0; i<self.groupData.count; i++) {
        GroupModel *model = self.groupData[i];
        if ([model.id isEqualToNumber:notification.object]) {
            [self.groupData removeObjectAtIndex:i];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void)getLeftViewNewMessage:(NSNotification *)notification{
    NSArray *array = notification.object;
    NSDictionary * dict=array[0];
    
    
//    if ([self.deleGroupId isEqualToNumber:dict[@"groupId"]]) {
//        
//        self.deleGroupId = @(-1);
//        return;
//    }
    GroupModel *modeee = nil; //存储groupId 对应的那组数据
    // 遍历2组数据  找到groupId   区分是不是私信组的消息
    for (int i=0; i<_groupData.count+_PrivateGroupData.count; i++) {
        GroupModel *mode=nil;
        int k=i;
        if (i<_groupData.count) {
            mode=[self.groupData objectAtIndex:k];
        }else{
            k=i-(int)_groupData.count;
            mode=[self.PrivateGroupData objectAtIndex:k];
        }
        
        if ([mode.id isEqualToNumber:dict[@"groupId"]]) {
            modeee = mode;
//            if ([mode.category isEqualToNumber:@3]) {
//                segContro.selectedSegmentIndex=1;
//            }else{
//                segContro.selectedSegmentIndex=0;
//            }
            /**
             *  11.10更改
             */
            if (!((NSNumber*)dict[@"groupId"]==_willInGroupId)) { // 未选中小组的未读消息增加
                if (![mode.category isEqualToNumber:@3]) { //私信或者群组
                    
                    GrpUnreadCount++;
                }else{
                    
                    PrvGrpUnreadCount++;
                }
            }
            /**
             *  11.10更改
             */
            
            
//            if (![mode.category isEqualToNumber:@3]) { //私信或者群组
//
//                GrpUnreadCount++;
//            }else{
//
//                PrvGrpUnreadCount++;
//            }
            break;
        }
    }

    NSInteger count = self.groupData.count+self.PrivateGroupData.count;
    int i = 0;
    for (i=0;i<count;i++) {

        GroupModel *mode = nil;
        
        int k=i;
        
        if (i<_groupData.count) {
            mode=[self.groupData objectAtIndex:i];
        }else{
            k=i-(int)_groupData.count;
            mode=[self.PrivateGroupData objectAtIndex:k];
        }
        
        if (mode.id.intValue ==[(NSNumber*)dict[@"groupId"] intValue]) { // 刷新message
            mode.messages=array;
            int uc = [mode.unreadCount intValue];  //某一群组的未读数
            uc++;//未读数本地加1
        
            
            /**
             *  11.10更改
             */
            if ((NSNumber*)dict[@"groupId"]==_willInGroupId) {  //当前grpID相同不添加未读
                
//                mode.unreadCount=[NSNumber numberWithInt:0];
//                if (![modeee.category isEqualToNumber:@3]) {
//
//                    GrpUnreadCount--;
//                }else{
//                    PrvGrpUnreadCount--;
//                }
                
            } else {
//                mode.unreadCount=[NSNumber numberWithInt:0];
                
//                if (![modeee.category isEqualToNumber:@3]) {
//                    GrpUnreadCount--;
//                }else{
//                    PrvGrpUnreadCount--;
//                }

                mode.unreadCount=[NSNumber numberWithInt:uc];
            }
            
            
            if (![mode.category isEqualToNumber:@3]) {
                [self.groupData replaceObjectAtIndex:k withObject:mode];
            } else {
                [self.PrivateGroupData replaceObjectAtIndex:k withObject:mode];
            }
            
            if (self.isPrivateGroup && i >= _groupData.count) {
                NSIndexPath *te=[NSIndexPath indexPathForRow:k inSection:0];//刷新第i个section的第0行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
            }
            if (!self.isPrivateGroup && i < _groupData.count) {
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第i个section的第0行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
            }
//            NSIndexPath *te=[NSIndexPath indexPathForRow:k inSection:0];//刷新第i个section的第0行
//            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
    
    if (PrvGrpUnreadCount>0) {
        weiduNum2.hidden=NO;
    }else{
        weiduNum2.hidden=YES;
    }
    if (GrpUnreadCount>0) {
        weiduNum.hidden=NO;
    }else{
        weiduNum.hidden=YES;
    }
    
    if (i==count) {
//        self.PrivId = dict[@"groupId"];
//        
//        NSLog(@"PrivId==%@",self.PrivId);
        [self getUpLeftViewUserGroupList];
    }
}

- (void)removeUnReadConutPriGroup:(NSNotification *)not{
    NSLog(@"222222345");
    for (GroupModel *model in self.PrivateGroupData) {
        if ([not.object isEqualToNumber:model.id]) {
            PrvGrpUnreadCount-=model.unreadCount.intValue;
            model.unreadCount = @0;
            
            if (PrvGrpUnreadCount>0) {
                weiduNum2.hidden=NO;
            } else {
                weiduNum2.hidden=YES;
            }
            [self.tableView reloadData];
            break;
        }
    }
}

#pragma mark 获取数据

- (void)getUpLeftViewUserGroupList{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [manager GET:userGroupsPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            [self.groupData removeAllObjects];
            [self.PrivateGroupData removeAllObjects];
            
            PrvGrpUnreadCount=0;
            GrpUnreadCount=0;
            
            for (NSDictionary *dict in responseObject[@"data"]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if ([model.category isEqualToNumber:@3]) { // 私聊群组
                    [self.PrivateGroupData addObject:model];
                    PrvGrpUnreadCount+=[model.unreadCount intValue];
                } else {
                    [self.groupData addObject:model];
                    GrpUnreadCount+=[model.unreadCount intValue];
                }
            }
            
            if (PrvGrpUnreadCount>0) {
                weiduNum2.hidden=NO;
            }else{
                weiduNum2.hidden=YES;
            }
            if (GrpUnreadCount>0) {
                weiduNum.hidden=NO;
            }else{
                weiduNum.hidden=YES;
            }
            
            [self.tableView headerEndRefreshing];
            [self.tableView reloadData];
            
            if (self.isPrivateGroup) {
                if (self.PrivateGroupData.count == 0) {
                    self.tableView.backgroundView = self.bgImage;
                } else {
                    self.tableView.backgroundView = nil;
                    self.bgImage = nil;
                }
            } else {
//                if (self.groupData.count == 0) {
//                    self.tableView.backgroundView = self.bgImage;
//                } else {
//                    self.tableView.backgroundView = nil;
//                    self.bgImage = nil;
//                }
                
                self.tableView.backgroundView = nil;
            }
            
        }else if([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
        }else{
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:@"加载失败，请检查网络"];
        [self.tableView headerEndRefreshing];
    }];
}

#pragma mark 导航栏点击事件

- (void)onLeftViewSearchButtonClick{
    addGroupViewController *addGroupVC = [[addGroupViewController alloc] init];
    addGroupVC.block = ^() {
        [self getUpLeftViewUserGroupList];
    };
    addGroupVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addGroupVC animated:YES];
}
- (void)onLeftViewAddGroupButtonClick{
    CreatGroupViewController *creatGroupVC = [[CreatGroupViewController alloc] init];
    creatGroupVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:creatGroupVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isPrivateGroup==NO) {
        return self.groupData.count;
    } else {
        return self.PrivateGroupData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"GroupTableViewCellId";
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GroupTableViewCell" owner:self options:nil] lastObject];
    }
    cell.joinGroupBtn.hidden = YES;
    GroupModel *model =  nil;
    if (self.isPrivateGroup==NO) {
        model =  self.groupData[indexPath.row];
    } else {
        model =  self.PrivateGroupData[indexPath.row];
    }
    
//    if ([model.id isEqualToNumber:self.PrivId]) {
//        NSLog(@"22");
//        PrvGrpUnreadCount -= model.unreadCount.intValue;
//        model.unreadCount = @0;
//        if (PrvGrpUnreadCount>0) {
//            weiduNum2.hidden=NO;
//        } else {
//            weiduNum2.hidden=YES;
//        }
//        
////        self.PrivId = @(-1);
//    }
    
    cell.groupNameLabel.text = model.name;
    
    if (model.category.intValue == 4) {
        cell.isPrivateGroup.hidden = NO;
    } else {
        cell.isPrivateGroup.hidden = YES;
    }
    
    if ([model.unreadCount isEqualToNumber:@0]) {
        cell.messageRemindView.hidden = YES;
    } else {
        cell.messageRemindView.hidden = NO;
        
        CGRect frame = cell.messageRemindView.frame;
        if (model.unreadCount.intValue>=100) { // 信息大于100，不显示数字
            frame.size.width = 10;
            frame.size.height = 10;
            frame.origin.x = CGRectGetMaxX(cell.groupIconImageView.frame)-7;
            frame.origin.y = CGRectGetMinY(cell.groupIconImageView.frame)-3;
            cell.messageRemindView.layer.cornerRadius = 5;
            cell.messageRemindView.text = @"";
        } else {
            frame.size.width = 18;
            frame.size.height = 13;
            frame.origin.x = CGRectGetMaxX(cell.groupIconImageView.frame)-15;
            frame.origin.y = CGRectGetMinY(cell.groupIconImageView.frame)-2;
            
            cell.messageRemindView.layer.cornerRadius = 7;
            cell.messageRemindView.text = model.unreadCount.stringValue;
        }
        cell.messageRemindView.frame = frame;
    }
    NSDictionary *message = [model.messages lastObject];
    int type = [message[@"type"] intValue];
    if ([model.category isEqualToNumber:@3]) {
        
        if (type == 12) {
            cell.groupDescribeLabel.text = @"发表图片";
        } else if (type==13) {
            cell.groupDescribeLabel.text = @"发表地图";
        } else {
            cell.groupDescribeLabel.text = message[@"info"][@"message"];
        }
        
    } else {
        
        
        NSString *sender = message[@"sender"];
        NSString *text = [NSString string];
        if (type==3||type==2) { // 加入群组
            text = [sender stringByAppendingString:@"加入小组"];
        } else if (type==4) { // 退出群组
            text = [sender stringByAppendingString:@"退出小组"];
        } else if (type==7) { // 主题
            
            int subType = [message[@"subType"] intValue];
            if (subType==7) {
                text = [sender stringByAppendingFormat:@"发表链接"];
            } else if (subType==2) {
                text = [sender stringByAppendingFormat:@"发表图片主题"];
            } else if (subType==3) {
                text = [sender stringByAppendingFormat:@"发表地图主题"];
            } else if (subType==4) {
                text = [sender stringByAppendingFormat:@"发表投票主题"];
            } else {
//                text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"desc"]];
                text = [sender stringByAppendingString:@"发表主题"];
            }
            
//            if (message[@"info"][@"desc"]==nil) {
//                text = [sender stringByAppendingString:@"发表主题"];
//            } else {
//                text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"desc"]];
//            }
        } else if (type==8) { // 评论
            
            int subType = [message[@"subType"] intValue];
            if (subType==2) {
                text = [sender stringByAppendingFormat:@":回复图片"];
            } else {
               text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"message"]];
            }
        } else {
            text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"message"]];
        }
        
        // 自定义text属性(加粗)
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:sender];
        [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0] range:range];
        cell.groupDescribeLabel.attributedText = str;

    }
    
    cell.updatedAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:message[@"updatedAt"]];
    if (model.logo==nil) {
        
    }else{
        [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    }
   return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return groupCellHeight;
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    GroupModel *model = nil;
    
    if (self.isPrivateGroup==NO) {
        model = self.groupData[indexPath.row];
        MessageViewController *messageVC = [[MessageViewController alloc] init];
        messageVC.hidesBottomBarWhenPushed = YES;
        
        messageVC.groupId = model.id;
        _willInGroupId=model.id;
        messageVC.groupName = model.name;
        messageVC.unreadCount = model.unreadCount;
        messageVC.broadcast = model.broadcast.intValue;
        GrpUnreadCount-=[model.unreadCount intValue];
        messageVC.cellBlock = ^(void) {
            model.unreadCount = @0;
            _willInGroupId = @(-1);
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:messageVC animated:YES];
    } else {
        model = self.PrivateGroupData[indexPath.row];
        DirectChatViewController *directVC = [[DirectChatViewController alloc] init];
        directVC.groupId = model.id;
        _willInGroupId=model.id;
        directVC.groupName = model.name;
        PrvGrpUnreadCount-=[model.unreadCount intValue];
        directVC.cellBlock = ^(void) {
            model.unreadCount = @0;
            _willInGroupId = @(-1);
            [self.tableView reloadData];
        };
        directVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:directVC animated:YES];
    }
    
    if (PrvGrpUnreadCount>0) {
        weiduNum2.hidden=NO;
    }else{
        weiduNum2.hidden=YES;
    }
    if (GrpUnreadCount>0) {
        weiduNum.hidden=NO;
    }else{
        weiduNum.hidden=YES;
    }
    
//    if ([model.category isEqualToNumber:@1]) {
//        MessageViewController *messageVC = [[MessageViewController alloc] init];
//        messageVC.hidesBottomBarWhenPushed = YES;
//        
//        messageVC.groupId = model.id;
//        _willInGroupId=model.id;
//        messageVC.groupName = model.name;
//        messageVC.cellBlock = ^(void) {
//            model.unreadCount = @0;
//            [self.tableView reloadData];
//        };
//        [self.navigationController pushViewController:messageVC animated:YES];
//    } else if ([model.category isEqualToNumber:@3]) {
//        DirectChatViewController *directVC = [[DirectChatViewController alloc] init];
//        directVC.groupId = model.id;
//        model.unreadCount = @0;
//        [self.tableView reloadData];
//        directVC.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:directVC animated:YES];
//    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除会话" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        GroupModel *model = self.PrivateGroupData[indexPath.row];
        
        self.deleGroupId = model.id;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [ToolOfClass authToken];
        [manager POST:[NSString stringWithFormat:DeleteDirectChat,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@0]) {
                [ToolOfClass showMessage:@"成功删除会话!"];
                
                if (model.unreadCount.intValue>0) {
                    PrvGrpUnreadCount-=model.unreadCount.intValue;
                }
                
                [self.PrivateGroupData removeObject:model];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                if (self.PrivateGroupData.count == 0) {
                    self.tableView.backgroundView = self.bgImage;
                } else {
                    self.tableView.backgroundView = nil;
                    self.bgImage = nil;
                }
                
                
                if (PrvGrpUnreadCount>0) {
                    weiduNum2.hidden=NO;
                }else{
                    weiduNum2.hidden=YES;
                }
                
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }];
    deleteAction.backgroundColor = [UIColor colorWithRed:197/255.0 green:196/255.0 blue:201/255.0 alpha:1];
    
    UITableViewRowAction *logOutAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"退出小组" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        GroupModel *model = self.groupData[indexPath.row];
        
        self.deleGroupId = model.id;
        
        NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        
        if (model.creator==nil) {
            [UIAlertView alertViewWithTitle:@"该小组暂时没有组长，暂时不能退出小组"];
            return;
        }
        
        if ([num isEqualToNumber:model.creator]) {
            
            [UIAlertView alertViewWithTitle:@"退出小组" subTitle:
             @"你是该小组的组长，请先转让组长身份" target:self];
        } else {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            parameter[@"accessToken"] = [ToolOfClass authToken];
            [manager POST:[NSString stringWithFormat:logoutGroupPath,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] isEqualToNumber:@0]) {
                    
                    [ToolOfClass showMessage:@"成功退出小组!"];
                    
                    if (model.unreadCount.intValue>0) {
                        GrpUnreadCount-=model.unreadCount.intValue;
                    }
                    
                    [self.groupData removeObject:model];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    if (self.groupData.count == 0) {
                        self.tableView.backgroundView = nil;
                    }
                    
                    if (GrpUnreadCount>0) {
                        weiduNum.hidden=NO;
                    }else{
                        weiduNum.hidden=YES;
                    }
                    
                } else {
                    [ToolOfClass showMessage:responseObject[@"message"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
        }
        
    }];
    logOutAction.backgroundColor = [UIColor colorWithRed:197/255.0 green:196/255.0 blue:201/255.0 alpha:1];
    
    return self.isPrivateGroup==YES? @[deleteAction]:@[logOutAction];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.tabBarController.tabBar.hidden = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    self.PrivId = @(-1);
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)navigationBar:(UINavigationBar *)navigationBar didPushItem:(UINavigationItem *)item{
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.translucent = YES;
//    [self.navigationController.navigationBar setAlpha:1.0];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        for (GroupModel *model in self.groupData) {
            
            if ([self.deleGroupId isEqualToNumber:model.id]) {
                
//                GroupDisbandGroupViewController *disband = [[GroupDisbandGroupViewController alloc] initWithNibName:@"GroupDisbandGroupViewController" bundle:[NSBundle mainBundle]];
//                disband.groupId = model.id;
//                disband.userCount = model.userCount.integerValue;
//                disband.groupName = model.name;
//                disband.hidesBottomBarWhenPushed = YES;
//                disband.conveyBlock = ^(NSNumber *userId){
//                    model.creator = userId;
//                };
//                [self.navigationController pushViewController:disband animated:YES];
                
                
                GroupDetailViewController *detail = [[GroupDetailViewController alloc] init];
                detail.groupId = model.id;
                [self.navigationController pushViewController:detail animated:YES];
                
                break;
            }
        }
    }
}

@end
