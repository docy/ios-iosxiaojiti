//
//  LoginViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import <AFNetworking/AFNetworking.h>
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "PersonalMessageModel.h"
#import "ToolOfClass.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import <Socket_IO_Client_Swift/Socket_IO_Client_Swift-Swift.h>
#import "ResetPasswordViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}
//服务器端设置当前用户ID对应的百度Token
-(void)pushToken{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    if ([info objectForKey:@"deviceToken"]==nil) {
        return;
    }
    parameter[@"pushToken"]=[info objectForKey:@"deviceToken"];//[BPush getChannelId];
    parameter[@"os"]=@"ios";

    [manager POST:SetPushToken parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@1]) {
            NSLog(@"error is %@",responseObject[@"error"]);
        }else if ([responseObject[@"code"] isEqualToNumber:@0]) {
            NSLog(@"Dic is %@",responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error is %@",error);
    }];
}

// 注册新用户
- (IBAction)onRegisterNewUserButtonClick:(id)sender {
    self.loginButton.enabled=YES;
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

// 登录按钮点击事件
- (IBAction)LoginButtonClick:(id)sender {
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
//    [SVProgressHUD showWithStatus:@"正在登录..."];
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    if (self.phoneTextField.text.length==0||self.passwordTextField.text.length==0) {
        [ToolOfClass showMessage:@"请完善信息"];
    } else {
        self.loginButton.enabled=NO;
        [self LoginViewLoginRequestWithAccount:self.phoneTextField.text passWord:self.passwordTextField.text];
    }
}

- (IBAction)onLoginResetPasswordBtnClick:(id)sender {
//    self.loginButton.enabled=YES;pjnmn
    ResetPasswordViewController *resetPass = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];

    [self.navigationController pushViewController:resetPass animated:YES];
    
}

- (void)LoginViewLoginRequestWithAccount:(NSString *)account passWord:(NSString *)password{
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:account forKey:@"name"];
    [info setObject:password forKey:@"password"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = account;
    parameter[@"password"] = password;
    
    [manager POST:LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@0] ) {

            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
			[info setObject:data[@"name"] forKey:@"name"];
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];

            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:@"登录失败"];
        self.loginButton.enabled=YES;
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

#define ScreenWidth [[UIScreen mainScreen] bounds].size.width//获取屏幕宽度，兼容性测试

//-(void)showMessage:(NSString *)message{
//    UIWindow * window = [UIApplication sharedApplication].keyWindow;
//    UIView *showview =  [[UIView alloc]init];
//    showview.backgroundColor = [UIColor blackColor];
//    showview.frame = CGRectMake(1, 1, 1, 1);
//    showview.alpha = 1.0f;
//    showview.layer.cornerRadius = 5.0f;
//    showview.layer.masksToBounds = YES;
//    [window addSubview:showview];
//    
//    UILabel *label = [[UILabel alloc]init];
//    CGSize LabelSize = CGSizeMake(ScreenWidth, 100);//[message sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(290, 9000)];
//    label.frame = CGRectMake(10, 5, LabelSize.width, LabelSize.height);
//    label.text = message;
//    label.textColor = [UIColor whiteColor];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont boldSystemFontOfSize:13];
//    [showview addSubview:label];
//    showview.frame = CGRectMake((ScreenWidth - LabelSize.width - 20)/2,  200, LabelSize.width+20, LabelSize.height+10);
//    [UIView animateWithDuration:10.5 animations:^{
//        showview.alpha = 0;
//    } completion:^(BOOL finished) {
//        [showview removeFromSuperview];
//    }];
//}


@end
