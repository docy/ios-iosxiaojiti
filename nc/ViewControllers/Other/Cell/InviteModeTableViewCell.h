//
//  InviteModeTableViewCell.h
//  nc
//
//  Created by docy admin on 6/23/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 通讯录选择方式cell

@interface InviteModeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *IconImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
