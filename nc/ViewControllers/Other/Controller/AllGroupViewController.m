//
//  AllGroupViewController.m
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import "AllGroupViewController.h"
#import "ListDataTableViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "UserModel.h"
#import "ToolOfClass.h"
#import "UIImageView+WebCache.h"
#import "GroupModel.h"
#import "UserJionedGroupTableViewCell.h"
#import "GroupDetailViewController.h"

@interface AllGroupViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) NSMutableArray *groups;   //公司小组列表
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AllGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.groups =[[NSMutableArray alloc] init];
    self.navigationItem.title = @"所有小组";
    [self getUpGroupsListData];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(AllGroupViewBack)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view from its nib.
}

- (void)AllGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpGroupsListData
{
    [self.groups removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:allCompanyGroups,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue] == 0) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if ([dict[@"category"] intValue] == 1) {
                    
                    GroupModel *model = [[GroupModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    [self.groups addObject:model];
                }
            }
            [self.tableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *celId = @"cellId";
    UserJionedGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:celId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"UserJionedGroupTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CGRect rect = cell.statusLabel.frame;
    rect.size.height = 0.5;
    cell.statusLabel.frame = rect;
    
    if (indexPath.row==self.groups.count-1) {
        cell.statusLabel.hidden = YES;
    } else {
        cell.statusLabel.hidden = NO;
    }
    cell.lockIgView.hidden = YES;
    GroupModel *model = self.groups[indexPath.row];
    cell.groupName.text = model.name;
    [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    cell.groupUserCount.text = [NSString stringWithFormat:@"%@人",model.userCount];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupDetailViewController *detail = [[GroupDetailViewController alloc] initWithNibName:@"GroupDetailViewController" bundle:[NSBundle mainBundle]];
    GroupModel *model = self.groups[indexPath.row];
    detail.groupId = model.id;
    detail.SuperViewStr = @"chakan";
    [self.navigationController pushViewController:detail animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
