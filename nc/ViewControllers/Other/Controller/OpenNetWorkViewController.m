//
//  OpenNetWorkViewController.m
//  nc
//
//  Created by docy admin on 15/9/15.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "OpenNetWorkViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

@interface OpenNetWorkViewController ()

@end

@implementation OpenNetWorkViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        self.navigationItem.title = @"网络无法连接";
        self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onOpenNetWorkViewBackItemClick)];
    }
    return self;
}
- (void)onOpenNetWorkViewBackItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
