//
//  CompanyListTVCell.h
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"
@interface CompanyListTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *companyImageView;
@property (weak, nonatomic) IBOutlet FXLabel *companyName;
@property (weak, nonatomic) IBOutlet FXLabel *creatorLabel;
@property (weak, nonatomic) IBOutlet FXLabel *userCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *settinButClick;

@end
