//
//  ExitCompanyViewController.h
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"


typedef void(^conveyBlock)(NSNumber *);

@interface ExitCompanyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *namelabel;

- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender;

- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *exitAndTransf;
@property (weak, nonatomic) IBOutlet UIButton *dismissCompanyBut;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;

// 传值
@property (nonatomic, assign) NSInteger userCount;
@property (nonatomic ,retain) NSNumber *groupId;   //公司id
@property (nonatomic ,retain) CompanyModel *model;  //公司详情

@property (nonatomic, strong) conveyBlock conveyBlock;

@end
