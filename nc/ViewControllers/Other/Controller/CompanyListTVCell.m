//
//  CompanyListTVCell.m
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import "CompanyListTVCell.h"


@implementation CompanyListTVCell

- (void)awakeFromNib {
    // Initialization code
}

-(FXLabel*)companyName{
    _companyName.shadowColor = nil;
    _companyName.shadowColor = [UIColor blackColor];
    _companyName.shadowBlur=2.0f;
    return _companyName;
}

-(FXLabel*)creatorLabel{
    _creatorLabel.shadowColor = nil;
    _creatorLabel.shadowColor = [UIColor blackColor];
    _creatorLabel.shadowBlur=2.0f;
    return _creatorLabel;
}

-(FXLabel*)userCountLabel{
    _userCountLabel.shadowColor = nil;
    _userCountLabel.shadowColor = [UIColor blackColor];
    _userCountLabel.shadowBlur=2.0f;
    return _userCountLabel;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
