//
//  CompanySetViewController.m
//  
//
//  Created by guanxf on 15/10/23.
//
//

#import "CompanySetViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyModel.h"
#import "AddAddressBookViewController.h"
#import "AddBookViewController.h"
#import "ToolOfClass.h"
#import "ExitCompanyViewController.h"
#import "AllGroupViewController.h"
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "CompanyInviteCodeViewController.h"
#import "UIAlertView+AlertView.h"
#import "transferPowerViewController.h"

@interface CompanySetViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, assign) BOOL cannotDisband;

@property (nonatomic, copy) NSString *logoName; // 公司logo名

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *companyNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *descTV;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;

@property (weak, nonatomic) IBOutlet UILabel *userCount;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;

@property (weak, nonatomic) IBOutlet UIButton *exitAndTransf;
@property (weak, nonatomic) IBOutlet UIButton *dismissCompanyBut;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;


@end

@implementation CompanySetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self RegisterCreatCompanyViewSetUpData];
    [self setUsersCompanyList];
    [self updataViewData];
}
// 设置基本属性
- (void)RegisterCreatCompanyViewSetUpData
{
    float hight=172;
    if (iPhone6) {
        hight=190;
    }else if (iPhone6Plus){
        hight=200;
    }
//    self.logoImageView.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight/2);
    [self.bgImage sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:_model.logoUrlOrigin]] placeholderImage:nil];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = @"集体设置";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(RegisterCreatCompanyViewBackBtnClick)];
    UIBarButtonItem * butitem = [UIBarButtonItem itemWithTitle:@"完成" target:self action:@selector(onUpdataButtonClick)]; //alloc] initWithTitle:@"修改" style:UIBarButtonItemStyleDone target:self action:@selector(onUpdataButtonClick)];
    //    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:getImageBeforeRegisterPath,@"company"]] placeholderImage:nil];
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenHeight+200);
    
}

-(void)setButtonShowAndHidden{
    UIBarButtonItem * butitem = [UIBarButtonItem itemWithTitle:@"完成" target:self action:@selector(onUpdataButtonClick)];
    
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    if ([num isEqualToNumber:_model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
        _exitButton.hidden=YES;
        _dismissCompanyBut.hidden= NO;
        
        self.companyNameLabel.enabled=YES;
        self.descriptionLabel.editable=YES;
        self.cameraButton.hidden=NO;
        self.navigationItem.rightBarButtonItem=butitem;
    }else{
        _exitButton.hidden=NO;
        _dismissCompanyBut.hidden=YES;
        
        self.companyNameLabel.enabled=NO;
        self.descriptionLabel.editable=NO;
        self.cameraButton.hidden=YES;
        self.navigationItem.rightBarButtonItem =nil;
    }
}

// 返回按钮
- (void)RegisterCreatCompanyViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 获取公司info
- (void)setUsersCompanyList
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString * urlstr=[NSString stringWithFormat:GetCompanyInfo,[_companyId intValue]];
    [manager GET:urlstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            [_model setValuesForKeysWithDictionary:responseObject[@"data"]];
            [self updataViewData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)updataViewData{
    self.companyNameLabel.text=_model.name;
    self.descriptionLabel.text=_model.desc;
    _userCount.text=[NSString stringWithFormat:@"%@人",_model.userCount];
    _creatorLabel.text=_model.creatorName;
    if (_model.desc!=nil) {
        _descTV.hidden=YES;
    }
    
    [self setButtonShowAndHidden]; //设置button显示
}

- (IBAction)chanshengyaoqingmaClick:(id)sender {
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    if ([num isEqualToNumber:_model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
        [self pushAddFriend];
    }else{
        [ToolOfClass showMessage:@"您没有相应权限哦!"];
    }
}

#pragma mark textFieldDidBeginEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSInteger size=0;
    int height=173+150;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)sender {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}


- (IBAction)touchView:(id)sender {
    [self.view endEditing:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _descTV.hidden=YES;
    NSInteger size=0;
    int height=173+150+70;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length==0) {
        _descTV.hidden=NO;
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)cameraButClick:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
    
}

// 修改公司信息
- (void)onUpdataButtonClick{

    //1.信息是否变更
//    if (self.companyNameTextField.text.length==0 || self.phoneTextField.text.length==0) {
//        [ToolOfClass showMessage:@"输入集体名称或集体电话"];
//        return;
//    }
    
    
    NSData *imagedata=nil;
    NSInteger changeNum=0;  //公司图片、名字、描述 改变个数  其中至少有一个改变才能提交修改
    
    NSData * Iimagedata = UIImageJPEGRepresentation(self.logoImageView.image, 1.0);
    if (Iimagedata.length>0) {
        changeNum++;
        imagedata=Iimagedata;
        if (self.logoName==nil) { // 上传默认图片
            self.logoName = @"companyAvatar.png";
        }
    }
    
    if (![_model.name isEqualToString:self.companyNameLabel.text]) {
        if (_companyNameLabel.text.length>20) {
            UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体名称太长了，不要超过20字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [uiav show];
        }
        changeNum++;
    }
    
    if (![_model.desc isEqualToString:self.descriptionLabel.text]) {
        if (_descriptionLabel.text.length>60) {
            UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体描述太长了，不要超过60字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [uiav show];
        }
        changeNum++;
    }
    
    if (changeNum==0) {   //没有修改数据
        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"您尚未修改信息" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [uiav show];
        return;
    }
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:@"正在发送数据..."];
    
    // 修改公司
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"name"] = self.companyNameLabel.text;
    parameter[@"desc"] = self.descriptionLabel.text;
//    if (Iimagedata.length>0) {
//        parameter[@"logo"] =Iimagedata;
//    }
    NSString* url=[NSString stringWithFormat:UpdataCompanyInfo,_companyId];
    [manager POST:url parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (self.logoName!=nil) { // 上传默认图片
            [formData appendPartWithFileData:imagedata name:@"logo" fileName:self.logoName mimeType:@"image/png"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue]==0) {
            [ToolOfClass showMessage:@"修改成功"];
            [SVProgressHUD dismiss];
            [self.navigationController popViewControllerAnimated:YES];
            //修改成功
        } else {
            [SVProgressHUD dismiss];
            [ToolOfClass showMessage:responseObject[@"message"]];
            NSLog(@"responseObject:::%@",responseObject[@"message"]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

#pragma mark --UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    self.logoName = [fileName copy];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//邀请好友点击事件
-(void)pushAddFriend{
    [self getyaoqingma];
//        AddBookViewController *phoneAB = [[AddBookViewController alloc] init];
//        phoneAB.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:phoneAB animated:YES];
}

-(void)getyaoqingma{
    __block NSString * inviteCode;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSLog(@"xuanZhongNum -------is :%d",[[info objectForKey:@"currentCompany"] intValue]);
    parameter[@"target"]=@"13000000000";//xuanZhongNum;//
    NSString * URLstr=[NSString stringWithFormat:CompanyInviteCode,[_model.id intValue]];
    
    [manager POST:URLstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Dic is %@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@1]) {
            //            NSLog(@"error is %@",[self replaceUnicode:responseObject[@"error"]]);
            [UIAlertView alertViewWithTitle:responseObject[@"error"]];
        }else if ([responseObject[@"code"] isEqualToNumber:@0]) {
            inviteCode =[responseObject[@"data"] objectForKey:@"code"];
            
            CompanyInviteCodeViewController *phoneAB = [[CompanyInviteCodeViewController alloc] init];
            phoneAB.hidesBottomBarWhenPushed = YES;
            phoneAB.navigationItem.title = @"手机通讯录";
            phoneAB.code=inviteCode;
//            phoneAB.Fname=friendName;
//            phoneAB.FriendPhoneNum=xuanZhongNum;
            [self.navigationController pushViewController:phoneAB animated:YES];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

//- (IBAction)exitButtonClick:(id)sender {
//    
//    CompanyModel *model = _model;
////    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
////    if ([num isEqualToNumber:model.creator]) {
//    ExitCompanyViewController *exitvc = [[ExitCompanyViewController alloc] initWithNibName:@"ExitCompanyViewController" bundle:[NSBundle mainBundle]];
//    exitvc.model=model;
//        exitvc.groupId = model.id;
//    if (model.userCount==nil) {
//        exitvc.userCount=1;
//    }else{
//        exitvc.userCount = [model.userCount integerValue];
//    }
//        [self.navigationController pushViewController:exitvc animated:YES];
////    }
//}

- (IBAction)allGroupsButClick:(id)sender {
    AllGroupViewController * allgvc=[[AllGroupViewController alloc] initWithNibName:@"AllGroupViewController" bundle:nil];
    allgvc.groupId=_model.id;
    [self.navigationController pushViewController:allgvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --退出相关
//转让点击事件
- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender {
    
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你不是admin，无权转让集体admin身份!" target:nil];
    } else {
        
        if ([_model.userCount intValue]<=1) {
            [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"这个集体就你一人儿，你还想转给谁，赶紧解散了吧，哈哈哈。" target:nil];
            return;
        }
        
        transferPowerViewController *convey = [[transferPowerViewController alloc] init];
        convey.groupId = _model.id;
        convey.succeedBlock = ^(NSNumber *userId){
            self.cannotDisband = YES;
            [self setUsersCompanyList];
            
        };
        [self.navigationController pushViewController:convey animated:YES];
    }
    
}
//解散点击事件
- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender {
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你不是Admin，无权解散集体!" target:nil];
    } else {
        [UIAlertView alertViewWithTitle:@"解散集体" subTitle:@"解散集体之后你将和好友失去联系，确定解散集体?" target:self];
    }
    
}
//退出点击事件
- (IBAction)exitButtonClick:(id)sender {
    UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否真的要退出该集体" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertV.tag=1000;
    [alertV show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        NSString *path =  [NSString stringWithFormat:delectCompanyPath,_model.id];
        NSString * mess=@"解散";
        if (alertView.tag==1000) {  //退出
            path = [NSString stringWithFormat:leaveCompanyPath,_model.id];
            mess=@"退出";
        }
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] intValue] == 0) {
                [ToolOfClass showMessage:[NSString stringWithFormat:@"%@成功",mess]];
                if ([mess isEqualToString:@"解散"]) {
                    _exitButton.hidden=NO;
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
                UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:responseObject[@"message"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
                [alertV show];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:[NSString stringWithFormat:@"%@失败",mess]];
        }];
        
    }
}


@end
