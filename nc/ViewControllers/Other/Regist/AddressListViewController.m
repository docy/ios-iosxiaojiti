//
//  AddressListViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "AddressListViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "InviteModeTableViewController.h"
#import "GroupModel.h"

@interface AddressListViewController ()

@end

@implementation AddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"访问通讯录";
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"composedingwei" target:self action:@selector(onBackButtonClick)];
}

// 返回按钮的点击事件
- (void)onBackButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 访问通讯录按钮点击事件
- (IBAction)addressListButton:(id)sender {
    InviteModeTableViewController *inviteVC = [[InviteModeTableViewController alloc] init];
    [self.navigationController pushViewController:inviteVC animated:YES];
}

// 拒绝按钮点击事件
- (IBAction)refuseButton:(id)sender {
    
}
@end
