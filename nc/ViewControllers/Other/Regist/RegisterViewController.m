//
//  RegisterViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "RegisterViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyNameViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "SeconRegisterViewController.h"
#import "ToolOfClass.h"

@interface RegisterViewController () <UITextFieldDelegate>

//@property (nonatomic, copy) NSString *identifyCode; // 存储验证码

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpRegisterViewData];
}

- (void)setUpRegisterViewData
{
    self.navigationItem.title = @"注册";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
//    self.identifyCode = [NSString string];
    
    self.nameLabel.delegate = self;
    self.emailLabel.delegate = self;
    self.phoneLabel.delegate = self;
    self.identifyCodeLabel.delegate = self;
    self.passwordLabel.delegate = self;
}

// 返回按钮的点击事件
- (void)onRegisterViewBackButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 获取验证码
- (IBAction)getIdentifyCodeBtnClick:(id)sender {
    
    if (self.phoneLabel.text.length==0) {
        [ToolOfClass showMessage:@"请输入手机号"];
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneLabel.text;
        
        [manager POST:getidentifyCodePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] intValue] == 0) {
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
//                [self startTimeSecondsCountDown];
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel];
                _identifyCodeBtn.enabled=NO;
                double delayInSeconds = 30.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyCodeBtn.enabled=YES;
                });
//                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}


// 下一步点击事件
- (IBAction)onNextButtonClick:(id)sender {
    
    if (self.nameLabel.text.length==0||self.phoneLabel.text.length == 0||self.emailLabel.text.length== 0||self.passwordLabel.text.length==0){
        [ToolOfClass showMessage:@"请输入有效信息!"];
    } else if (self.identifyCodeLabel.text.length==0) {
        [ToolOfClass showMessage:@"请输入手机验证码"];
    } else if (self.nameLabel.text.length>15) {
        [ToolOfClass showMessage:@"用户名控制在15个字以内"];
    } else {// 确保用户名，电话，邮箱已经输入信息
        // 检测输入信息是否正确
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"name"] = self.nameLabel.text;
        parameter[@"email"] = self.emailLabel.text;
        parameter[@"phone"] = self.phoneLabel.text;
        parameter[@"code"] = self.identifyCodeLabel.text;
        parameter[@"password"] = self.passwordLabel.text;
        
        [manager POST:registCheck parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

            if ([responseObject[@"code"] isEqualToNumber:@0]) { // 输入注册信息可用
                PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
                model.name = self.nameLabel.text;
                model.email = self.emailLabel.text;
                model.phone = self.phoneLabel.text;
                model.password = self.passwordLabel.text;

                // 跳转至个人资料界面
                NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
                [info setObject:self.nameLabel.text forKey:@"name"];
                SeconRegisterViewController *secondVC = [[SeconRegisterViewController alloc] initWithNibName:@"SeconRegisterViewController" bundle:[NSBundle mainBundle]];
                secondVC.personalModel = model;
                [self.navigationController pushViewController:secondVC animated:YES];
                
                
            } else { // 输入信息错误
                [ToolOfClass showMessage:responseObject[@"message"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

//-(void)startTimeSecondsCountDown{
//    __block int timeout = 30; //倒计时时间
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
//    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
//    dispatch_source_set_event_handler(_timer, ^{
//        if(timeout<=0){ //倒计时结束，关闭
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                [self.identifyCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
//                self.identifyCodeBtn.userInteractionEnabled = YES;
//            });
//        }else{
//            int seconds = timeout % 60;
//            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                //NSLog(@"____%@",strTime);
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
//                [self.identifyCodeBtn setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
//                [UIView commitAnimations];
//                self.identifyCodeBtn.userInteractionEnabled = NO;
//            });
//            timeout--;
//        }
//    });
//    dispatch_resume(_timer);
//}

@end
