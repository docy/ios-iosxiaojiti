//
//  RegisterCreatCompanyViewController.m
//  xjt
//
//  Created by docy admin on 7/31/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "RegisterCreatCompanyViewController.h"

#import <AFNetworking/AFNetworking.h>
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "APIHeader.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"


@interface RegisterCreatCompanyViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate>

@property (nonatomic, copy) NSString *logoName; // 公司logo名
@property (nonatomic, copy) NSString *companyPath; // 公司logo名
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISwitch *switchBar;
@end

@implementation RegisterCreatCompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self RegisterCreatCompanyViewSetUpData];
    [self getUpRegisterCreatCompanyViewRandomLogo];
}
// 设置基本属性
- (void)RegisterCreatCompanyViewSetUpData
{
    float hight=172;
    if (iPhone6) {
        hight=190;
    }else if (iPhone6Plus){
        hight=200;
    }
//    self.logoImageView.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight/2);
    
    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenHeight+100);
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = @"创建集体";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(RegisterCreatCompanyViewBackBtnClick)];
    self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithTitle:@"完成" target:self action:@selector(sendCreateMessageButtonClick)];
//    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:getImageBeforeRegisterPath,@"company"]] placeholderImage:nil];
    
 
}

-(void)sendCreateMessageButtonClick{
    [self RegisterCreatCompanyViewCreatCompanyBtnClick:nil];
}

// 返回按钮
- (void)RegisterCreatCompanyViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpRegisterCreatCompanyViewRandomLogo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:randomLogo] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            self.companyPath = responseObject[@"data"][@"logo"];
            [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:responseObject[@"data"][@"logo"]]] placeholderImage:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
// 选择图片
- (IBAction)RegisterCreatCompanyViewSelectLogoBtnClick:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

// 创建公司
- (IBAction)RegisterCreatCompanyViewCreatCompanyBtnClick:(id)sender {
    
    if (self.companyNameTextField.text.length==0 || self.phoneTextField.text.length==0) {
        [ToolOfClass showMessage:@"请输入集体名称或集体描述"];
        return;
    }
    if (_companyNameTextField.text.length>20) {
        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体名字太长了，不要超过20字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [uiav show];
        return;
    }
    if (_phoneTextField.text.length>60) {
        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体描述太长了，不要超过60字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [uiav show];
        return;
    }
    
    
    // 创建公司
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"name"] = self.companyNameTextField.text;
    parameter[@"desc"] = self.phoneTextField.text;
    parameter[@"isPrivate"] = @(self.switchBar.on?1:0);
    NSData *imagedata;
    if (self.logoName==nil) { // 上传默认图片
        self.logoName = @"companyAvatar.png";
        parameter[@"logoUrl"] = self.companyPath;
    }
    imagedata = UIImageJPEGRepresentation(self.logoImageView.image, 1.0);
    [manager POST:creatCompanyPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imagedata name:@"logoUrl" fileName:self.logoName mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue]==0) {
            // 设置当前公司
            [self RegisterCreatCompanyViewSetUserCurrentCompany:responseObject[@"data"][@"id"]];
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
            NSLog(@"responseObject:::%@",responseObject[@"message"]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark --UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    self.logoName = [fileName copy];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 设置为当前公司
- (void)RegisterCreatCompanyViewSetUserCurrentCompany:(NSNumber *)companyId
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [info setObject:companyId forKey:@"currentCompany"];
    NSString *str = [NSString stringWithFormat:setCurrentCompany,companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            
            [info setObject:companyId forKey:@"currentCompany"];
            [info setValue:@"YES" forKey:@"isHaveCurrent"];
            [info setObject:@"NO" forKey:@"isLogout"];

//            self.view.window.rootViewController = [UITabBarController creatTabBarController];
//            self.view.window.rootViewController = [[XJTTabBarController alloc] init];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
            [ToolOfClass showMessage:@"成功创建公司"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [ToolOfClass showMessage:responseObject[@"message"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:@"创建失败"];
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.companyNameTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
}

#pragma mark textFieldDidBeginEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSInteger size=0;
    int height=173+150;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
    //    [self.tableview setContentOffset:CGPointMake(0, 70) animated:YES];
    //    [self.themeTextFiled becomeFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)sender {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (IBAction)touchView:(id)sender {
    [self.view endEditing:YES];  
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _descTV.hidden=YES;
    NSInteger size=0;
    int height=173+150+70;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length==0) {
        _descTV.hidden=NO;
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}



@end
