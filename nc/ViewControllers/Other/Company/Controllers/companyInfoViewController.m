//
//  companyInfoViewController.m
//  
//
//  Created by guanxf on 15/10/26.
//
//

#import "companyInfoViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIImageView+WebCache.h"
#import "AFHTTPRequestOperationManager.h"
#import "AllGroupViewController.h"

@interface companyInfoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *companyNameL;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *usersCount;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;

@end

@implementation companyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpGroupConveyGroupViewData];
    
    _companyNameL.text=_model.name;
    _descriptionText.text=_model.desc==nil?@"暂无描述内容！":_model.desc;
    _descriptionText.font=[UIFont systemFontOfSize:15];
    _descriptionText.textColor =[UIColor grayColor];
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:_model.logoUrlOrigin]] placeholderImage:nil];
    
    _usersCount.text=[NSString stringWithFormat:@"%@人",_model.userCount];
    _creatorLabel.text=_model.creatorName;
}

- (void)setUpGroupConveyGroupViewData{
    self.navigationItem.title = @"集体信息";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpGroupConveyGroupViewBack)];
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
}

-(void)setUpGroupConveyGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)joinCompanyButtonclick:(id)sender {
    [self joinInCompany];
}


// 加入公司
- (void)joinInCompany
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [addCompany stringByAppendingFormat:@"%d",[_model.id intValue]];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@0]) { // 加入公司成功
            // 设置为用户当前公司
            [self setUserCurrentCompany];
            //设置成功后直接跳转到 新加公司
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 设置当前公司
- (void)setUserCurrentCompany
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,[NSNumber numberWithInt:[_model.id intValue]]];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:[NSNumber numberWithInt:[_model.id intValue]] forKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"添加设置当前集体失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

- (IBAction)showAllGroupsButClick:(id)sender {
    AllGroupViewController * allgvc=[[AllGroupViewController alloc] initWithNibName:@"AllGroupViewController" bundle:nil];
    allgvc.groupId=_model.id;
    [self.navigationController pushViewController:allgvc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
