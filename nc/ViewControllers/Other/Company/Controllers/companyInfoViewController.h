//
//  companyInfoViewController.h
//  
//
//  Created by guanxf on 15/10/26.
//
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"
@interface companyInfoViewController : UIViewController

@property(nonatomic ,retain) CompanyModel * model;  //传入的公司数据

@end
