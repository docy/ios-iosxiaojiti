//
//  LeadingViewController.m
//  nc
//
//  Created by docy admin on 6/6/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LeadingViewController.h"
#import "LoginViewController.h"
#import "XJTNavigationController.h"
#import "CompanyListViewController.h"
#import "UIButton+XJTButton.h"

#define kImageCount 3
@interface LeadingViewController ()

@property (nonatomic, retain) NSMutableArray *imageData;

@end

@implementation LeadingViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageData = [NSMutableArray array];
    
    self.pageView.hidden = YES;
    
    // 设置属性
    [self setUpScrollerView];
}

- (void)setUpScrollerView
{
//    self.scrollerView.delegate = self;

    self.scrollerView.contentSize = CGSizeMake(kImageCount*ScreenWidth, 0);
    NSArray *imageArr = @[@"引导页1",@"引导页2",@"引导页3"];
    for (NSInteger i = 0; i < kImageCount; i++) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageArr[i]]];
        imageView.frame = CGRectMake(i*ScreenWidth, 0, ScreenWidth, ScreenHeight);
        [self.scrollerView addSubview:imageView];
        
        if (i == kImageCount - 1) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(0, 0, ScreenWidth*0.4, 30);
            [button setTitle:@"立即体验" forState:UIControlStateNormal];
            button.center = CGPointMake(0.5*ScreenWidth, 0.91*ScreenHeight);
            [button addTarget:self action:@selector(onEnterButtonClick) forControlEvents:UIControlEventTouchUpInside];
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            button.layer.borderWidth = 0.5;
            [imageView addSubview:button];
            imageView.userInteractionEnabled = YES;
        }

    }
    
    // 设置pageView
    self.pageView.numberOfPages = kImageCount;
    self.pageView.currentPage = 0;
}

- (void)onEnterButtonClick
{
    // 还应该判断是否是登录状态，是否是版本更新（即已安装，只是更新新版本）
    [UIApplication sharedApplication].keyWindow.rootViewController = [[XJTNavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
}

// 减速结束时调用
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSInteger currentPage = self.scrollerView.contentOffset.x/ScreenWidth;
//    self.pageView.currentPage = currentPage;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
