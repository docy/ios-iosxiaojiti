//
//  MessageNotifyModel.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"

// 新消息通知model

@interface MessageNotifyModel : CommonModel

@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *body;

@end
